package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"net"
	"os"

	"bitbucket.org/kmomuke/rentau/grpc/dbManagers"
	pb "bitbucket.org/kmomuke/rentau/grpc/proto"
	"bitbucket.org/kmomuke/rentau/grpc/services"
	log "github.com/Sirupsen/logrus"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
)

var (
	certFile = flag.String("cert_file", "certfiles/ssl.crt", "The TLS cert file")
	keyFile  = flag.String("key_file", "certfiles/ssl.key", "The TLS key file")
	//jwtPrivateKey = flag.String("jwt_key_file", "certfiles/jwt-key.pem", "The TLS key file")
	jwtPrivateKey = flag.String("jwt_private_key_file", "certfiles/private.pem", "The TLS key file")
	jwtPublicKey  = flag.String("jwt_public_key_file", "certfiles/public.pem", "The TLS key file")
)

const (
	port = ":50051"
)

func init() {
	// Log as JSON instead of the default ASCII formatter.
	log.SetFormatter(&log.JSONFormatter{})

	// Output to stdout instead of the default stderr
	// Can be any io.Writer, see below for File example
	log.SetOutput(os.Stdout)

	// Only log the warning severity or above.
	log.SetLevel(log.InfoLevel)
}

func main() {

	var err error
	var lis net.Listener
	var grpcServer *grpc.Server

	creds, err := credentials.NewServerTLSFromFile(*certFile, *keyFile)
	lis, err = net.Listen("tcp", port)
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
		// grpcServer = grpc.NewServer(grpc.Creds(creds))
	}
	grpcServer = grpc.NewServer(grpc.Creds(creds))
	// grpcServer = grpc.NewServer()
	println("Listen on " + port + " with TLS")

	privateKey, err := ioutil.ReadFile(*jwtPrivateKey)
	if err != nil {
		log.Fatalf("ReadFile: %v", err)
	}

	publicKey, err := ioutil.ReadFile(*jwtPublicKey)
	if err != nil {
		log.Fatalf("ReadFile: %v", err)
	}

	connInfo := fmt.Sprintf(
		"user=%s dbname=%s password=%s host=%s port=%s sslmode=disable",
		os.Getenv("DB_ENV_POSTGRES_USER"),
		os.Getenv("DB_ENV_POSTGRES_DATABASENAME"),
		os.Getenv("DB_ENV_POSTGRES_PASSWORD"),
		os.Getenv("RENTAU_POSTGRES_1_PORT_5432_TCP_ADDR"),
		os.Getenv("RENTAU_POSTGRES_1_PORT_5432_TCP_PORT"),
	)

	log.WithFields(log.Fields{"-----------------------------": ""}).Info("")
	log.WithFields(log.Fields{"connInfo": connInfo}).Info("")
	log.WithFields(log.Fields{"-----------------------------": ""}).Info("")

	dbManagers.MigrateDatabaseUp(connInfo, "file://dbManagers/migrations/")
	dbMng := dbManagers.NewDbManager(connInfo)

	//dbManagers.MigrateDatabaseUp(dbManagers.DevelopmentPath, "file://dbManagers/migrations/")
	//dbMng := dbManagers.NewDbManager(dbManagers.DevelopmentPath)

	rentauService, err := services.NewRentauService(privateKey, publicKey, dbMng)
	if err != nil {
		log.Fatalf("NewRentauService: %v", err)
	}

	pb.RegisterRentauServiceServer(grpcServer, rentauService)
	grpcServer.Serve(lis)
}
