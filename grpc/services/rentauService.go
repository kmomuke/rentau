package services

import (
	"crypto/rsa"
	"io"
	"time"

	"bitbucket.org/kmomuke/rentau/grpc/auth"
	"bitbucket.org/kmomuke/rentau/grpc/dbManagers"
	"bitbucket.org/kmomuke/rentau/grpc/dbManagers/models"
	pb "bitbucket.org/kmomuke/rentau/grpc/proto"
	log "github.com/Sirupsen/logrus"
	jwt "github.com/dgrijalva/jwt-go"
	google_protobuf "github.com/golang/protobuf/ptypes/empty"
	"github.com/pkg/errors"
	uuid "github.com/satori/go.uuid"
	"golang.org/x/crypto/bcrypt"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
)

type RentauServiceInterface interface {
	generateToken(userUUID string) (string, error)
	getUserFromContext(ctx context.Context) (*pb.UserRequest, error)

	SignUp(context.Context, *pb.SignUpRequest) (*pb.SignUpRequest, error)
	SignIn(context.Context, *pb.SignInRequest) (*pb.SignInRequest, error)
	IsEmailRegistered(context.Context, *pb.SignInRequest) (*pb.SignInRequest, error)
	GetUser(context.Context, *google_protobuf.Empty) (*pb.UserRequest, error)
	DeleteAllForCompany(context.Context, *pb.SignInRequest) (*pb.SignInRequest, error)

	CreateUser(context.Context, *pb.UserRequest) (*pb.UserRequest, error)
	UpdateUser(context.Context, *pb.UserRequest) (*pb.UserRequest, error)
	AllUsersForInitial(context.Context, *pb.UserFilter) (*pb.AllUserResponse, error)
	CheckUsersForUpdate(context.Context, *pb.UserFilter) (*pb.AllUserResponse, error)

	CreateCategory(context.Context, *pb.CategoryRequest) (*pb.CategoryRequest, error)
	UpdateCategory(context.Context, *pb.CategoryRequest) (*pb.CategoryRequest, error)
	AllCategoriesForInitial(context.Context, *pb.CategoryFilter) (*pb.AllCategoryResponse, error)
	CheckCategoriesForUpdate(context.Context, *pb.CategoryFilter) (*pb.AllCategoryResponse, error)

	CreateProduct(context.Context, *pb.CreateProductRequest) (*pb.CreateProductRequest, error)
	UpdateProduct(context.Context, *pb.CreateProductRequest) (*pb.CreateProductRequest, error)
	AllProductsForInitial(context.Context, *pb.ProductFilter) (*pb.AllProductsResponse, error)
	CheckProductsForUpdate(context.Context, *pb.ProductFilter) (*pb.AllProductsResponse, error)

	CheckOrderDetailsForUpdate(context.Context, *pb.OrderDetailFilter) (*pb.AllOrderDetailResponse, error)
	AllOrderDetails(context.Context, *pb.OrderDetailFilter) (*pb.AllOrderDetailResponse, error)

	CreateCustomer(context.Context, *pb.CreateCustomerRequest) (*pb.CreateCustomerRequest, error)
	UpdateCustomer(context.Context, *pb.CustomerRequest) (*pb.CustomerRequest, error)
	UpdateCustomerBalance(context.Context, *pb.CreateCustomerRequest) (*pb.CreateCustomerRequest, error)
	AllCustomersForInitial(context.Context, *pb.CustomerFilter) (*pb.AllCustomersResponse, error)
	CheckCustomersForUpdate(context.Context, *pb.CustomerFilter) (*pb.AllCustomersResponse, error)

	CreateSupplier(context.Context, *pb.CreateSupplierRequest) (*pb.CreateSupplierRequest, error)
	UpdateSupplier(context.Context, *pb.SupplierRequest) (*pb.SupplierRequest, error)
	UpdateSupplierBalance(context.Context, *pb.CreateSupplierRequest) (*pb.CreateSupplierRequest, error)
	AllSuppliersForInitial(context.Context, *pb.SupplierFilter) (*pb.AllSuppliersResponse, error)
	CheckSuppliersForUpdate(context.Context, *pb.SupplierFilter) (*pb.AllSuppliersResponse, error)

	UpdateTransaction(context.Context, *pb.TransactionRequest) (*pb.TransactionRequest, error)
	CheckTransactionsForUpdate(context.Context, *pb.TransactionFilter) (*pb.AllTransactionResponse, error)
	AllTransactionsForInitial(context.Context, *pb.TransactionFilter) (*pb.AllTransactionResponse, error)

	CreateOrder(context.Context, *pb.CreateOrderRequest) (*pb.CreateOrderRequest, error)
	UpdateOrder(context.Context, *pb.OrderRequest) (*pb.OrderRequest, error)
	AllOrdersForInitial(context.Context, *pb.OrderFilter) (*pb.AllOrderResponse, error)
	CheckOrdersForUpdate(context.Context, *pb.OrderFilter) (*pb.AllOrderResponse, error)

	UpdateStream(pb.RentauService_UpdateStreamServer) error
}

type RentauService struct {
	jwtPrivateKey   *rsa.PrivateKey
	jwtPublicKey    *rsa.PublicKey
	dbManager       *dbManagers.DbManager
	syncChannel     chan func()
	responseChannel chan interface{}
	errorChannel    chan error
}

func NewRentauService(rsaPrivateKey []byte, rsaPublicKey []byte, db *dbManagers.DbManager) (*RentauService, error) {
	privateKey, err := jwt.ParseRSAPrivateKeyFromPEM(rsaPrivateKey)
	if err != nil {
		return nil, errors.Wrap(err, "Error parsing the jwt private key")
	}

	publicKey, err := jwt.ParseRSAPublicKeyFromPEM(rsaPublicKey)
	if err != nil {
		return nil, errors.Wrap(err, "Error parsing the jwt public key")
	}

	service := new(RentauService)
	service.jwtPrivateKey = privateKey
	service.jwtPublicKey = publicKey
	service.dbManager = db
	service.syncChannel = make(chan func(), 1000)
	service.responseChannel = make(chan interface{}, 1000)
	service.errorChannel = make(chan error, 1000)
	service.followSyncChannel()

	return service, nil
}

func (s *RentauService) followSyncChannel() {
	go func() {
		for f := range s.syncChannel {
			f()
		}
	}()
}

func (s *RentauService) sendObj(obj interface{}) {
	go func() {
		s.responseChannel <- obj
	}()
}

func (s *RentauService) sendError(err error) {
	if err != nil {
		go func() {
			log.WithFields(log.Fields{"err": err}).Warn("")
			s.errorChannel <- err
		}()
	}
}

func (s *RentauService) generateToken(userUUID string) (string, error) {

	claims := jwt.StandardClaims{
		Audience:  userUUID,
		ExpiresAt: time.Now().Add(time.Hour * 1000000).Unix(),
		Issuer:    "auth.service",
		IssuedAt:  time.Now().Unix(),
	}
	t := jwt.NewWithClaims(jwt.SigningMethodRS256, claims)
	ts, err := t.SignedString(s.jwtPrivateKey)
	if err != nil {
		return "", errors.Wrap(err, "Failed to create the auth token")
	}

	return ts, nil
}

func (s *RentauService) getUserFromContext(ctx context.Context) (*pb.UserRequest, error) {
	token, ok := auth.GetTokenFromContext(ctx, s.jwtPublicKey)
	if !ok {
		return nil, grpc.Errorf(codes.Unauthenticated, "valid token required.")
	}
	claims := token.Claims.(*jwt.StandardClaims)
	user, err := s.dbManager.UserForUUID(claims.Audience)
	if err != nil {
		return nil, grpc.Errorf(codes.FailedPrecondition, "User not found")
	}
	return user, nil
}

// Authorization
func (s *RentauService) SignUp(ctx context.Context, signUpReq *pb.SignUpRequest) (*pb.SignUpRequest, error) {

	userExsist, err := s.dbManager.UserForEmail(signUpReq.Email)
	if userExsist != nil {
		return &pb.SignUpRequest{}, errors.New("User already exsists")
	}

	pass, err := bcrypt.GenerateFromPassword([]byte(signUpReq.Password), bcrypt.DefaultCost)
	if err != nil {
		return &pb.SignUpRequest{}, errors.New("Failed to encrypt the Password")
	}

	company := new(pb.CompanyRequest)
	company.CompanyName = signUpReq.FirstName
	company.Email = signUpReq.Email
	company.Address = signUpReq.SecondName
	companyId, err := s.dbManager.CreateCompany(company)

	user := new(pb.UserRequest)
	user.RoleId = 1000
	uuidLocal, _ := uuid.NewV4()
	user.UserUUID = uuidLocal.String()
	user.UserImagePath = ""
	user.FirstName = signUpReq.FirstName
	user.SecondName = signUpReq.SecondName
	user.Email = signUpReq.Email
	user.Password = string(pass)
	user.PhoneNumber = ""
	user.Address = ""

	userId, err := s.dbManager.CreateUser(user, companyId)
	log.WithFields(log.Fields{"user created userId": userId}).Info("")
	if err != nil {
		return &pb.SignUpRequest{}, err
	}

	log.WithFields(log.Fields{"user.UserUUID": user.UserUUID}).Info("")

	token, err := s.generateToken(user.UserUUID)
	if err != nil {
		return &pb.SignUpRequest{}, err
	}

	signUpReq.Token = token

	return signUpReq, nil
}

func (s *RentauService) SignIn(ctx context.Context, signInReq *pb.SignInRequest) (*pb.SignInRequest, error) {

	userExsist, err := s.dbManager.UserForEmail(signInReq.Email)
	if userExsist == nil {
		return &pb.SignInRequest{}, errors.New("User with email does not exsists")
	}

	if bcrypt.CompareHashAndPassword([]byte(userExsist.Password), []byte(signInReq.Password)) != nil {
		return &pb.SignInRequest{}, grpc.Errorf(codes.PermissionDenied, "Password invalid")
	}

	token, err := s.generateToken(userExsist.UserUUID)
	if err != nil {
		return &pb.SignInRequest{}, err
	}

	signInReq.Token = token

	return signInReq, nil
}

func (s *RentauService) IsEmailRegistered(ctx context.Context, signInReq *pb.SignInRequest) (*pb.SignInRequest, error) {
	userExsist, err := s.dbManager.UserForEmail(signInReq.Email)
	if err != nil {
		return &pb.SignInRequest{}, err
	}
	if userExsist == nil {
		signInReq.Email = "empty"
		return signInReq, nil
	} else {
		return signInReq, nil
	}
}

func (s *RentauService) GetUser(ctx context.Context, empty *google_protobuf.Empty) (*pb.UserRequest, error) {
	user, err := s.getUserFromContext(ctx)
	if err != nil {
		return &pb.UserRequest{}, err
	}
	return user, nil
}

func (s *RentauService) DeleteAllForCompany(ctx context.Context, signInReq *pb.SignInRequest) (*pb.SignInRequest, error) {

	syncF := func() {
		userCompany, err := s.getUserFromContext(ctx)
		if err != nil {
			s.sendError(err)
		}

		tx := s.dbManager.DB.MustBegin()
		_, err = models.DeleteCategories(tx, userCompany.CompanyId)
		if err != nil {
			tx.Rollback()
			s.sendError(err)
		}

		_, err = models.DeleteCustomers(tx, userCompany.CompanyId)
		if err != nil {
			tx.Rollback()
			s.sendError(err)
		}

		_, err = models.DeleteOrders(tx, userCompany.CompanyId)
		if err != nil {
			tx.Rollback()
			s.sendError(err)
		}

		_, err = models.DeleteOrderDetails(tx, userCompany.CompanyId)
		if err != nil {
			tx.Rollback()
			s.sendError(err)
		}

		_, err = models.DeleteProducts(tx, userCompany.CompanyId)
		if err != nil {
			tx.Rollback()
			s.sendError(err)
		}

		_, err = models.DeleteSuppliers(tx, userCompany.CompanyId)
		if err != nil {
			tx.Rollback()
			s.sendError(err)
		}

		_, err = models.DeleteTransactions(tx, userCompany.CompanyId)
		if err != nil {
			tx.Rollback()
			s.sendError(err)
		}

		err = ctx.Err()
		if err != nil {
			tx.Rollback()
			s.sendError(err)
		}

		err = tx.Commit()
		if err != nil {
			s.sendError(err)
		}

		s.sendObj(signInReq)
	}

	s.syncChannel <- syncF

	select {
	case obj := <-s.responseChannel:
		s, ok := obj.(*pb.SignInRequest)
		if !ok {
			return &pb.SignInRequest{}, errors.New("Fail casting")
		} else {
			return s, nil
		}
	case err := <-s.errorChannel:
		return &pb.SignInRequest{}, err
	}
}

// User
func (s *RentauService) CreateUser(ctx context.Context, userReq *pb.UserRequest) (*pb.UserRequest, error) {
	userCompany, err := s.getUserFromContext(ctx)
	if err != nil {
		log.WithFields(log.Fields{"CreateUser": err}).Warn("")
		return &pb.UserRequest{}, err
	}

	hashedPass, err := bcrypt.GenerateFromPassword([]byte(userReq.Password), bcrypt.DefaultCost)
	if err != nil {
		return &pb.UserRequest{}, errors.New("Failed to encrypt the Password")
	}

	var originPassword = userReq.Password
	userReq.Password = string(hashedPass)
	userSerialKey, err := s.dbManager.CreateUser(userReq, userCompany.CompanyId)
	userReq.Password = originPassword

	if err != nil {
		log.WithFields(log.Fields{"CreateUser": err}).Warn("")
		return &pb.UserRequest{}, err
	}
	userReq.UserId = userSerialKey
	return userReq, nil
}

func (s *RentauService) UpdateUser(ctx context.Context, userReq *pb.UserRequest) (*pb.UserRequest, error) {
	userCompany, err := s.getUserFromContext(ctx)
	if err != nil {
		log.WithFields(log.Fields{"UpdateUser": err}).Warn("")
		return &pb.UserRequest{}, err
	}

	hashedPass, err := bcrypt.GenerateFromPassword([]byte(userReq.Password), bcrypt.DefaultCost)
	if err != nil {
		return &pb.UserRequest{}, errors.New("Failed to encrypt the Password")
	}

	var originPassword = userReq.Password
	userReq.Password = string(hashedPass)
	_, err = s.dbManager.UpdateUser(userReq, userCompany.CompanyId)
	userReq.Password = originPassword

	if err != nil {
		log.WithFields(log.Fields{"UpdateUser": err}).Warn("")
		return &pb.UserRequest{}, err
	}
	return userReq, nil
}

func (s *RentauService) AllUsersForInitial(ctx context.Context, userFilter *pb.UserFilter) (*pb.AllUserResponse, error) {
	userCompany, err := s.getUserFromContext(ctx)
	if err != nil {
		log.WithFields(log.Fields{"AllUsersForInitial": err}).Warn("")
		return &pb.AllUserResponse{}, err
	}
	users, err := s.dbManager.AllUsers(userCompany.CompanyId)
	if err != nil {
		log.WithFields(log.Fields{"AllUsersForInitial": err}).Warn("")
		return &pb.AllUserResponse{}, err
	}
	allUsersResponse := new(pb.AllUserResponse)
	allUsersResponse.UserRequests = users
	return allUsersResponse, nil
}

func (s *RentauService) CheckUsersForUpdate(ctx context.Context, userFilter *pb.UserFilter) (*pb.AllUserResponse, error) {
	userCompany, err := s.getUserFromContext(ctx)
	if err != nil {
		log.WithFields(log.Fields{"CheckUsersForUpdate": err}).Warn("")
		return &pb.AllUserResponse{}, err
	}
	users, err := s.dbManager.AllUsersForUpdate(userFilter, userCompany.CompanyId)
	if err != nil {
		log.WithFields(log.Fields{"CheckUsersForUpdate": err}).Warn("")
		return &pb.AllUserResponse{}, err
	}
	allUsersResponse := new(pb.AllUserResponse)
	allUsersResponse.UserRequests = users
	return allUsersResponse, nil
}

// Category
func (s *RentauService) CreateCategory(ctx context.Context, catReq *pb.CategoryRequest) (*pb.CategoryRequest, error) {
	userCompany, err := s.getUserFromContext(ctx)
	if err != nil {
		log.WithFields(log.Fields{"CreateCategory": err}).Warn("")
		return &pb.CategoryRequest{}, err
	}
	categorySerialKey, err := s.dbManager.CreateCategory(catReq, userCompany.CompanyId)
	if err != nil {
		log.WithFields(log.Fields{"CreateCategory": err}).Warn("")
		return &pb.CategoryRequest{}, err
	}
	catReq.CategoryId = categorySerialKey
	return catReq, nil
}

func (s *RentauService) UpdateCategory(ctx context.Context, catReq *pb.CategoryRequest) (*pb.CategoryRequest, error) {
	userCompany, err := s.getUserFromContext(ctx)
	if err != nil {
		log.WithFields(log.Fields{"UpdateCategory": err}).Warn("")
		return &pb.CategoryRequest{}, err
	}
	_, err = s.dbManager.UpdateCategory(catReq, userCompany.CompanyId)
	if err != nil {
		log.WithFields(log.Fields{"UpdateCategory": err}).Warn("")
		return &pb.CategoryRequest{}, err
	}
	return catReq, nil
}

func (s *RentauService) AllCategoriesForInitial(ctx context.Context, catFilter *pb.CategoryFilter) (*pb.AllCategoryResponse, error) {
	userCompany, err := s.getUserFromContext(ctx)
	if err != nil {
		log.WithFields(log.Fields{"AllCategoriesForInitial": err}).Warn("")
		return &pb.AllCategoryResponse{}, err
	}
	categories, err := s.dbManager.AllCategoriesForInitial(userCompany.CompanyId)
	if err != nil {
		log.WithFields(log.Fields{"AllCategoriesForInitial": err}).Warn("")
		return &pb.AllCategoryResponse{}, err
	}

	allCategoriesResponse := new(pb.AllCategoryResponse)
	allCategoriesResponse.CategoryRequests = categories
	return allCategoriesResponse, nil
}

func (s *RentauService) CheckCategoriesForUpdate(ctx context.Context, catFilter *pb.CategoryFilter) (*pb.AllCategoryResponse, error) {
	userCompany, err := s.getUserFromContext(ctx)
	if err != nil {
		log.WithFields(log.Fields{"CheckCategoriesForUpdate": err}).Warn("")
		return &pb.AllCategoryResponse{}, err
	}
	categories, err := s.dbManager.AllUpdatedCategories(catFilter, userCompany.CompanyId)
	if err != nil {
		log.WithFields(log.Fields{"CheckCategoriesForUpdate": err}).Warn("")
		return &pb.AllCategoryResponse{}, err
	}
	allCategoriesResponse := new(pb.AllCategoryResponse)
	allCategoriesResponse.CategoryRequests = categories
	return allCategoriesResponse, nil
}

// Product
func (s *RentauService) CreateProduct(ctx context.Context, creatProdReq *pb.CreateProductRequest) (*pb.CreateProductRequest, error) {

	syncF := func() {
		user, err := s.getUserFromContext(ctx)
		if err != nil {
			s.sendError(err)
		}

		tx := s.dbManager.DB.MustBegin()
		productSerialKey, err := models.StoreProduct(tx, creatProdReq.Product, user.CompanyId)
		if err != nil {
			tx.Rollback()
			s.sendError(err)
		}

		creatProdReq.Product.ProductId = productSerialKey
		creatProdReq.OrderDetail.ProductId = productSerialKey

		orderDetailSerialKey, err := models.StoreOrderDetails(tx, creatProdReq.OrderDetail, user.CompanyId)
		if err != nil {
			tx.Rollback()
			s.sendError(err)
		}

		creatProdReq.OrderDetail.OrderDetailId = orderDetailSerialKey

		contexErr := ctx.Err()
		if contexErr != nil {
			tx.Rollback()
			s.sendError(err)
		}

		err = tx.Commit()
		if err != nil {
			s.sendError(err)
		}

		s.sendObj(creatProdReq)
	}

	s.syncChannel <- syncF

	select {
	case obj := <-s.responseChannel:
		s, ok := obj.(*pb.CreateProductRequest)
		if !ok {
			return &pb.CreateProductRequest{}, errors.New("Fail casting")
		} else {
			return s, nil
		}
	case err := <-s.errorChannel:
		return &pb.CreateProductRequest{}, err
	}
}

func (s *RentauService) UpdateProduct(ctx context.Context, creatProdReq *pb.CreateProductRequest) (*pb.CreateProductRequest, error) {
	syncF := func() {
		user, err := s.getUserFromContext(ctx)
		if err != nil {
			s.sendError(err)
		}

		tx := s.dbManager.DB.MustBegin()
		_, err = models.UpdateProduct(tx, creatProdReq.Product, user.CompanyId)
		if err != nil {
			tx.Rollback()
			s.sendError(err)
		}

		orderDetailSerialKey, err := models.StoreOrderDetails(tx, creatProdReq.OrderDetail, user.CompanyId)
		if err != nil {
			tx.Rollback()
			s.sendError(err)
		}

		creatProdReq.OrderDetail.OrderDetailId = orderDetailSerialKey

		contexErr := ctx.Err()
		if contexErr != nil {
			tx.Rollback()
			s.sendError(err)
		}

		err = tx.Commit()
		if err != nil {
			s.sendError(err)
		}

		s.sendObj(creatProdReq)
	}

	s.syncChannel <- syncF

	select {
	case obj := <-s.responseChannel:
		s, ok := obj.(*pb.CreateProductRequest)
		if !ok {
			return &pb.CreateProductRequest{}, errors.New("Fail casting")
		} else {
			return s, nil
		}
	case err := <-s.errorChannel:
		return &pb.CreateProductRequest{}, err
	}
}

func (s *RentauService) createProductResponse(products []*pb.ProductRequest) (*pb.AllProductsResponse, error) {
	createProdRequests := make([]*pb.CreateProductRequest, 0)

	for _, productReq := range products {

		orderDetReq, err := models.RecentOrderDetailForProduct(s.dbManager.DB, productReq)
		if err != nil {
			break
			return &pb.AllProductsResponse{}, err
		}

		createProductReq := new(pb.CreateProductRequest)
		createProductReq.OrderDetail = orderDetReq
		createProductReq.Product = productReq

		createProdRequests = append(createProdRequests, createProductReq)
	}

	allProdResponse := new(pb.AllProductsResponse)
	allProdResponse.ProductRequests = createProdRequests

	return allProdResponse, nil
}

func (s *RentauService) AllProductsForInitial(ctx context.Context, prodFilter *pb.ProductFilter) (*pb.AllProductsResponse, error) {
	userCompany, err := s.getUserFromContext(ctx)
	if err != nil {
		log.WithFields(log.Fields{"AllProductsForInitial": err}).Warn("")
		return &pb.AllProductsResponse{}, err
	}
	products, err := s.dbManager.AllProductsForInitial(userCompany.CompanyId)
	if err != nil {
		log.WithFields(log.Fields{"AllProductsForInitial": err}).Warn("")
		return &pb.AllProductsResponse{}, err
	}
	all, err := s.createProductResponse(products)
	if err != nil {
		log.WithFields(log.Fields{"AllProductsForInitial": err}).Warn("")
		return &pb.AllProductsResponse{}, err
	}
	return all, nil
}

func (s *RentauService) CheckProductsForUpdate(ctx context.Context, prodFilter *pb.ProductFilter) (*pb.AllProductsResponse, error) {
	userCompany, err := s.getUserFromContext(ctx)
	if err != nil {
		log.WithFields(log.Fields{"CheckProductsForUpdate": err}).Warn("")
		return &pb.AllProductsResponse{}, err
	}
	products, err := s.dbManager.AllUpdatedProducts(prodFilter, userCompany.CompanyId)
	if err != nil {
		log.WithFields(log.Fields{"CheckProductsForUpdate": err}).Warn("")
		return &pb.AllProductsResponse{}, err
	}

	all, err := s.createProductResponse(products)
	if err != nil {
		log.WithFields(log.Fields{"CheckProductsForUpdate": err}).Warn("")
		return &pb.AllProductsResponse{}, err
	}
	return all, nil
}

// OrderDetails
func (s *RentauService) CheckOrderDetailsForUpdate(ctx context.Context, orderDetailFilter *pb.OrderDetailFilter) (*pb.AllOrderDetailResponse, error) {
	userCompany, err := s.getUserFromContext(ctx)
	if err != nil {
		log.WithFields(log.Fields{"AllProductsForInitial": err}).Warn("")
		return &pb.AllOrderDetailResponse{}, err
	}
	orderDetails, err := s.dbManager.AllUpdatedOrderDetails(orderDetailFilter, userCompany.CompanyId)
	if err != nil {
		log.WithFields(log.Fields{"AllProductsForInitial": err}).Warn("")
		return &pb.AllOrderDetailResponse{}, err
	}

	allOrderDetailResponse := new(pb.AllOrderDetailResponse)
	allOrderDetailResponse.OrderDetailRequests = orderDetails
	return allOrderDetailResponse, nil
}

func (s *RentauService) AllOrderDetails(ctx context.Context, orderDetailFilter *pb.OrderDetailFilter) (*pb.AllOrderDetailResponse, error) {
	userCompany, err := s.getUserFromContext(ctx)
	if err != nil {
		log.WithFields(log.Fields{"AllOrderDetails": err}).Warn("")
		return &pb.AllOrderDetailResponse{}, err
	}
	orderDetails, err := s.dbManager.AllOrderDetails(orderDetailFilter, userCompany.CompanyId)
	if err != nil {
		log.WithFields(log.Fields{"AllOrderDetails": err}).Warn("")
		return &pb.AllOrderDetailResponse{}, err
	}

	allOrderDetailResponse := new(pb.AllOrderDetailResponse)
	allOrderDetailResponse.OrderDetailRequests = orderDetails
	return allOrderDetailResponse, nil
}

// Customer
func (s *RentauService) CreateCustomer(ctx context.Context, crteCstrReq *pb.CreateCustomerRequest) (*pb.CreateCustomerRequest, error) {

	syncF := func() {
		userCompany, err := s.getUserFromContext(ctx)
		if err != nil {
			s.sendError(err)
		}

		tx := s.dbManager.DB.MustBegin()
		customerSerial, err := models.StoreCustomer(tx, crteCstrReq.Customer, userCompany.CompanyId)
		if err != nil {
			tx.Rollback()
			s.sendError(err)
		}

		crteCstrReq.Customer.CustomerId = customerSerial
		crteCstrReq.Transaction.CustomerId = customerSerial
		crteCstrReq.Account.CustomerId = customerSerial

		transactionSerial, err := models.StoreTransaction(tx, crteCstrReq.Transaction, userCompany.CompanyId)
		if err != nil {
			tx.Rollback()
			s.sendError(err)
		}

		crteCstrReq.Transaction.TransactionId = transactionSerial

		accountSerial, err := models.StoreAccount(tx, crteCstrReq.Account)
		if err != nil {
			tx.Rollback()
			s.sendError(err)
		}

		crteCstrReq.Account.AccountId = accountSerial

		contexErr := ctx.Err()
		if contexErr != nil {
			tx.Rollback()
			s.sendError(err)
		}

		err = tx.Commit()
		if err != nil {
			s.sendError(err)
		}

		s.sendObj(crteCstrReq)
	}

	s.syncChannel <- syncF

	select {
	case obj := <-s.responseChannel:
		s, ok := obj.(*pb.CreateCustomerRequest)
		if !ok {
			return &pb.CreateCustomerRequest{}, errors.New("Fail casting")
		} else {
			return s, nil
		}
	case err := <-s.errorChannel:
		return &pb.CreateCustomerRequest{}, err
	}
}

func (s *RentauService) UpdateCustomer(ctx context.Context, customerReq *pb.CustomerRequest) (*pb.CustomerRequest, error) {
	userCompany, err := s.getUserFromContext(ctx)
	if err != nil {
		log.WithFields(log.Fields{"UpdateCustomer": err}).Warn("")
		return &pb.CustomerRequest{}, err
	}
	_, err = s.dbManager.UpdateCustomer(customerReq, userCompany.CompanyId)
	if err != nil {
		log.WithFields(log.Fields{"UpdateCustomer": err}).Warn("")
		return &pb.CustomerRequest{}, err
	}
	return customerReq, nil
}

func (s *RentauService) UpdateCustomerBalance(ctx context.Context, updateCustBalanceReq *pb.CreateCustomerRequest) (*pb.CreateCustomerRequest, error) {

	syncF := func() {
		userCompany, err := s.getUserFromContext(ctx)
		if err != nil {
			s.sendError(err)
		}

		tx := s.dbManager.DB.MustBegin()
		transactionSerial, err := models.StoreTransaction(tx, updateCustBalanceReq.Transaction, userCompany.CompanyId)
		if err != nil {
			tx.Rollback()
			s.sendError(err)
		}

		updateCustBalanceReq.Transaction.TransactionId = transactionSerial

		_, err = models.UpdateCustomerBalance(tx, updateCustBalanceReq.Account.CustomerId, updateCustBalanceReq.Account.Balance)
		if err != nil {
			tx.Rollback()
			s.sendError(err)
		}

		contexErr := ctx.Err()
		if contexErr != nil {
			tx.Rollback()
			s.sendError(err)
		}

		err = tx.Commit()
		if err != nil {
			s.sendError(err)
		}

		s.sendObj(updateCustBalanceReq)
	}

	s.syncChannel <- syncF

	select {
	case obj := <-s.responseChannel:
		s, ok := obj.(*pb.CreateCustomerRequest)
		if !ok {
			return &pb.CreateCustomerRequest{}, errors.New("Fail casting")
		} else {
			return s, nil
		}
	case err := <-s.errorChannel:
		return &pb.CreateCustomerRequest{}, err
	}
}

func (s *RentauService) createCustomerResponse(customers []*pb.CustomerRequest) (*pb.AllCustomersResponse, error) {
	createCustomerRequests := make([]*pb.CreateCustomerRequest, 0)
	for _, customerReq := range customers {

		transactionReq, err := s.dbManager.RecentTransactionForCustomer(customerReq.CustomerId)
		if err != nil {
			break
			return &pb.AllCustomersResponse{}, err
		}

		accountReq, err := s.dbManager.AccountForCustomer(customerReq.CustomerId)
		if err != nil {
			break
			return &pb.AllCustomersResponse{}, err
		}

		createCustomerRequest := new(pb.CreateCustomerRequest)
		createCustomerRequest.Customer = customerReq
		createCustomerRequest.Transaction = transactionReq
		createCustomerRequest.Account = accountReq

		createCustomerRequests = append(createCustomerRequests, createCustomerRequest)
	}

	allCustomersResponse := new(pb.AllCustomersResponse)
	allCustomersResponse.CustomerRequests = createCustomerRequests

	return allCustomersResponse, nil
}

func (s *RentauService) AllCustomersForInitial(ctx context.Context, customerFilter *pb.CustomerFilter) (*pb.AllCustomersResponse, error) {

	userCompany, err := s.getUserFromContext(ctx)
	if err != nil {
		log.WithFields(log.Fields{"AllCustomersForInitial": err}).Warn("")
		return &pb.AllCustomersResponse{}, err
	}

	customers, err := s.dbManager.AllCustomersForInitial(userCompany.CompanyId)
	if err != nil {
		log.WithFields(log.Fields{"AllCustomersForInitial": err}).Warn("")
		return &pb.AllCustomersResponse{}, err
	}

	allCustResp, err := s.createCustomerResponse(customers)
	if err != nil {
		log.WithFields(log.Fields{"AllCustomersForInitial": err}).Warn("")
		return &pb.AllCustomersResponse{}, err
	}
	return allCustResp, nil
}

func (s *RentauService) CheckCustomersForUpdate(ctx context.Context, customerFilter *pb.CustomerFilter) (*pb.AllCustomersResponse, error) {
	userCompany, err := s.getUserFromContext(ctx)
	if err != nil {
		log.WithFields(log.Fields{"CheckCustomersForUpdate": err}).Warn("")
		return &pb.AllCustomersResponse{}, err
	}
	customers, err := s.dbManager.AllUpdatedCustomers(customerFilter, userCompany.CompanyId)
	if err != nil {
		log.WithFields(log.Fields{"CheckCustomersForUpdate": err}).Warn("")
		return &pb.AllCustomersResponse{}, err
	}

	allCustResp, err := s.createCustomerResponse(customers)
	if err != nil {
		log.WithFields(log.Fields{"CheckCustomersForUpdate": err}).Warn("")
		return &pb.AllCustomersResponse{}, err
	}
	return allCustResp, nil
}

// Supplier
func (s *RentauService) CreateSupplier(ctx context.Context, createSupplReq *pb.CreateSupplierRequest) (*pb.CreateSupplierRequest, error) {
	syncF := func() {
		userCompany, err := s.getUserFromContext(ctx)
		if err != nil {
			s.sendError(err)
		}

		tx := s.dbManager.DB.MustBegin()
		supplierSerial, err := models.StoreSupplier(tx, createSupplReq.Supplier, userCompany.CompanyId)
		if err != nil {
			tx.Rollback()
			s.sendError(err)
		}

		createSupplReq.Supplier.SupplierId = supplierSerial
		createSupplReq.Transaction.SupplierId = supplierSerial
		createSupplReq.Account.SupplierId = supplierSerial

		transactionSerial, err := models.StoreTransaction(tx, createSupplReq.Transaction, userCompany.CompanyId)
		if err != nil {
			tx.Rollback()
			s.sendError(err)
		}

		createSupplReq.Transaction.TransactionId = transactionSerial

		accountSerial, err := models.StoreAccount(tx, createSupplReq.Account)
		if err != nil {
			tx.Rollback()
			s.sendError(err)
		}

		createSupplReq.Account.AccountId = accountSerial

		contexErr := ctx.Err()
		if contexErr != nil {
			tx.Rollback()
			s.sendError(err)
		}

		err = tx.Commit()
		if err != nil {
			s.sendError(err)
		}

		s.sendObj(createSupplReq)
	}

	s.syncChannel <- syncF

	select {
	case obj := <-s.responseChannel:
		s, ok := obj.(*pb.CreateSupplierRequest)
		if !ok {
			return &pb.CreateSupplierRequest{}, errors.New("Fail casting")
		} else {
			return s, nil
		}
	case err := <-s.errorChannel:
		return &pb.CreateSupplierRequest{}, err
	}
}

func (s *RentauService) UpdateSupplier(ctx context.Context, supplReq *pb.SupplierRequest) (*pb.SupplierRequest, error) {
	userCompany, err := s.getUserFromContext(ctx)
	if err != nil {
		log.WithFields(log.Fields{"UpdateCustomer": err}).Warn("")
		return &pb.SupplierRequest{}, err
	}
	_, err = s.dbManager.UpdateSupplier(supplReq, userCompany.CompanyId)
	if err != nil {
		log.WithFields(log.Fields{"UpdateCustomer": err}).Warn("")
		return &pb.SupplierRequest{}, err
	}
	return supplReq, nil
}

func (s *RentauService) UpdateSupplierBalance(ctx context.Context, updateSupplBlReq *pb.CreateSupplierRequest) (*pb.CreateSupplierRequest, error) {
	syncF := func() {
		userCompany, err := s.getUserFromContext(ctx)
		if err != nil {
			s.sendError(err)
		}

		tx := s.dbManager.DB.MustBegin()
		transactionSerial, err := models.StoreTransaction(tx, updateSupplBlReq.Transaction, userCompany.CompanyId)
		if err != nil {
			tx.Rollback()
			s.sendError(err)
		}

		updateSupplBlReq.Transaction.TransactionId = transactionSerial

		_, err = models.UpdateSupplierBalance(tx, updateSupplBlReq.Account.SupplierId, updateSupplBlReq.Account.Balance)
		if err != nil {
			tx.Rollback()
			s.sendError(err)
		}

		contexErr := ctx.Err()
		if contexErr != nil {
			tx.Rollback()
			s.sendError(err)
		}

		err = tx.Commit()
		if err != nil {
			s.sendError(err)
		}

		s.sendObj(updateSupplBlReq)
	}

	s.syncChannel <- syncF

	select {
	case obj := <-s.responseChannel:
		s, ok := obj.(*pb.CreateSupplierRequest)
		if !ok {
			return &pb.CreateSupplierRequest{}, errors.New("Fail casting")
		} else {
			return s, nil
		}
	case err := <-s.errorChannel:
		return &pb.CreateSupplierRequest{}, err
	}
}

func (s *RentauService) createSupplierResponse(suppliers []*pb.SupplierRequest) (*pb.AllSuppliersResponse, error) {
	createSupplierRequests := make([]*pb.CreateSupplierRequest, 0)
	for _, supplierReq := range suppliers {

		transactionReq, err := s.dbManager.RecentTransactionForSupplier(supplierReq.SupplierId)
		if err != nil {
			break
			return &pb.AllSuppliersResponse{}, err
		}

		accountReq, err := s.dbManager.AccountForSupplier(supplierReq.SupplierId)
		if err != nil {
			break
			return &pb.AllSuppliersResponse{}, err
		}

		createSupplierRequest := new(pb.CreateSupplierRequest)
		createSupplierRequest.Supplier = supplierReq
		createSupplierRequest.Transaction = transactionReq
		createSupplierRequest.Account = accountReq

		createSupplierRequests = append(createSupplierRequests, createSupplierRequest)
	}

	allSuppliersResponse := new(pb.AllSuppliersResponse)
	allSuppliersResponse.SupplierRequests = createSupplierRequests

	return allSuppliersResponse, nil
}

func (s *RentauService) AllSuppliersForInitial(ctx context.Context, supplFilter *pb.SupplierFilter) (*pb.AllSuppliersResponse, error) {
	userCompany, err := s.getUserFromContext(ctx)
	if err != nil {
		log.WithFields(log.Fields{"AllSuppliersForInitial": err}).Warn("")
		return &pb.AllSuppliersResponse{}, err
	}

	suppliers, err := s.dbManager.AllSuppliersForInitial(userCompany.CompanyId)
	if err != nil {
		log.WithFields(log.Fields{"AllSuppliersForInitial": err}).Warn("")
		return &pb.AllSuppliersResponse{}, err
	}

	allSuppResp, err := s.createSupplierResponse(suppliers)
	if err != nil {
		log.WithFields(log.Fields{"AllSuppliersForInitial": err}).Warn("")
		return &pb.AllSuppliersResponse{}, err
	}
	return allSuppResp, nil
}

func (s *RentauService) CheckSuppliersForUpdate(ctx context.Context, supplFilter *pb.SupplierFilter) (*pb.AllSuppliersResponse, error) {
	userCompany, err := s.getUserFromContext(ctx)
	if err != nil {
		log.WithFields(log.Fields{"CheckSuppliersForUpdate": err}).Warn("")
		return &pb.AllSuppliersResponse{}, err
	}
	suppliers, err := s.dbManager.AllUpdatedSuppliers(supplFilter, userCompany.CompanyId)
	if err != nil {
		log.WithFields(log.Fields{"CheckSuppliersForUpdate": err}).Warn("")
		return &pb.AllSuppliersResponse{}, err
	}

	allSuppResp, err := s.createSupplierResponse(suppliers)
	if err != nil {
		log.WithFields(log.Fields{"CheckSuppliersForUpdate": err}).Warn("")
		return &pb.AllSuppliersResponse{}, err
	}
	return allSuppResp, nil
}

// Transaction
func (s *RentauService) UpdateTransaction(ctx context.Context, transReq *pb.TransactionRequest) (*pb.TransactionRequest, error) {
	userCompany, err := s.getUserFromContext(ctx)
	if err != nil {
		log.WithFields(log.Fields{"UpdateTransaction": err}).Warn("")
		return &pb.TransactionRequest{}, err
	}
	_, err = s.dbManager.UpdateTransaction(transReq, userCompany.CompanyId)
	if err != nil {
		log.WithFields(log.Fields{"UpdateTransaction": err}).Warn("")
		return &pb.TransactionRequest{}, err
	}
	return transReq, nil
}

func (s *RentauService) CheckTransactionsForUpdate(ctx context.Context, transFilter *pb.TransactionFilter) (*pb.AllTransactionResponse, error) {
	userCompany, err := s.getUserFromContext(ctx)
	if err != nil {
		log.WithFields(log.Fields{"CheckTransactionsForUpdate": err}).Warn("")
		return &pb.AllTransactionResponse{}, err
	}

	transactions, err := s.dbManager.AllUpdatedTransactions(transFilter, userCompany.CompanyId)
	if err != nil {
		log.WithFields(log.Fields{"CheckTransactionsForUpdate": err}).Warn("")
		return &pb.AllTransactionResponse{}, err
	}

	allTransResponse := new(pb.AllTransactionResponse)
	allTransResponse.TransactionRequests = transactions

	return allTransResponse, nil
}

func (s *RentauService) AllTransactionsForInitial(ctx context.Context, transFilter *pb.TransactionFilter) (*pb.AllTransactionResponse, error) {
	userCompany, err := s.getUserFromContext(ctx)
	if err != nil {
		log.WithFields(log.Fields{"CheckTransactionsForUpdate": err}).Warn("")
		return &pb.AllTransactionResponse{}, err
	}

	transactions, err := s.dbManager.AllTransactionsForFilter(transFilter, userCompany.CompanyId)
	if err != nil {
		log.WithFields(log.Fields{"CheckTransactionsForUpdate": err}).Warn("")
		return &pb.AllTransactionResponse{}, err
	}

	allTransResponse := new(pb.AllTransactionResponse)
	allTransResponse.TransactionRequests = transactions

	return allTransResponse, nil
}

// Order
func (s *RentauService) UpdateOrder(ctx context.Context, orderReq *pb.OrderRequest) (*pb.OrderRequest, error) {
	userCompany, err := s.getUserFromContext(ctx)
	if err != nil {
		log.WithFields(log.Fields{"UpdateOrder": err}).Warn("")
		return &pb.OrderRequest{}, err
	}
	_, err = s.dbManager.UpdateOrder(orderReq, userCompany.CompanyId)
	if err != nil {
		log.WithFields(log.Fields{"UpdateOrder": err}).Warn("")
		return &pb.OrderRequest{}, err
	}
	return orderReq, nil
}

func (s *RentauService) createOrderResponse(orders []*pb.OrderRequest) (*pb.AllOrderResponse, error) {
	createOrderRequests := make([]*pb.CreateOrderRequest, 0)
	for _, order := range orders {
		createOrderRequest := new(pb.CreateOrderRequest)
		createOrderRequest.Order = order

		payment, error := models.PaymentForOrder(s.dbManager.DB, order.PaymentId)
		if error != nil {
			break
			return &pb.AllOrderResponse{}, error
		}
		createOrderRequest.Payment = payment

		transaction, error := models.TransactionForOrder(s.dbManager.DB, order.OrderId)
		if error != nil {
			log.WithFields(log.Fields{"TransactionForOrder": error}).Warn("")
		}
		if transaction != nil {
			createOrderRequest.Transaction = transaction
		}

		orderDetails, error := models.AllOrderDetailsForOrder(s.dbManager.DB, order.OrderId)
		if error != nil {
			log.WithFields(log.Fields{"AllOrderDetailsForOrder": error}).Warn("")
		}
		createOrderRequest.OrderDetails = orderDetails

		createOrderRequests = append(createOrderRequests, createOrderRequest)
	}

	allOrderResponse := new(pb.AllOrderResponse)
	allOrderResponse.OrderRequests = createOrderRequests
	return allOrderResponse, nil
}

func (s *RentauService) AllOrdersForInitial(ctx context.Context, orderFilter *pb.OrderFilter) (*pb.AllOrderResponse, error) {
	userCompany, err := s.getUserFromContext(ctx)
	if err != nil {
		log.WithFields(log.Fields{"AllOrdersForInitial": err}).Warn("")
		return &pb.AllOrderResponse{}, err
	}

	orders, err := s.dbManager.AllOrdersForInitial(orderFilter, userCompany.CompanyId)
	if err != nil {
		log.WithFields(log.Fields{"AllOrdersForInitial": err}).Warn("")
		return &pb.AllOrderResponse{}, err
	}

	allTransResponse, err := s.createOrderResponse(orders)
	if err != nil {
		log.WithFields(log.Fields{"AllOrdersForInitial": err}).Warn("")
		return &pb.AllOrderResponse{}, err
	}
	return allTransResponse, nil
}

func (s *RentauService) CheckOrdersForUpdate(ctx context.Context, orderFilter *pb.OrderFilter) (*pb.AllOrderResponse, error) {
	userCompany, err := s.getUserFromContext(ctx)
	if err != nil {
		log.WithFields(log.Fields{"CheckOrdersForUpdate": err}).Warn("")
		return &pb.AllOrderResponse{}, err
	}

	orders, err := s.dbManager.AllUpdatedOrders(orderFilter, userCompany.CompanyId)
	if err != nil {
		log.WithFields(log.Fields{"CheckOrdersForUpdate": err}).Warn("")
		return &pb.AllOrderResponse{}, err
	}

	allTransResponse, err := s.createOrderResponse(orders)
	if err != nil {
		log.WithFields(log.Fields{"CheckOrdersForUpdate": err}).Warn("")
		return &pb.AllOrderResponse{}, err
	}
	return allTransResponse, nil
}

func (s *RentauService) UpdateStream(stream pb.RentauService_UpdateStreamServer) error {

	ctx := stream.Context()
	_, err := s.getUserFromContext(ctx)
	if err != nil {
		log.WithFields(log.Fields{"UpdateStream": err}).Warn("")
		return err
	}

	for {
		stickyNoteReq, err := stream.Recv()
		if err == io.EOF {
			return nil
		}

		if err != nil {
			return err
		}

		resp := &pb.StickyNoteResponse{}
		resp.Message = stickyNoteReq.Message

		if err := stream.Send(resp); err != nil {
			return err
		}
	}

	return nil
}

func (s *RentauService) CreateOrder(ctx context.Context, createOrdReq *pb.CreateOrderRequest) (*pb.CreateOrderRequest, error) {

	syncF := func() {

		userCompany, err := s.getUserFromContext(ctx)
		if err != nil {
			s.sendError(err)
		}

		tx := s.dbManager.DB.MustBegin()

		// payment
		paymentSerial, err := models.StorePayment(tx, createOrdReq.Payment)
		if err != nil {
			tx.Rollback()
			s.sendError(err)
		}

		createOrdReq.Payment.PaymentId = paymentSerial
		createOrdReq.Order.PaymentId = paymentSerial

		// order
		orderSerial, err := models.StoreOrder(tx, createOrdReq.Order, userCompany.CompanyId)
		if err != nil {
			tx.Rollback()
			s.sendError(err)
		}

		createOrdReq.Order.OrderId = orderSerial
		if createOrdReq.Transaction != nil {
			createOrdReq.Transaction.OrderId = orderSerial
		}

		// transaction
		if createOrdReq.Transaction != nil {
			transactionSerial, err := models.StoreTransaction(tx, createOrdReq.Transaction, userCompany.CompanyId)
			if err != nil {
				tx.Rollback()
				s.sendError(err)
			}
			createOrdReq.Transaction.TransactionId = transactionSerial
		}

		// order document, to update customer/supplier balance
		// also update product amount in stock
		var orderDocument string = ""
		if createOrdReq.Order.OrderDocument == 0 {
			orderDocument = ".none"

		} else if createOrdReq.Order.OrderDocument == 1000 {
			orderDocument = ".productOrderSaledToCustomer"

			// customer balance
			acountReq, err := models.AccountForCustomer(s.dbManager.DB, createOrdReq.Order.CustomerId)
			if err != nil {
				tx.Rollback()
				s.sendError(err)
			}
			acountReq.Balance = acountReq.Balance + createOrdReq.Payment.TotalPriceWithDiscount
			models.UpdateCustomerBalance(tx, createOrdReq.Order.CustomerId, acountReq.Balance)

			// product amount
			_, error := models.DecreaseProductsInStock(s.dbManager.DB, tx, createOrdReq.OrderDetails)
			if error != nil {
				tx.Rollback()
				s.sendError(err)
			}

		} else if createOrdReq.Order.OrderDocument == 2000 {
			orderDocument = ".productOrderSaleEditedToCustomer"

			// customer balance
			acountReq, err := models.AccountForCustomer(s.dbManager.DB, createOrdReq.Order.CustomerId)
			if err != nil {
				tx.Rollback()
				s.sendError(err)
			}
			acountReq.Balance = acountReq.Balance - createOrdReq.Payment.TotalPriceWithDiscount
			models.UpdateCustomerBalance(tx, createOrdReq.Order.CustomerId, acountReq.Balance)

			// product amount
			_, error := models.IncreaseProductsInStock(s.dbManager.DB, tx, createOrdReq.OrderDetails)
			if error != nil {
				tx.Rollback()
				s.sendError(err)
			}

		} else if createOrdReq.Order.OrderDocument == 3000 {
			orderDocument = ".productOrderReceivedFromSupplier"

			// supplier balance
			acountReq, err := models.AccountForSupplier(s.dbManager.DB, createOrdReq.Order.SupplierId)
			if err != nil {
				tx.Rollback()
				s.sendError(err)
			}
			acountReq.Balance = acountReq.Balance - createOrdReq.Payment.TotalPriceWithDiscount
			models.UpdateSupplierBalance(tx, createOrdReq.Order.SupplierId, acountReq.Balance)

			// product amount
			_, error := models.IncreaseProductsInStock(s.dbManager.DB, tx, createOrdReq.OrderDetails)
			if error != nil {
				tx.Rollback()
				s.sendError(err)
			}

		} else if createOrdReq.Order.OrderDocument == 4000 {
			orderDocument = ".productOrderReceiveEditedFromSupplier"

			// supplier balance
			acountReq, err := models.AccountForSupplier(s.dbManager.DB, createOrdReq.Order.SupplierId)
			if err != nil {
				tx.Rollback()
				s.sendError(err)
			}
			acountReq.Balance = acountReq.Balance + createOrdReq.Payment.TotalPriceWithDiscount
			models.UpdateSupplierBalance(tx, createOrdReq.Order.SupplierId, acountReq.Balance)

			// product amount
			_, error := models.DecreaseProductsInStock(s.dbManager.DB, tx, createOrdReq.OrderDetails)
			if error != nil {
				tx.Rollback()
				s.sendError(err)
			}

		} else if createOrdReq.Order.OrderDocument == 5000 {
			orderDocument = ".productReturnedFromCustomer"

			// customer balance
			acountReq, err := models.AccountForCustomer(s.dbManager.DB, createOrdReq.Order.CustomerId)
			if err != nil {
				tx.Rollback()
				s.sendError(err)
			}
			acountReq.Balance = acountReq.Balance - createOrdReq.Payment.TotalPriceWithDiscount
			models.UpdateCustomerBalance(tx, createOrdReq.Order.CustomerId, acountReq.Balance)

			// product amount
			_, error := models.IncreaseProductsInStock(s.dbManager.DB, tx, createOrdReq.OrderDetails)
			if error != nil {
				tx.Rollback()
				s.sendError(err)
			}

		} else if createOrdReq.Order.OrderDocument == 6000 {
			orderDocument = ".productReturneEditedFromCustomer"

		} else if createOrdReq.Order.OrderDocument == 5500 {
			orderDocument = ".productReturnedToSupplier"

			// supplier balance
			acountReq, err := models.AccountForSupplier(s.dbManager.DB, createOrdReq.Order.SupplierId)
			if err != nil {
				tx.Rollback()
				s.sendError(err)
			}
			acountReq.Balance = acountReq.Balance + createOrdReq.Payment.TotalPriceWithDiscount
			models.UpdateSupplierBalance(tx, createOrdReq.Order.SupplierId, acountReq.Balance)

			// product amount
			_, error := models.DecreaseProductsInStock(s.dbManager.DB, tx, createOrdReq.OrderDetails)
			if error != nil {
				tx.Rollback()
				s.sendError(err)
			}

		} else if createOrdReq.Order.OrderDocument == 6600 {
			orderDocument = ".productReturneEditedToSupplier"

		} else if createOrdReq.Order.OrderDocument == 7000 {
			orderDocument = ".moneyReceived"

			if createOrdReq.Order.CustomerId > 0 {

				// customer balance
				acountReq, err := models.AccountForCustomer(s.dbManager.DB, createOrdReq.Order.CustomerId)
				if err != nil {
					tx.Rollback()
					s.sendError(err)
				}
				acountReq.Balance = acountReq.Balance - createOrdReq.Payment.TotalPriceWithDiscount
				models.UpdateCustomerBalance(tx, createOrdReq.Order.CustomerId, acountReq.Balance)

			} else if createOrdReq.Order.SupplierId > 0 {

				// supplier balance
				acountReq, err := models.AccountForSupplier(s.dbManager.DB, createOrdReq.Order.SupplierId)
				if err != nil {
					tx.Rollback()
					s.sendError(err)
				}
				acountReq.Balance = acountReq.Balance - createOrdReq.Payment.TotalPriceWithDiscount
				models.UpdateSupplierBalance(tx, createOrdReq.Order.SupplierId, acountReq.Balance)
			}
		} else if createOrdReq.Order.OrderDocument == 8000 {
			orderDocument = ".moneyReceiveEdited"

			if createOrdReq.Order.CustomerId > 0 {

				// customer balance
				acountReq, err := models.AccountForCustomer(s.dbManager.DB, createOrdReq.Order.CustomerId)
				if err != nil {
					tx.Rollback()
					s.sendError(err)
				}
				acountReq.Balance = acountReq.Balance + createOrdReq.Payment.TotalPriceWithDiscount
				models.UpdateCustomerBalance(tx, createOrdReq.Order.CustomerId, acountReq.Balance)

			} else if createOrdReq.Order.SupplierId > 0 {

				// supplier balance
				acountReq, err := models.AccountForSupplier(s.dbManager.DB, createOrdReq.Order.SupplierId)
				if err != nil {
					tx.Rollback()
					s.sendError(err)
				}
				acountReq.Balance = acountReq.Balance + createOrdReq.Payment.TotalPriceWithDiscount
				models.UpdateSupplierBalance(tx, createOrdReq.Order.SupplierId, acountReq.Balance)
			}

		} else if createOrdReq.Order.OrderDocument == 10000 {
			orderDocument = ".moneyGone"

			if createOrdReq.Order.CustomerId > 0 {
				// customer balance
				acountReq, err := models.AccountForCustomer(s.dbManager.DB, createOrdReq.Order.CustomerId)
				if err != nil {
					tx.Rollback()
					s.sendError(err)
				}
				acountReq.Balance = acountReq.Balance + createOrdReq.Payment.TotalPriceWithDiscount
				models.UpdateCustomerBalance(tx, createOrdReq.Order.CustomerId, acountReq.Balance)

			} else if createOrdReq.Order.SupplierId > 0 {
				// supplier balance
				acountReq, err := models.AccountForSupplier(s.dbManager.DB, createOrdReq.Order.SupplierId)
				if err != nil {
					tx.Rollback()
					s.sendError(err)
				}
				acountReq.Balance = acountReq.Balance + createOrdReq.Payment.TotalPriceWithDiscount
				models.UpdateSupplierBalance(tx, createOrdReq.Order.SupplierId, acountReq.Balance)
			}

		} else if createOrdReq.Order.OrderDocument == 11000 {
			orderDocument = ".moneyGoneEdited"

			if createOrdReq.Order.CustomerId > 0 {
				// customer balance
				acountReq, err := models.AccountForCustomer(s.dbManager.DB, createOrdReq.Order.CustomerId)
				if err != nil {
					tx.Rollback()
					s.sendError(err)
				}
				acountReq.Balance = acountReq.Balance - createOrdReq.Payment.TotalPriceWithDiscount
				models.UpdateCustomerBalance(tx, createOrdReq.Order.CustomerId, acountReq.Balance)

			} else if createOrdReq.Order.SupplierId > 0 {
				// supplier balance
				acountReq, err := models.AccountForSupplier(s.dbManager.DB, createOrdReq.Order.SupplierId)
				if err != nil {
					tx.Rollback()
					s.sendError(err)
				}
				acountReq.Balance = acountReq.Balance - createOrdReq.Payment.TotalPriceWithDiscount
				models.UpdateSupplierBalance(tx, createOrdReq.Order.SupplierId, acountReq.Balance)
			}

		} else if createOrdReq.Order.OrderDocument == 12000 {
			orderDocument = "customerMadePreOrder"
		} else if createOrdReq.Order.OrderDocument == 13000 {
			orderDocument = "stokTaking"
		} else if createOrdReq.Order.OrderDocument == 21000 {
			orderDocument = "rawProductGoneToProd"

			// product amount
			_, error := models.DecreaseProductsInStock(s.dbManager.DB, tx, createOrdReq.OrderDetails)
			if error != nil {
				tx.Rollback()
				s.sendError(err)
			}
		} else if createOrdReq.Order.OrderDocument == 22000 {
			orderDocument = "rawProductGoneToProdEdited"

			// product amount
			_, error := models.IncreaseProductsInStock(s.dbManager.DB, tx, createOrdReq.OrderDetails)
			if error != nil {
				tx.Rollback()
				s.sendError(err)
			}

		} else if createOrdReq.Order.OrderDocument == 23000 {
			orderDocument = "readyProductReceivedFromProd"

			// product amount
			_, error := models.IncreaseProductsInStock(s.dbManager.DB, tx, createOrdReq.OrderDetails)
			if error != nil {
				tx.Rollback()
				s.sendError(err)
			}

		} else if createOrdReq.Order.OrderDocument == 24000 {
			orderDocument = "readyProductReceivedFromProdEdited"

			// product amount
			_, error := models.DecreaseProductsInStock(s.dbManager.DB, tx, createOrdReq.OrderDetails)
			if error != nil {
				tx.Rollback()
				s.sendError(err)
			}

		} else if createOrdReq.Order.OrderDocument == 25000 {
			orderDocument = "incomeProductFromStock"

			// product amount
			_, error := models.IncreaseProductsInStock(s.dbManager.DB, tx, createOrdReq.OrderDetails)
			if error != nil {
				tx.Rollback()
				s.sendError(err)
			}

		} else if createOrdReq.Order.OrderDocument == 26000 {
			orderDocument = "incomeProductFromStockEdited"

			// product amount
			_, error := models.DecreaseProductsInStock(s.dbManager.DB, tx, createOrdReq.OrderDetails)
			if error != nil {
				tx.Rollback()
				s.sendError(err)
			}

		} else if createOrdReq.Order.OrderDocument == 27000 {
			orderDocument = "removeProducts"

			// product amount
			_, error := models.DecreaseProductsInStock(s.dbManager.DB, tx, createOrdReq.OrderDetails)
			if error != nil {
				tx.Rollback()
				s.sendError(err)
			}

		} else if createOrdReq.Order.OrderDocument == 28000 {
			orderDocument = "removeProductsEdited"

			// product amount
			_, error := models.IncreaseProductsInStock(s.dbManager.DB, tx, createOrdReq.OrderDetails)
			if error != nil {
				tx.Rollback()
				s.sendError(err)
			}

		} else if createOrdReq.Order.OrderDocument == 29000 {
			orderDocument = "productMovememntToStock"

			// product amount
			_, error := models.DecreaseProductsInStock(s.dbManager.DB, tx, createOrdReq.OrderDetails)
			if error != nil {
				tx.Rollback()
				s.sendError(err)
			}

		} else if createOrdReq.Order.OrderDocument == 30000 {
			orderDocument = "productMovememntToStockEdited"

			// product amount
			_, error := models.IncreaseProductsInStock(s.dbManager.DB, tx, createOrdReq.OrderDetails)
			if error != nil {
				tx.Rollback()
				s.sendError(err)
			}
		}

		// orderDetails
		for _, orderDetailReq := range createOrdReq.OrderDetails {
			orderDetailReq.OrderId = orderSerial

			orderDetailSerial, err := models.StoreOrderDetails(tx, orderDetailReq, userCompany.CompanyId)
			if err != nil {
				break
				tx.Rollback()
				s.sendError(err)

			}

			orderDetailReq.OrderDetailId = orderDetailSerial
		}

		contexErr := ctx.Err()
		if contexErr != nil {
			tx.Rollback()
			s.sendError(err)
		}

		err = tx.Commit()
		if err != nil {
			tx.Rollback()
			s.sendError(err)
		}

		log.WithFields(log.Fields{"orderDocument ": orderDocument}).Info("")
		s.sendObj(createOrdReq)
	}

	s.syncChannel <- syncF

	select {
	case obj := <-s.responseChannel:
		s, ok := obj.(*pb.CreateOrderRequest)
		if !ok {
			return &pb.CreateOrderRequest{}, errors.New("Fail casting")
		} else {
			return s, nil
		}
	case err := <-s.errorChannel:
		return &pb.CreateOrderRequest{}, err
	}
}
