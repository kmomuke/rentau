package services
import (
	"testing"
	"golang.org/x/crypto/bcrypt"
	"github.com/stretchr/testify/assert"
	"fmt"
)

func TestBcrypt(t *testing.T) {
	pass, err := bcrypt.GenerateFromPassword([]byte("ruslan"), bcrypt.DefaultCost)
	if err != nil {
		fmt.Println(err)
	}
	assert.Nil(t, err, "error should be nil")
	if assert.NotNil(t, pass) {
		fmt.Println(string(pass))
		//$2a$10$.g8kW.nw2jxU8H5KNN0vquTpAj5PwZNgjlWDvD9.36M2QTlCG4ol6
	}
}