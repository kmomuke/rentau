package dbManagers

import (
	log "github.com/Sirupsen/logrus"
	"github.com/jmoiron/sqlx"
	pb "bitbucket.org/kmomuke/rentau/grpc/proto"
	"bitbucket.org/kmomuke/rentau/grpc/dbManagers/models"
)

type DatabaseInterface interface {
	followChannel()
	CreateUser(user *pb.UserRequest, companyId uint64) (uint64, error)
	UpdateUser(user *pb.UserRequest, companyId uint64) (uint64, error)
	AllUsers(companyId uint64) ([]*pb.UserRequest, error)
}

type DbManager struct {
	DB *sqlx.DB
	dbChannel chan func()
	responseChannel chan uint64
	errorChannel chan error
}

func NewDbManager(path string) *DbManager {

	db, err := sqlx.Connect("postgres", path)
	if err != nil {
		log.Fatalln(err)
	}

	dbM := new(DbManager)
	dbM.DB = db
	dbM.dbChannel = make(chan func(), 1000)
	dbM.responseChannel = make(chan uint64, 1000)
	dbM.errorChannel = make(chan error, 1000)
	dbM.followChannel()
	return dbM
}

//----------------------------- PRIVATE METHODS ------------------------------------------
func (dbM *DbManager) followChannel() {
	go func() {
		for f := range dbM.dbChannel {
			f()
		}
	}()
}

func (dbM *DbManager) sendLastInsertedId(lastInsertId uint64) {
	go func() {
		dbM.responseChannel <- lastInsertId
	}()
}

func (dbM *DbManager) sendError(err error) {
	if err != nil {
		log.WithFields(log.Fields{ "error": err}).Fatal("SendError")
		go func() {
			dbM.errorChannel <- err
		}()
	}
}

func (dbM *DbManager) waitForLastInsertedId() (uint64, error) {
	select {
	case lastInsertedId := <- dbM.responseChannel:
		return lastInsertedId, nil
	case err := <- dbM.errorChannel:
		return 0, err
	}
}


//---------------------------------- Company -------------------------------------------
func (dbM *DbManager) CreateCompany(company *pb.CompanyRequest) (uint64, error) {
	f := func() {
		tx := dbM.DB.MustBegin()

		lastInsertId, err := models.StoreCompany(tx, company)
		if err != nil {
			dbM.sendError(err)
		}

		err = tx.Commit()
		if err != nil {
			dbM.sendError(err)
		}

		dbM.sendLastInsertedId(lastInsertId)
	}

	dbM.dbChannel <- f

	return dbM.waitForLastInsertedId()
}

func (dbM *DbManager) UpdateCompany(company *pb.CompanyRequest) (uint64, error) {
	f := func() {
		tx := dbM.DB.MustBegin()

		rowsAffected, err := models.UpdateCompany(tx, company)
		if err != nil {
			dbM.sendError(err)
		}

		err = tx.Commit()
		if err != nil {
			dbM.sendError(err)
		}

		dbM.sendLastInsertedId(rowsAffected)
	}

	dbM.dbChannel <- f

	return dbM.waitForLastInsertedId()
}

func (dbM *DbManager) CompanyFor(companyId uint64) (*pb.CompanyRequest, error) {
	company, err := models.CompanyFor(dbM.DB, companyId)
	if err != nil {
		return nil, err
	}
	return company, nil
}

//---------------------------------- Account -------------------------------------------
func (dbM *DbManager) CreateAccount(account *pb.AccountRequest) (uint64, error) {
	f := func() {
		tx := dbM.DB.MustBegin()

		lastInsertId, err := models.StoreAccount(tx, account)
		if err != nil {
			dbM.sendError(err)
		}

		err = tx.Commit()
		if err != nil {
			dbM.sendError(err)
		}

		dbM.sendLastInsertedId(lastInsertId)
	}

	dbM.dbChannel <- f

	return dbM.waitForLastInsertedId()
}

func (dbM *DbManager) AccountForCustomer(customerId uint64) (*pb.AccountRequest, error) {
	account, err := models.AccountForCustomer(dbM.DB, customerId)
	if err != nil {
		return nil, err
	}
	return account, nil
}

func (dbM *DbManager) AccountForSupplier(supplierId uint64) (*pb.AccountRequest, error) {
	account, err := models.AccountForSupplier(dbM.DB, supplierId)
	if err != nil {
		return nil, err
	}
	return account, nil
}

//---------------------------------- USER -------------------------------------------
func (dbM *DbManager) CreateUser(user *pb.UserRequest, companyId uint64) (uint64, error) {
	f := func() {
		tx := dbM.DB.MustBegin()

		lastInsertId, err := models.StoreUser(tx, user, companyId)
		if err != nil {
			dbM.sendError(err)
		}

		err = tx.Commit()
		if err != nil {
			dbM.sendError(err)
		}

		dbM.sendLastInsertedId(lastInsertId)
	}

	dbM.dbChannel <- f

	return dbM.waitForLastInsertedId()
}

func (dbM *DbManager) UpdateUser(user *pb.UserRequest, companyId uint64) (uint64, error) {
	f := func() {
		tx := dbM.DB.MustBegin()

		rowsAffected, err := models.UpdateUser(tx, user, companyId)
		if err != nil {
			dbM.sendError(err)
		}

		err = tx.Commit()
		if err != nil {
			dbM.sendError(err)
		}

		dbM.sendLastInsertedId(rowsAffected)
	}

	dbM.dbChannel <- f

	return dbM.waitForLastInsertedId()
}

func (dbM *DbManager) AllUsers(companyId uint64) ([]*pb.UserRequest, error) {
	users, err := models.AllUsersForCompany(dbM.DB,companyId)
	if err != nil {
		return nil, err
	}
	return users, nil
}

func (dbM *DbManager) AllUsersForUpdate(filter *pb.UserFilter, companyId uint64) ([]*pb.UserRequest, error) {
	users, err := models.AllUsersForUpdate(dbM.DB, filter, companyId)
	if err != nil {
		return nil, err
	}
	return users, nil
}


func (dbM *DbManager) UserForEmail(email string) (*pb.UserRequest, error) {
	user, err := models.UserForEmail(dbM.DB, email)
	if err != nil {
		return nil, err
	}
	return user, nil
}

func (dbM *DbManager) UserForUUID(uuid string) (*pb.UserRequest, error) {
	user, err := models.UserForUUID(dbM.DB, uuid)
	if err != nil {
		return nil, err
	}
	return user, nil
}

func (dbM *DbManager) UserCompanyIdForUserUUID(uuid string) (uint64, error) {
	user, err := models.UserForUUID(dbM.DB, uuid)
	if err != nil {
		return 0, err
	}
	return user.CompanyId, nil
}

//---------------------------------- CATEGORY -------------------------------------------
func (dbM *DbManager) CreateCategory(category *pb.CategoryRequest, companyId uint64) (uint64, error) {
	f := func() {
		tx := dbM.DB.MustBegin()

		lastInsertId, err := models.StoreCategory(tx, category, companyId)
		if err != nil {
			dbM.sendError(err)
		}

		err = tx.Commit()
		if err != nil {
			dbM.sendError(err)
		}

		dbM.sendLastInsertedId(lastInsertId)
	}
	dbM.dbChannel <- f
	return dbM.waitForLastInsertedId()
}

func (dbM *DbManager) UpdateCategory(category *pb.CategoryRequest, companyId uint64) (uint64, error) {
	f := func() {
		tx := dbM.DB.MustBegin()

		rowsAffected, err := models.UpdateCategory(tx, category, companyId)
		if err != nil {
			dbM.sendError(err)
		}

		err = tx.Commit()
		if err != nil {
			dbM.sendError(err)
		}

		dbM.sendLastInsertedId(rowsAffected)
	}
	dbM.dbChannel <- f
	return dbM.waitForLastInsertedId()
}

func (dbM *DbManager) AllCategoriesForInitial(companyId uint64) ([]*pb.CategoryRequest, error) {
	categories, err := models.AllCategoriesForCompany(dbM.DB, companyId)
	if err != nil {
		return nil, err
	}
	return categories, nil
}

func (dbM *DbManager) AllUpdatedCategories(filter *pb.CategoryFilter, companyId uint64) ([]*pb.CategoryRequest, error) {
	categories, err := models.AllUpdatedCategories(dbM.DB, filter, companyId)
	if err != nil {
		return nil, err
	}
	return categories, nil
}

//---------------------------------- PRODUCT -------------------------------------------
func (dbM *DbManager) CreateProduct(product *pb.ProductRequest, companyId uint64) (uint64, error) {
	f := func() {
		tx := dbM.DB.MustBegin()

		lastInsertId, err := models.StoreProduct(tx, product, companyId)
		if err != nil {
			dbM.sendError(err)
		}

		err = tx.Commit()
		if err != nil {
			dbM.sendError(err)
		}

		dbM.sendLastInsertedId(lastInsertId)
	}
	dbM.dbChannel <- f
	return dbM.waitForLastInsertedId()
}

func (dbM *DbManager) UpdateProduct(product *pb.ProductRequest, companyId uint64) (uint64, error) {
	f := func() {
		tx := dbM.DB.MustBegin()

		rowsAffected, err := models.UpdateProduct(tx, product, companyId)
		if err != nil {
			dbM.sendError(err)
		}

		err = tx.Commit()
		if err != nil {
			dbM.sendError(err)
		}

		dbM.sendLastInsertedId(rowsAffected)
	}
	dbM.dbChannel <- f
	return dbM.waitForLastInsertedId()
}

func (dbM *DbManager) IncreaseProductsInStock(orderDetailReqs []*pb.OrderDetailRequest) (uint64, error) {
	f := func() {
		tx := dbM.DB.MustBegin()

		rowsAffected, err := models.IncreaseProductsInStock(dbM.DB, tx, orderDetailReqs)
		if err != nil {
			dbM.sendError(err)
		}

		err = tx.Commit()
		if err != nil {
			dbM.sendError(err)
		}

		dbM.sendLastInsertedId(rowsAffected)
	}
	dbM.dbChannel <- f
	return dbM.waitForLastInsertedId()
}

func (dbM *DbManager) DecreaseProductsInStock(orderDetailReqs []*pb.OrderDetailRequest) (uint64, error) {
	f := func() {
		tx := dbM.DB.MustBegin()

		rowsAffected, err := models.DecreaseProductsInStock(dbM.DB, tx, orderDetailReqs)
		if err != nil {
			dbM.sendError(err)
		}

		err = tx.Commit()
		if err != nil {
			dbM.sendError(err)
		}

		dbM.sendLastInsertedId(rowsAffected)
	}
	dbM.dbChannel <- f
	return dbM.waitForLastInsertedId()
}

func (dbM *DbManager) AllProductsForInitial(companyId uint64) ([]*pb.ProductRequest, error) {
	products, err := models.AllProductsForCompany(dbM.DB, companyId)
	if err != nil {
		return nil, err
	}
	return products, nil
}

func (dbM *DbManager) AllUpdatedProducts(filter *pb.ProductFilter, companyId uint64) ([]*pb.ProductRequest, error) {
	products, err := models.AllProductsForUpdate(dbM.DB, filter, companyId)
	if err != nil {
		return nil, err
	}
	return products, nil
}

//---------------------------------- OrderDetail -------------------------------------------
func (dbM *DbManager) CreateOrderDetail(orderDetail *pb.OrderDetailRequest, companyId uint64) (uint64, error) {
	f := func() {
		tx := dbM.DB.MustBegin()

		lastInsertId, err := models.StoreOrderDetails(tx, orderDetail, companyId)
		if err != nil {
			dbM.sendError(err)
		}

		err = tx.Commit()
		if err != nil {
			dbM.sendError(err)
		}

		dbM.sendLastInsertedId(lastInsertId)
	}
	dbM.dbChannel <- f
	return dbM.waitForLastInsertedId()
}

func (dbM *DbManager) AllOrderDetails(orderDetFilter *pb.OrderDetailFilter, companyId uint64) ([]*pb.OrderDetailRequest, error) {
	orderDetails, err := models.AllOrderDetailsForFilter(dbM.DB, orderDetFilter, companyId)
	if err != nil {
		return nil, err
	}
	return orderDetails, nil
}

func (dbM *DbManager) AllUpdatedOrderDetails(orderDetFilter *pb.OrderDetailFilter, companyId uint64) ([]*pb.OrderDetailRequest, error) {
	orderDetails, err := models.AllUpdatedOrderDetailsForFilter(dbM.DB, orderDetFilter, companyId)
	if err != nil {
		return nil, err
	}
	return orderDetails, nil
}

//---------------------------------- Customer -------------------------------------------
func (dbM *DbManager) CreateCustomer(customer *pb.CustomerRequest, companyId uint64) (uint64, error) {
	f := func() {
		tx := dbM.DB.MustBegin()

		lastInsertId, err := models.StoreCustomer(tx, customer, companyId)
		if err != nil {
			dbM.sendError(err)
		}

		err = tx.Commit()
		if err != nil {
			dbM.sendError(err)
		}

		dbM.sendLastInsertedId(lastInsertId)
	}
	dbM.dbChannel <- f
	return dbM.waitForLastInsertedId()
}

func (dbM *DbManager) UpdateCustomer(customer *pb.CustomerRequest, companyId uint64) (uint64, error) {
	f := func() {
		tx := dbM.DB.MustBegin()

		rowsAffected, err := models.UpdateCustomer(tx, customer, companyId)
		if err != nil {
			dbM.sendError(err)
		}

		err = tx.Commit()
		if err != nil {
			dbM.sendError(err)
		}

		dbM.sendLastInsertedId(rowsAffected)
	}
	dbM.dbChannel <- f
	return dbM.waitForLastInsertedId()
}

func (dbM *DbManager) UpdateCustomerBallance(customerId uint64, balance float64) (uint64, error) {
	f := func() {
		tx := dbM.DB.MustBegin()

		rowsAffected, err := models.UpdateCustomerBalance(tx, customerId, balance)
		if err != nil {
			dbM.sendError(err)
		}

		err = tx.Commit()
		if err != nil {
			dbM.sendError(err)
		}

		dbM.sendLastInsertedId(rowsAffected)
	}
	dbM.dbChannel <- f
	return dbM.waitForLastInsertedId()
}


func (dbM *DbManager) AllCustomersForInitial(companyId uint64) ([]*pb.CustomerRequest, error) {
	customers, err := models.AllCustomersForCompany(dbM.DB, companyId)
	if err != nil {
		return nil, err
	}
	return customers, nil
}

func (dbM *DbManager) AllUpdatedCustomers(custFilter *pb.CustomerFilter, companyId uint64) ([]*pb.CustomerRequest, error) {
	customers, err := models.AllUpdatedCustomers(dbM.DB, custFilter, companyId)
	if err != nil {
		return nil, err
	}
	return customers, nil
}

//---------------------------------- Supplier -------------------------------------------
func (dbM *DbManager) CreateSupplier(supplier *pb.SupplierRequest, companyId uint64) (uint64, error) {
	f := func() {
		tx := dbM.DB.MustBegin()

		lastInsertId, err := models.StoreSupplier(tx, supplier, companyId)
		if err != nil {
			dbM.sendError(err)
		}

		err = tx.Commit()
		if err != nil {
			dbM.sendError(err)
		}

		dbM.sendLastInsertedId(lastInsertId)
	}
	dbM.dbChannel <- f
	return dbM.waitForLastInsertedId()
}

func (dbM *DbManager) UpdateSupplier(supplier *pb.SupplierRequest, companyId uint64) (uint64, error) {
	f := func() {
		tx := dbM.DB.MustBegin()

		rowsAffected, err := models.UpdateSupplier(tx, supplier, companyId)
		if err != nil {
			dbM.sendError(err)
		}

		err = tx.Commit()
		if err != nil {
			dbM.sendError(err)
		}

		dbM.sendLastInsertedId(rowsAffected)
	}
	dbM.dbChannel <- f
	return dbM.waitForLastInsertedId()
}

func (dbM *DbManager) UpdateSupplierBallance(supplierId uint64, balance float64) (uint64, error) {
	f := func() {
		tx := dbM.DB.MustBegin()

		rowsAffected, err := models.UpdateSupplierBalance(tx, supplierId, balance)
		if err != nil {
			dbM.sendError(err)
		}

		err = tx.Commit()
		if err != nil {
			dbM.sendError(err)
		}

		dbM.sendLastInsertedId(rowsAffected)
	}
	dbM.dbChannel <- f
	return dbM.waitForLastInsertedId()
}


func (dbM *DbManager) AllSuppliersForInitial(companyId uint64) ([]*pb.SupplierRequest, error) {
	customers, err := models.AllSuppliersForCompany(dbM.DB, companyId)
	if err != nil {
		return nil, err
	}
	return customers, nil
}

func (dbM *DbManager) AllUpdatedSuppliers(suppFilter *pb.SupplierFilter, companyId uint64) ([]*pb.SupplierRequest, error) {
	customers, err := models.AllSuppliersForUpdate(dbM.DB, suppFilter, companyId)
	if err != nil {
		return nil, err
	}
	return customers, nil
}

//---------------------------------- Transaction -------------------------------------------
func (dbM *DbManager) StoreTransaction(transaction *pb.TransactionRequest, companyId uint64) (uint64, error) {
	f := func() {
		tx := dbM.DB.MustBegin()

		lastInsertId, err := models.StoreTransaction(tx, transaction, companyId)
		if err != nil {
			dbM.sendError(err)
		}

		err = tx.Commit()
		if err != nil {
			dbM.sendError(err)
		}

		dbM.sendLastInsertedId(lastInsertId)
	}
	dbM.dbChannel <- f
	return dbM.waitForLastInsertedId()
}

func (dbM *DbManager) UpdateTransaction(transaction *pb.TransactionRequest, companyId uint64) (uint64, error) {
	f := func() {
		tx := dbM.DB.MustBegin()

		rowsAffected, err := models.UpdateTransaction(tx, transaction, companyId)
		if err != nil {
			dbM.sendError(err)
		}

		err = tx.Commit()
		if err != nil {
			dbM.sendError(err)
		}

		dbM.sendLastInsertedId(rowsAffected)
	}
	dbM.dbChannel <- f
	return dbM.waitForLastInsertedId()
}

func (dbM *DbManager) RecentTransactionForCustomer(customerId uint64) (*pb.TransactionRequest, error) {
	transaction, err := models.RecentTransactionForCustomer(dbM.DB, customerId)
	if err != nil {
		return nil, err
	}
	return transaction, nil
}

func (dbM *DbManager) RecentTransactionForSupplier(supplierId uint64) (*pb.TransactionRequest, error) {
	transaction, err := models.RecentTransactionForSupplier(dbM.DB, supplierId)
	if err != nil {
		return nil, err
	}
	return transaction, nil
}

func (dbM *DbManager) AllTransactionsForFilter(filter *pb.TransactionFilter, companyId uint64) ([]*pb.TransactionRequest, error) {
	transactions, err := models.AllTransactionsForFilter(dbM.DB, filter, companyId)
	if err != nil {
		return nil, err
	}
	return transactions, nil
}

func (dbM *DbManager) AllUpdatedTransactions(filter *pb.TransactionFilter, companyId uint64) ([]*pb.TransactionRequest, error) {
	transactions, err := models.AllUpdatedTransactions(dbM.DB, filter, companyId)
	if err != nil {
		return nil, err
	}
	return transactions, nil
}

//---------------------------------- Payment -------------------------------------------
func (dbM *DbManager) CreatePayment(payment *pb.PaymentRequest) (uint64, error) {
	f := func() {
		tx := dbM.DB.MustBegin()

		lastInsertId, err := models.StorePayment(tx, payment)
		if err != nil {
			dbM.sendError(err)
		}

		err = tx.Commit()
		if err != nil {
			dbM.sendError(err)
		}

		dbM.sendLastInsertedId(lastInsertId)
	}
	dbM.dbChannel <- f
	return dbM.waitForLastInsertedId()
}

func (dbM *DbManager) PaymentFor(paymentId uint64) (*pb.PaymentRequest, error) {
	payment, err := models.PaymentForOrder(dbM.DB, paymentId)
	if err != nil {
		return nil, err
	}
	return payment, nil
}

//---------------------------------- Order -------------------------------------------
func (dbM *DbManager) CreateOrder(order *pb.OrderRequest, companyId uint64) (uint64, error) {
	f := func() {
		tx := dbM.DB.MustBegin()

		lastInsertId, err := models.StoreOrder(tx, order, companyId)
		if err != nil {
			dbM.sendError(err)
		}

		err = tx.Commit()
		if err != nil {
			dbM.sendError(err)
		}

		dbM.sendLastInsertedId(lastInsertId)
	}
	dbM.dbChannel <- f
	return dbM.waitForLastInsertedId()
}

func (dbM *DbManager) UpdateOrder(order *pb.OrderRequest, companyId uint64) (uint64, error) {
	f := func() {
		tx := dbM.DB.MustBegin()

		rowsAffected, err := models.UpdateOrder(tx, order, companyId)
		if err != nil {
			dbM.sendError(err)
		}

		err = tx.Commit()
		if err != nil {
			dbM.sendError(err)
		}

		dbM.sendLastInsertedId(rowsAffected)
	}
	dbM.dbChannel <- f
	return dbM.waitForLastInsertedId()
}

func (dbM *DbManager) AllOrdersForInitial(filter *pb.OrderFilter, companyId uint64) ([]*pb.OrderRequest, error) {
	orders, err := models.AllOrdersForFilter(dbM.DB, filter, companyId)
	if err != nil {
		return nil, err
	}
	return orders, nil
}

func (dbM *DbManager) AllUpdatedOrders(filter *pb.OrderFilter, companyId uint64) ([]*pb.OrderRequest, error) {
	orders, err := models.AllUpdatedOrders(dbM.DB, filter, companyId)
	if err != nil {
		return nil, err
	}
	return orders, nil
}