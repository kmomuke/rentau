ALTER TABLE payments ADD COLUMN plus_price REAL DEFAULT 0;
ALTER TABLE payments ADD COLUMN minus_price REAL DEFAULT 0;
ALTER TABLE payments ADD COLUMN comment varchar (400) DEFAULT '';
