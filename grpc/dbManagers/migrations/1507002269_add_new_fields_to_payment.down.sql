ALTER TABLE payments DROP COLUMN IF EXISTS plus_price;
ALTER TABLE payments DROP COLUMN IF EXISTS minus_price;
ALTER TABLE payments DROP COLUMN IF EXISTS comment;