package models

import (
	"github.com/jmoiron/sqlx"
	log "github.com/Sirupsen/logrus"
	pb "bitbucket.org/kmomuke/rentau/grpc/proto"
)

//type ProductRequest struct {
//	ProductId        uint64
//	ProductImagePath string
//	ProductName      string
//	SupplierId       uint64
//	CategoryId       uint64
//	Barcode          string
//	QuantityPerUnit  string
//	SaleUnitPrice    float64
//	IncomeUnitPrice  float64
//	UnitsInStock     float64
//}

type Product struct {
	productId uint64 `db:"product_id"`
	companyId uint64 `db:"company_id"`
	productImagePath string `db:"product_image_path"`
	productName string `db:"product_name"`
	supplierId uint64 `db:"supplier_id"`
	categoryId uint64 `db:"category_id"`
	barcode string `db:"barcode"`
	quantityPerUnit string `db:"quantity_per_unit"`
	saleUnitPrice float32 `db:"sale_unit_price"`
	incomeUnitPrice float32 `db:"income_unit_price"`
	unitsInStock float32 `db:"units_in_stock"`
}

//CREATE TABLE IF NOT EXISTS products (
//	product_id BIGSERIAL PRIMARY KEY NOT NULL,
//	company_id BIGINT,
//	product_image_path varchar (400),
//	product_name varchar (400),
//	supplier_id BIGINT,
//	category_id BIGINT,
//	barcode VARCHAR (300),
//	quantity_per_unit VARCHAR (300),
//	sale_unit_price REAL,
//	income_unit_price REAL,
//	units_in_stock REAL,
//	updated_at BIGINT
//);

func StoreProduct(tx *sqlx.Tx, product *pb.ProductRequest, companyId uint64) (uint64, error) {

	var lastInsertId uint64
	err := tx.QueryRow("INSERT INTO products " +
		"(product_image_path, " +
		"company_id, " +
		"product_name, " +
		"supplier_id, " +
		"category_id, " +
		"barcode, " +
		"quantity_per_unit, " +
		"sale_unit_price, " +
		"income_unit_price, " +
		"units_in_stock, " +
		"updated_at) " +
		"VALUES($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11) returning product_id;",
		product.ProductImagePath,
		companyId,
		product.ProductName,
		product.SupplierId,
		product.CategoryId,
		product.Barcode,
		product.QuantityPerUnit,
		product.SaleUnitPrice,
		product.IncomeUnitPrice,
		product.UnitsInStock,
		updatedAt()).Scan(&lastInsertId)

	if err != nil {
		return ErrorFunc(err)
	}

	return lastInsertId, nil
}

func UpdateProduct(tx *sqlx.Tx, product *pb.ProductRequest, companyId uint64) (uint64, error)  {

	stmt, err :=tx.Prepare("UPDATE products SET " +
		"product_image_path=$1, " +
		"company_id=$2, " +
		"product_name=$3, " +
		"supplier_id=$4, " +
		"category_id=$5, " +
		"barcode=$6, " +
		"quantity_per_unit=$7, " +
		"sale_unit_price=$8, " +
		"income_unit_price=$9, " +
		"units_in_stock=$10, " +
		"updated_at=$11 " +
		"WHERE product_id=$12")

	if err != nil {
		return ErrorFunc(err)
	}

	res, err := stmt.Exec(
		product.ProductImagePath,
		companyId,
		product.ProductName,
		product.SupplierId,
		product.CategoryId,
		product.Barcode,
		product.QuantityPerUnit,
		product.SaleUnitPrice,
		product.IncomeUnitPrice,
		product.UnitsInStock,
		updatedAt(),
		product.ProductId)

	if err != nil {
		return ErrorFunc(err)
	}

	affect, err := res.RowsAffected()
	if err != nil {
		return ErrorFunc(err)
	}

	return uint64(affect), nil
}

func IncreaseProductsInStock(db *sqlx.DB, tx *sqlx.Tx, orderDetailReqs []*pb.OrderDetailRequest)  (uint64, error)  {

	productIds := make([]uint64, 0)
	updateValues := make(map[uint64]float64)

	for _, orderDetailReq := range orderDetailReqs {
		value, ok := updateValues[orderDetailReq.ProductId]
		if ok {
			updateValues[orderDetailReq.ProductId] = orderDetailReq.OrderQuantity + value
		} else {
			productIds = append(productIds, orderDetailReq.ProductId)
			updateValues[orderDetailReq.ProductId] = orderDetailReq.OrderQuantity
		}
	}

	products_, err := getProductsForProductIds(db, productIds)
	if err != nil {
		print("error")
		return 0, err
	}

	forUpdatesInDatabase := make(map[uint64]float64)

	for _, product := range products_ {
		quantity := updateValues[product.ProductId]
		product.UnitsInStock = product.UnitsInStock + quantity
		forUpdatesInDatabase[product.ProductId] = product.UnitsInStock
	}

	rowsAffected, err := updateProductsInStock(tx, forUpdatesInDatabase)
	if err != nil {
		log.WithFields(log.Fields{"err": err}).Warn("")
		return 0, err
	}

	return rowsAffected, nil
}

func DecreaseProductsInStock(db *sqlx.DB, tx *sqlx.Tx, orderDetailReqs []*pb.OrderDetailRequest) (uint64, error) {

	productIds := make([]uint64, 0)
	updateValues := make(map[uint64]float64)

	for _, orderDetailReq := range orderDetailReqs {
		value, ok := updateValues[orderDetailReq.ProductId]
		if ok {
			updateValues[orderDetailReq.ProductId] = orderDetailReq.OrderQuantity + value
		} else {
			productIds = append(productIds, orderDetailReq.ProductId)
			updateValues[orderDetailReq.ProductId] = orderDetailReq.OrderQuantity
		}
	}

	products_, err := getProductsForProductIds(db, productIds)
	if err != nil {
		print("error")
		return 0, err
	}

	forUpdatesInDatabase := make(map[uint64]float64)

	for _, product := range products_ {
		quantity := updateValues[product.ProductId]
		product.UnitsInStock = product.UnitsInStock - quantity
		forUpdatesInDatabase[product.ProductId] = product.UnitsInStock
	}

	rowsAffected, err := updateProductsInStock(tx, forUpdatesInDatabase)
	if err != nil {
		tx.Rollback()
		log.WithFields(log.Fields{"err": err}).Warn("")
		return 0, err
	}

	return rowsAffected, nil
}

func updateProductsInStock(tx *sqlx.Tx, updateValues map[uint64]float64) (uint64, error) {

	var j int64 = 0
	for key, value := range updateValues {

		stmt, err := tx.Prepare("UPDATE products SET " +
			"units_in_stock=$1 " +
			"WHERE product_id=$2")

		if err != nil {
			break
			return ErrorFunc(err)
		}

		res, err := stmt.Exec(value, key)
		if err != nil {
			break
			return ErrorFunc(err)
		}

		affect, err := res.RowsAffected()
		if err != nil {
			break
			return ErrorFunc(err)
		}

		j = j + affect
	}

	log.WithFields(log.Fields{
		"update products in stock": j,
	}).Info("")
	return uint64(j), nil
}

var selectProductRow string =
		"product_id, " +
		"product_image_path, " +
		"product_name, " +
		"supplier_id, " +
		"category_id, " +
		"barcode, " +
		"quantity_per_unit, " +
		"sale_unit_price, " +
		"income_unit_price, " +
		"units_in_stock "

func scanProductRows(rows *sqlx.Rows) ([]*pb.ProductRequest, error) {
	products := make([]*pb.ProductRequest, 0)
	for rows.Next() {
		product := new(pb.ProductRequest)
		err := rows.Scan(
			&product.ProductId,
			&product.ProductImagePath,
			&product.ProductName,
			&product.SupplierId,
			&product.CategoryId,
			&product.Barcode,
			&product.QuantityPerUnit,
			&product.SaleUnitPrice,
			&product.IncomeUnitPrice,
			&product.UnitsInStock)

		if err != nil {
			return nil, err
		}
		products = append(products, product)
	}

	if err := rows.Err(); err != nil {
		return nil, err
	}

	return products, nil
}

func getProductsForProductIds(db *sqlx.DB, productIds []uint64) ([]*pb.ProductRequest, error) {

	query, args, err := sqlx.In("SELECT " +
		selectProductRow +
		"FROM products WHERE product_id IN (?)", productIds)

	if err != nil {
		log.WithFields(log.Fields{"error":err,}).Warn("ERROR")
		return nil, err
	}

	query = sqlx.Rebind(sqlx.DOLLAR, query) //only if postgres
	rows, err := db.Queryx(query, args...)

	products, err := scanProductRows(rows)
	if err != nil {
		log.WithFields(log.Fields{"error":err,}).Warn("ERROR")
		return nil, err
	}

	return products, nil
}

func AllProductsForCompany(db *sqlx.DB, companyId uint64) ([]*pb.ProductRequest, error) {

	rows, err := db.Queryx("SELECT " +
		selectProductRow +
		"FROM products WHERE company_id=$1 ORDER BY product_id DESC", companyId)

	if err != nil {
		print("error")
	}

	products, err := scanProductRows(rows)
	if err != nil {
		log.WithFields(log.Fields{"error":err,}).Warn("ERROR")
		return nil, err
	}

	return products, nil
}

func AllProductsForUpdate(db *sqlx.DB, productFilter *pb.ProductFilter, companyId uint64) ([]*pb.ProductRequest, error) {

	pingError := db.Ping()

	if pingError != nil {
		log.Fatalln(pingError)
		return nil, pingError
	}

	rows, err := db.Queryx("SELECT " +
		selectProductRow +
		"FROM products WHERE company_id=$1 AND updated_at >= $2 ORDER BY product_id DESC LIMIT $3", companyId, productFilter.ProductUpdatedAt, 1000)

	if err != nil {
		print("error")
	}

	products, err := scanProductRows(rows)
	if err != nil {
		log.WithFields(log.Fields{"error":err,}).Warn("ERROR")
		return nil, err
	}

	return products, nil
}

func DeleteProducts(tx *sqlx.Tx, companyId uint64) (uint64, error) {

	stmt, err := tx.Prepare("DELETE FROM products WHERE company_id = $1")
	if err != nil {
		return ErrorFunc(err)
	}

	res, err := stmt.Exec(companyId)
	if err != nil {
		return ErrorFunc(err)
	}

	affect, err := res.RowsAffected()
	if err != nil {
		return ErrorFunc(err)
	}

	return uint64(affect), nil
}