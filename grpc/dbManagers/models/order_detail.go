package models

import (
	"github.com/jmoiron/sqlx"
	log "github.com/Sirupsen/logrus"
	pb "bitbucket.org/kmomuke/rentau/grpc/proto"
	"errors"
)

//type OrderDetailRequest struct {
//	OrderDetailId      uint64
//	OrderId            uint64
//	OrderDetailDate    uint64
//	IsLast             uint32
//	ProductId          uint64
//	BillingNo          string
//	OrderDetailComment string
//	Price              float64
//	OrderQuantity      float64
//	OrderDetailUUID    string
//	ProductQuantity    float64
//}

type OrderDetail struct {
	orderDetailId uint64 `db:"order_detail_id"`
	orderId uint64 `db:"order_id"`
	companyId uint64 `db:"company_id"`
	orderDetailDate uint64 `db:"order_detail_date"`
	billingNo string `db:"billing_no"`
	orderDetailComment string `db:"orderdetail_comment"`
	uuid string `db:"uuid"`
	productId uint64 `db:"product_id"`
	price float32 `db:"price"`
	orderQuantity float32 `db:"order_quantity"`
	productQuantity float32 `db:"product_quantity"`
	discount int32 `db:"discount"`
}

//CREATE TABLE IF NOT EXISTS orderdetails (
//	order_detail_id BIGSERIAL PRIMARY KEY NOT NULL,
//	order_id BIGINT,
//	company_id BIGINT,
//	order_detail_date BIGINT,
//	is_last INTEGER,
//	billing_no varchar (400),
//	orderdetail_comment varchar (400),
//	product_id BIGINT,
//	price REAL,
//	order_quantity REAL,
//	product_quantity REAL,
//	uuid varchar (400),
//	updated_at BIGINT
//);

func StoreOrderDetails(tx *sqlx.Tx, orderDetail *pb.OrderDetailRequest, companyId uint64) (uint64, error)  {

	var lastInsertId uint64
	err := tx.QueryRow("INSERT INTO orderdetails " +
		"(order_id, " +
		"company_id, " +
		"order_detail_date, " +
		"is_last, " +
		"billing_no, " +
		"orderdetail_comment, " +
		"product_id, " +
		"price, " +
		"order_quantity, " +
		"product_quantity, " +
		"uuid, " +
		"updated_at) " +
		"VALUES($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12) returning order_detail_id;",
		orderDetail.OrderId,
		companyId,
		orderDetail.OrderDetailDate,
		orderDetail.IsLast,
		orderDetail.BillingNo,
		orderDetail.OrderDetailComment,
		orderDetail.ProductId,
		orderDetail.Price,
		orderDetail.OrderQuantity,
		orderDetail.ProductQuantity,
		orderDetail.OrderDetailUUID,
		updatedAt()).Scan(&lastInsertId)

	if err != nil {
		return ErrorFunc(err)
	}

	return lastInsertId, nil
}

var selectOrderDetailRow string =
		"order_detail_id, " +
		"order_id, " +
		"order_detail_date, " +
		"is_last, " +
		"billing_no, " +
		"orderdetail_comment, " +
		"product_id, " +
		"price, " +
		"order_quantity, " +
		"product_quantity, " +
		"uuid "

func scanOrderDetailRows(rows *sqlx.Rows) ([]*pb.OrderDetailRequest, error) {
	orderDetails := make([]*pb.OrderDetailRequest, 0)
	for rows.Next() {
		orderDetail := new(pb.OrderDetailRequest)
		err := rows.Scan(
			&orderDetail.OrderDetailId,
			&orderDetail.OrderId,
			&orderDetail.OrderDetailDate,
			&orderDetail.IsLast,
			&orderDetail.BillingNo,
			&orderDetail.OrderDetailComment,
			&orderDetail.ProductId,
			&orderDetail.Price,
			&orderDetail.OrderQuantity,
			&orderDetail.ProductQuantity,
			&orderDetail.OrderDetailUUID)
		if err != nil {
			return nil, err
		}
		orderDetails = append(orderDetails, orderDetail)
	}
	if err := rows.Err(); err != nil {
		return nil, err
	}
	return orderDetails, nil
}

func AllOrderDetailsForFilter(db *sqlx.DB, orderDetFilter *pb.OrderDetailFilter, companyId uint64) ([]*pb.OrderDetailRequest, error) {

	var rows *sqlx.Rows
	var err error
	if orderDetFilter.ProductId > 0 {
		rows, err = db.Queryx("SELECT " +
			selectOrderDetailRow +
			"FROM orderdetails WHERE order_detail_date<=$1 AND product_id=$2 AND company_id=$3 " +
			"ORDER BY order_detail_date DESC LIMIT $4", orderDetFilter.OrderDetailDate, orderDetFilter.ProductId, companyId, orderDetFilter.Limit)
	} else {
		rows, err = db.Queryx("SELECT " +
			selectOrderDetailRow +
			"FROM orderdetails WHERE order_detail_date<=$1 AND order_id=$2 AND company_id=$3 " +
			"ORDER BY order_detail_date DESC LIMIT $4", orderDetFilter.OrderDetailDate, 0, companyId, orderDetFilter.Limit)
	}

	if err != nil {
		print("error")
	}

	orderDetails, err := scanOrderDetailRows(rows)
	if err != nil {
		return nil, err
	}

	return orderDetails, nil
}

func AllUpdatedOrderDetailsForFilter(db *sqlx.DB, orderDetFilter *pb.OrderDetailFilter, companyId uint64) ([]*pb.OrderDetailRequest, error) {

	var rows *sqlx.Rows
	var err error
	if orderDetFilter.ProductId > 0 {
		rows, err = db.Queryx("SELECT " +
			selectOrderDetailRow +
			"FROM orderdetails WHERE updated_at >= $1 AND product_id=$2 AND company_id=$3 " +
			"ORDER BY order_detail_date DESC LIMIT $4", orderDetFilter.OrderDetailDate, orderDetFilter.ProductId, companyId, orderDetFilter.Limit)
	} else {
		rows, err = db.Queryx("SELECT " +
			selectOrderDetailRow +
			"FROM orderdetails WHERE updated_at >= $1 AND order_id=$2 AND company_id=$3 " +
			"ORDER BY order_detail_date DESC LIMIT $4", orderDetFilter.OrderDetailDate, 0, companyId, orderDetFilter.Limit)
	}

	if err != nil {
		print("error")
	}

	orderDetails, err := scanOrderDetailRows(rows)
	if err != nil {
		return nil, err
	}

	return orderDetails, nil
}


func RecentOrderDetailForProduct(db *sqlx.DB, productReq *pb.ProductRequest) (*pb.OrderDetailRequest, error) {

	rows, err := db.Queryx("SELECT " +
		selectOrderDetailRow +
		"FROM orderdetails WHERE product_id=$1 ORDER BY order_detail_date DESC LIMIT $2", productReq.ProductId, 1)

	if err != nil {
		print("error")
	}

	orderDetails, err := scanOrderDetailRows(rows)
	if err != nil {
		return nil, err
	}

	if len(orderDetails) > 0 {
		return orderDetails[0], nil
	}

	log.WithFields(log.Fields{"productReq.ProductId": productReq.ProductId}).Warn("")
	return nil, errors.New("Not found RecentOrderDetailForProduct")
}

func AllOrderDetailsForOrder(db *sqlx.DB, orderId uint64) ([]*pb.OrderDetailRequest, error) {

	rows, err := db.Queryx("SELECT " +
		selectOrderDetailRow +
		"FROM orderdetails WHERE order_id=$1 " +
		"ORDER BY order_detail_date DESC", orderId)

	if err != nil {
		print("error")
	}

	orderDetails, err := scanOrderDetailRows(rows)
	if err != nil {
		return nil, err
	}

	return orderDetails, nil
}

func DeleteOrderDetails(tx *sqlx.Tx, companyId uint64) (uint64, error) {

	stmt, err := tx.Prepare("DELETE FROM orderdetails WHERE company_id = $1")
	if err != nil {
		return ErrorFunc(err)
	}

	res, err := stmt.Exec(companyId)
	if err != nil {
		return ErrorFunc(err)
	}

	affect, err := res.RowsAffected()
	if err != nil {
		return ErrorFunc(err)
	}

	return uint64(affect), nil
}