package models

import (
	pb "bitbucket.org/kmomuke/rentau/grpc/proto"
	"github.com/jmoiron/sqlx"
	log "github.com/Sirupsen/logrus"
	"errors"
)

//type CompanyRequest struct {
//	CompanyId   uint64
//	CompanyName string
//	Email       string
//	Address     string
//}

type Company struct {
	companyId uint64 `db:"company_id"`
	companyName string `db:"company_name"`
	email string `db:"email"`
	address string `db:"address"`
}

//CREATE TABLE IF NOT EXISTS companies (
//	company_id BIGSERIAL PRIMARY KEY NOT NULL,
//	company_name VARCHAR (300),
//	email VARCHAR (300) UNIQUE,
//	address VARCHAR (300),
//	updated_at BIGINT
//);

func StoreCompany(tx *sqlx.Tx, companyReq *pb.CompanyRequest) (uint64, error)  {

	var lastInsertId uint64

	err := tx.QueryRow("INSERT INTO companies " +
		"(company_name, " +
		"email, " +
		"address, " +
		"updated_at) " +
		"VALUES($1, $2, $3, $4) returning company_id;",
		companyReq.CompanyName,
		companyReq.Email,
		companyReq.Address,
		updatedAt()).Scan(&lastInsertId)

	if err != nil {
		return ErrorFunc(err)
	}

	return lastInsertId, nil
}

func UpdateCompany(tx *sqlx.Tx, companyReq *pb.CompanyRequest) (uint64, error)  {

	stmt, err := tx.Prepare("UPDATE companies SET " +
		"company_name=$1, " +
		"email=$2, " +
		"address=$3, " +
		"updated_at=$4 " +
		"WHERE company_id=$5")

	if err != nil {
		return ErrorFunc(err)
	}

	res, err := stmt.Exec(
		companyReq.CompanyName,
		companyReq.Email,
		companyReq.Address,
		updatedAt(),
		companyReq.CompanyId)

	if err != nil {
		return ErrorFunc(err)
	}

	affect, err := res.RowsAffected()
	if err != nil {
		return ErrorFunc(err)
	}

	return uint64(affect), nil
}

func CompanyFor(db *sqlx.DB, companyId uint64) (*pb.CompanyRequest, error) {

	rows, err := db.Queryx("SELECT " +
		"company_id, " +
		"company_name, " +
		"email, " +
		"address " +
		"FROM companies WHERE company_id=$1 LIMIT $2", companyId, 1)

	if err != nil {
		log.WithFields(log.Fields{"err": err}).Warn("")
	}

	companies := make([]*pb.CompanyRequest, 0)
	for rows.Next() {
		company := new(pb.CompanyRequest)
		err := rows.Scan(
			&company.CompanyId,
			&company.CompanyName,
			&company.Email,
			&company.Address)

		if err != nil {
			log.WithFields(log.Fields{"company":err,}).Warn("ERROR")
			return nil, err
		}
		companies = append(companies, company)
	}

	if err := rows.Err(); err != nil {
		return nil, err
	}

	if len(companies) > 0 {
		return companies[0], nil
	}

	log.WithFields(log.Fields{"companyId": companyId}).Warn("")
	return nil, errors.New("Not found TransactionForOrder")
}
