package models

import (
	"github.com/jmoiron/sqlx"
	log "github.com/Sirupsen/logrus"
	pb "bitbucket.org/kmomuke/rentau/grpc/proto"
	"errors"
)

//type TransactionRequest struct {
//	TransactionId     uint64
//	TransactionDate   uint64
//	IsLastTransaction uint32
//	TransactionType   uint32
//	MoneyAmount       float64
//	OrderId           uint64
//	CustomerId        uint64
//	SupplierId        uint64
//	UserId            uint64
//	Comment           string
//	TransactionUUID   string
//	BallanceAmount    float64
//}

type Transaction struct {
	transactionId uint64 `db:"product_id"`
	transactionDate uint64 `db:"transaction_date"`
	company_id uint64 `db:"company_id"`
	transactionType uint64 `db:"transaction_type"`
	moneyAmount float32 `db:"money_amount"`
	isLastTransaction uint `db:"is_last_transaction"`
	orderId uint64 `db:"order_id"`
	customerId uint64 `db:"customer_id"`
	supplierId uint64 `db:"supplier_id"`
	userId uint64 `db:"user_id"`
	comment string `db:"comment"`
	transactionUUID string `db:"uuid"`
	ballanceAmount float32 `db:"money_amount"`
}

//CREATE TABLE IF NOT EXISTS transactions (
//	transaction_id BIGSERIAL PRIMARY KEY NOT NULL,
//	transaction_date BIGINT,
//	company_id BIGINT,
//	is_last_transaction INTEGER,
//	transaction_type INTEGER,
//	money_amount REAL,
//	order_id BIGINT,
//	customer_id BIGINT,
//	supplier_id BIGINT,
//	user_id BIGINT,
//	comment varchar (500),
//	uuid varchar (400),
//      ballance_amount REAL,
//	updated_at BIGINT
//);

func StoreTransaction(tx *sqlx.Tx, transaction *pb.TransactionRequest, companyId uint64) (uint64, error)  {

	var lastInsertId uint64
	err := tx.QueryRow("INSERT INTO transactions (" +
		"transaction_date, " +
		"company_id, " +
		"is_last_transaction, " +
		"transaction_type, " +
		"money_amount, " +
		"order_id, " +
		"customer_id, " +
		"supplier_id, " +
		"user_id, " +
		"comment, " +
		"uuid, " +
		"ballance_amount, " +
		"updated_at) " +
		"VALUES($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13) returning transaction_id;",
		transaction.TransactionDate,
		companyId,
		transaction.IsLastTransaction,
		transaction.TransactionType,
		transaction.MoneyAmount,
		transaction.OrderId,
		transaction.CustomerId,
		transaction.SupplierId,
		transaction.UserId,
		transaction.Comment,
		transaction.TransactionUUID,
		transaction.BallanceAmount,
		updatedAt()).Scan(&lastInsertId)

	if err != nil {
		return ErrorFunc(err)
	}

	return lastInsertId, nil
}

func UpdateTransaction(tx *sqlx.Tx, transaction *pb.TransactionRequest, companyId uint64) (uint64, error)  {

	stmt, err := tx.Prepare("UPDATE transactions SET " +
		"transaction_date=$1, " +
		"company_id=$2, " +
		"is_last_transaction=$3, " +
		"transaction_type=$4, " +
		"money_amount=$5, " +
		"order_id=$6, " +
		"customer_id=$7, " +
		"supplier_id=$8, " +
		"user_id=$9, " +
		"comment=$10, " +
		"uuid=$11, " +
		"ballance_amount=$12, " +
		"updated_at=$13 " +
		"WHERE transaction_id=$14")

	if err != nil {
		return ErrorFunc(err)
	}

	res, err := stmt.Exec(
		transaction.TransactionDate,
		companyId,
		transaction.IsLastTransaction,
		transaction.TransactionType,
		transaction.MoneyAmount,
		transaction.OrderId,
		transaction.CustomerId,
		transaction.SupplierId,
		transaction.UserId,
		transaction.Comment,
		transaction.TransactionUUID,
		transaction.BallanceAmount,
		updatedAt(),
		transaction.TransactionId)

	if err != nil {
		return ErrorFunc(err)
	}

	affect, err := res.RowsAffected()
	if err != nil {
		return ErrorFunc(err)
	}

	return uint64(affect), nil
}

var selectTransactionRow string =
				"transaction_id, " +
				"transaction_date, " +
				"is_last_transaction, " +
				"transaction_type, " +
				"money_amount, " +
				"order_id, " +
				"customer_id, " +
				"supplier_id, " +
				"user_id, " +
				"comment, " +
				"uuid, " +
				"ballance_amount "

func scanTransactionRow(rows *sqlx.Rows) ([]*pb.TransactionRequest, error) {
	transactions := make([]*pb.TransactionRequest, 0)
	for rows.Next() {
		transaction := new(pb.TransactionRequest)
		err := rows.Scan(
			&transaction.TransactionId,
			&transaction.TransactionDate,
			&transaction.IsLastTransaction,
			&transaction.TransactionType,
			&transaction.MoneyAmount,
			&transaction.OrderId,
			&transaction.CustomerId,
			&transaction.SupplierId,
			&transaction.UserId,
			&transaction.Comment,
			&transaction.TransactionUUID,
			&transaction.BallanceAmount)

		if err != nil {
			log.WithFields(log.Fields{"scanTransactionRow":err,}).Warn("ERROR")
			return nil, err
		}
		transactions = append(transactions, transaction)
	}

	if err := rows.Err(); err != nil {
		return nil, err
	}

	return transactions, nil
}

func RecentTransactionForCustomer(db *sqlx.DB, customerId uint64) (*pb.TransactionRequest, error) {

	rows, err := db.Queryx("SELECT " +
		selectTransactionRow +
		"FROM transactions WHERE customer_id=$1 ORDER BY transaction_date DESC LIMIT $2", customerId, 1)

	if err != nil {
		log.WithFields(log.Fields{"err": err}).Warn("")
	}

	transactions, err := scanTransactionRow(rows)
	if err != nil {
		log.WithFields(log.Fields{"err": err}).Warn("")
		return nil, err
	}

	if len(transactions) > 0 {
		return transactions[0], nil
	}

	log.WithFields(log.Fields{"Not found RecentTransactionForCustomer": customerId}).Warn("")
	return nil, errors.New("Not found RecentTransactionForCustomer")
}

func RecentTransactionForSupplier(db *sqlx.DB, supplierId uint64) (*pb.TransactionRequest, error) {

	rows, err := db.Queryx("SELECT " +
		selectTransactionRow +
		"FROM transactions WHERE supplier_id=$1 ORDER BY transaction_date DESC LIMIT $2", supplierId, 1)

	if err != nil {
		log.WithFields(log.Fields{"err": err}).Warn("")
	}

	transactions, err := scanTransactionRow(rows)
	if err != nil {
		log.WithFields(log.Fields{"err": err}).Warn("")
		return nil, err
	}

	if len(transactions) > 0 {
		return transactions[0], nil
	}

	log.WithFields(log.Fields{"Not found RecentTransactionForSupplier": supplierId}).Warn("")
	return nil, errors.New("Not found RecentTransactionForSupplier")
}

func AllUpdatedTransactions(db *sqlx.DB, transactFilter *pb.TransactionFilter, companyId uint64) ([]*pb.TransactionRequest, error) {

	var rows *sqlx.Rows
	var err error
	rows, err = db.Queryx("SELECT " +
		selectTransactionRow +
		"FROM transactions " +
		"WHERE updated_at >=$1 AND order_id=0 AND company_id=$2 ORDER BY transaction_date DESC LIMIT $3",
		transactFilter.TransactionDate, companyId, transactFilter.Limit)

	if err != nil {
		log.WithFields(log.Fields{"err": err}).Warn("")
	}

	transactions, err := scanTransactionRow(rows)
	if err != nil {
		log.WithFields(log.Fields{"err": err}).Warn("")
		return nil, err
	}

	return transactions, nil
}

func AllTransactionsForFilter(db *sqlx.DB, transactFilter *pb.TransactionFilter, companyId uint64) ([]*pb.TransactionRequest, error) {

	var rows *sqlx.Rows
	var err error

	if transactFilter.CustomerId > 0 {
		rows, err = db.Queryx("SELECT " +
			selectTransactionRow +
			"FROM transactions " +
			"WHERE transaction_date<=$1 AND customer_id=$2 AND company_id=$3 ORDER BY transaction_date DESC LIMIT $4",
			transactFilter.TransactionDate, transactFilter.CustomerId, companyId, transactFilter.Limit)
	} else if transactFilter.SupplierId > 0 {
		rows, err = db.Queryx("SELECT " +
			selectTransactionRow +
			"FROM transactions " +
			"WHERE transaction_date<=$1 AND supplier_id=$2 AND company_id=$3 ORDER BY transaction_date DESC LIMIT $4",
			transactFilter.TransactionDate, transactFilter.SupplierId, companyId, transactFilter.Limit)
	} else {
		rows, err = db.Queryx("SELECT " +
			selectTransactionRow +
			"FROM transactions " +
			"WHERE transaction_date<=$1 AND company_id=$2 ORDER BY transaction_date DESC LIMIT $3",
			transactFilter.TransactionDate, companyId, transactFilter.Limit)
	}

	if err != nil {
		log.WithFields(log.Fields{"err": err}).Warn("")
	}

	transactions, err := scanTransactionRow(rows)
	if err != nil {
		log.WithFields(log.Fields{"err": err}).Warn("")
		return nil, err
	}

	return transactions, nil
}

func TransactionForOrder(db *sqlx.DB, orderId uint64) (*pb.TransactionRequest, error) {

	rows, err := db.Queryx("SELECT " +
		selectTransactionRow +
		"FROM transactions WHERE order_id=$1 ORDER BY transaction_date DESC LIMIT $2", orderId, 1)

	if err != nil {
		log.WithFields(log.Fields{"err": err}).Warn("")
	}

	transactions, err := scanTransactionRow(rows)
	if err != nil {
		log.WithFields(log.Fields{"err": err}).Warn("")
		return nil, err
	}

	if len(transactions) > 0 {
		return transactions[0], nil
	}

	log.WithFields(log.Fields{"Not found TransactionForOrder": orderId}).Warn("")
	return nil, errors.New("Not found TransactionForOrder")
}

func DeleteTransactions(tx *sqlx.Tx, companyId uint64) (uint64, error) {

	stmt, err := tx.Prepare("DELETE FROM transactions WHERE company_id = $1")
	if err != nil {
		return ErrorFunc(err)
	}

	res, err := stmt.Exec(companyId)
	if err != nil {
		return ErrorFunc(err)
	}

	affect, err := res.RowsAffected()
	if err != nil {
		return ErrorFunc(err)
	}

	return uint64(affect), nil
}