package models

import (
	pb "bitbucket.org/kmomuke/rentau/grpc/proto"
	"github.com/jmoiron/sqlx"
	"time"
	log "github.com/Sirupsen/logrus"
	"errors"
)

//type UserRequest struct {
//	UserId        uint64
//	UserUUID      string
//	CompanyId     uint64
//	RoleId        uint64
//	UserImagePath string
//	FirstName     string
//	SecondName    string
//	Email         string
//	Password      string
//	PhoneNumber   string
//	Address       string
//}

type User struct {
	userId uint64 `db:"user_id"`
	userUUID string `db:"user_uuid"`
	companyId uint64 `db:"company_id"`
	roleId uint64 `db:"role_id"`
	userImagePath string `db:"user_image_path"`
	firstName string `db:"first_name"`
	secondName string `db:"second_name"`
	email string `db:"email"`
	password string `db:"password"`
	phoneNumber string `db:"phone_number"`
	address string `db:"address"`
}

//CREATE TABLE IF NOT EXISTS users (
//	user_id BIGSERIAL PRIMARY KEY NOT NULL,
//	user_uuid VARCHAR (300) UNIQUE,
//	comany_id BIGINT,
//	role_id INTEGER,
//	user_image_path VARCHAR (300),
//	first_name VARCHAR (300),
//	second_name VARCHAR (300),
//	email VARCHAR (300) UNIQUE,
//	password VARCHAR (500),
//	phone_number VARCHAR (300),
//	address VARCHAR (300),
//	updated_at BIGINT
//);

func StoreUser(tx *sqlx.Tx, user *pb.UserRequest, companyId uint64) (uint64, error)  {

	updatedAt := (time.Now().UnixNano() / 1000000)
	var lastInsertId uint64
	err := tx.QueryRow("INSERT INTO users " +
		"(company_id, " +
		"user_uuid, " +
		"role_id, " +
		"user_image_path, " +
		"first_name, " +
		"second_name, " +
		"email, " +
		"password, " +
		"phone_number, " +
		"address, " +
		"updated_at) " +
		"VALUES($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11) returning user_id;",
		companyId,
		user.UserUUID,
		user.RoleId,
		user.UserImagePath,
		user.FirstName,
		user.SecondName,
		user.Email,
		user.Password,
		user.PhoneNumber,
		user.Address,
		updatedAt).Scan(&lastInsertId)

	if err != nil {
		return ErrorFunc(err)
	}

	return lastInsertId, nil
}

var selectUserRow string =	"user_id, " +
				"company_id, " +
				"user_uuid, " +
				"role_id, " +
				"user_image_path, " +
				"first_name, " +
				"second_name, " +
				"email, " +
				"password, " +
				"phone_number, " +
				"address "

func scanUserRow(rows *sqlx.Rows) ([]*pb.UserRequest, error) {
	users := make([]*pb.UserRequest, 0)
	for rows.Next() {
		user := new(pb.UserRequest)
		err := rows.Scan(
			&user.UserId,
			&user.CompanyId,
			&user.UserUUID,
			&user.RoleId,
			&user.UserImagePath,
			&user.FirstName,
			&user.SecondName,
			&user.Email,
			&user.Password,
			&user.PhoneNumber,
			&user.Address)

		if err != nil {
			log.WithFields(log.Fields{"scanUserRow":err,}).Warn("ERROR")
			break
		}

		users = append(users, user)
	}

	return users, nil
}

func UpdateUser(tx *sqlx.Tx, user *pb.UserRequest, companyId uint64) (uint64, error)  {

	updatedAt := (time.Now().UnixNano() / 1000000)

	stmt, err := tx.Prepare("UPDATE users SET " +
		"role_id=$1, " +
		"company_id=$2, " +
		"user_image_path=$3, " +
		"first_name=$4, " +
		"second_name=$5, " +
		"email=$6, " +
		"password=$7, " +
		"phone_number=$8, " +
		"address=$9, " +
		"updated_at=$10 " +
		"WHERE user_id=$11")

	if err != nil {
		return ErrorFunc(err)
	}

	res, err := stmt.Exec(user.RoleId,
		companyId,
		user.UserImagePath,
		user.FirstName,
		user.SecondName,
		user.Email,
		user.Password,
		user.PhoneNumber,
		user.Address,
		updatedAt,
		user.UserId)

	if err != nil {
		return ErrorFunc(err)
	}

	affect, err := res.RowsAffected()
	if err != nil {
		return ErrorFunc(err)
	}

	log.WithFields(log.Fields{"update user rows changed": affect,}).Info("")
	return uint64(affect), nil
}


func AllUsersForCompany(db *sqlx.DB, company_id uint64) ([]*pb.UserRequest, error) {

	rows, err := db.Queryx("SELECT " +
		selectUserRow +
		"FROM users WHERE company_id = $1", company_id)

	if err != nil {
		log.WithFields(log.Fields{"Queryx":err,}).Warn("ERROR")
		return nil, err
	}

	users, err := scanUserRow(rows)

	if err != nil {
		return nil, err
	}

	if err = rows.Err(); err != nil {
		return nil, err
	}

	return users, nil
}

func AllUsersForUpdate(db *sqlx.DB, filter *pb.UserFilter, company_id uint64) ([]*pb.UserRequest, error) {

	rows, err := db.Queryx("SELECT " +
		selectUserRow +
		"FROM users WHERE updated_at >= $1 AND company_id = $2 LIMIT $3",filter.UserUpdatedAt, company_id, 3000)

	if err != nil {
		log.WithFields(log.Fields{"Queryx":err,}).Warn("ERROR")
		return nil, err
	}

	users, err := scanUserRow(rows)

	if err != nil {
		return nil, err
	}

	if err = rows.Err(); err != nil {
		return nil, err
	}

	return users, nil
}

func UserForEmail(db *sqlx.DB, email string) (*pb.UserRequest, error)  {

	rows, err := db.Queryx("SELECT " +
		selectUserRow +
		"FROM users WHERE email=$1", email)

	if err != nil {
		print("error")
	}

	users, err := scanUserRow(rows)

	if len(users) > 0 {
		return users[0], nil
	}

	return nil, nil
}


func UserForUUID(db *sqlx.DB, uuid string) (*pb.UserRequest, error)  {

	rows, err := db.Queryx("SELECT " +
		selectUserRow +
		"FROM users WHERE user_uuid = $1", uuid)

	if err != nil {
		print("error")
	}

	users, err := scanUserRow(rows)

	if len(users) > 0 {
		return users[0], nil
	}

	return nil, errors.New("No such user")
}
