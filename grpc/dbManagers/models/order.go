package models

import (
	"github.com/jmoiron/sqlx"
	log "github.com/Sirupsen/logrus"
	pb "bitbucket.org/kmomuke/rentau/grpc/proto"
)

//type OrderRequest struct {
//	OrderId           uint64
//	OrderDocument     uint32
//	OrderMovement     uint32
//	MoneyMovementType uint32
//	BillingNo         string
//	UserId            uint64
//	CustomerId        uint64
//	SupplierId        uint64
//	OrderDate         uint64
//	PaymentId         uint64
//	ErrorMsg          string
//	Comment           string
//	OrderUUID         string
//	IsDeleted         uint32
//	IsMoneyForDebt    uint32
//	IsEdited          uint32
//}

type Order struct {
	orderId uint64 `db:"order_id"`
	companyId uint64 `db:"company_id"`
	orderDocument uint32 `db:"order_document"`
	orderMovement uint32 `db:"order_movement"`
	moneyMovementType uint32 `db:"money_movement"`
	billingNo string `db:"billing_no"`

	userId uint64 `db:"user_id"`
	customerId uint64 `db:"customer_id"`
	supplierId uint64 `db:"supplier_id"`
	paymentId uint64 `db:"payment_id"`

	orderDate uint64 `db:"order_date"`
	errorMsg string `db:"error_msg"`
	comment string `db:"comment"`
	orderUUID string `db:"uuid"`

	isDeleted uint32 `db:"is_deleted"`
	isMoneyForDebt uint32 `db:"is_money_for_debt"`
	isEdited uint32 `db:"is_editted"`
}

//CREATE TABLE IF NOT EXISTS orders (
//	order_id BIGSERIAL PRIMARY KEY NOT NULL,
//	company_id BIGINT,
//	order_document INTEGER,
//	order_movement INTEGER,
//	money_movement INTEGER,
//	billing_no varchar (400),
//
//	user_id BIGINT,
//	customer_id BIGINT,
//	supplier_id BIGINT,
//	order_date BIGINT,
//	payment_id BIGINT,
//
//	error_msg varchar (400),
//      uuid varchar (400),
//	comment varchar (400),
//	is_deleted INTEGER,
//	is_money_for_debt INTEGER,
//	is_editted INTEGER,
//	updated_at BIGINT
//);

func StoreOrder(tx *sqlx.Tx, order *pb.OrderRequest, companyId uint64) (uint64, error)  {

	var lastInsertId uint64
	err := tx.QueryRow("INSERT INTO orders " +
		"(order_document, " +
		"order_movement, " +
		"money_movement, " +
		"company_id, " +
		"billing_no, " +
		"user_id, " +
		"customer_id, " +
		"supplier_id, " +
		"order_date, " +
		"payment_id, " +
		"error_msg, " +
		"uuid, " +
		"comment, " +
		"is_deleted, " +
		"is_money_for_debt, " +
		"is_editted, " +
		"updated_at) " +
		"VALUES($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14, $15, $16, $17) returning order_id;",
		order.OrderDocument,
		order.OrderMovement,
		order.MoneyMovementType,
		companyId,
		order.BillingNo,
		order.UserId,
		order.CustomerId,
		order.SupplierId,
		order.OrderDate,
		order.PaymentId,
		order.ErrorMsg,
		order.OrderUUID,
		order.Comment,
		order.IsDeleted,
		order.IsMoneyForDebt,
		order.IsEdited,
		updatedAt()).Scan(&lastInsertId)

	if err != nil {
		return ErrorFunc(err)
	}

	return lastInsertId, nil
}

func UpdateOrder(tx *sqlx.Tx, order *pb.OrderRequest, companyId uint64) (uint64, error)  {

	stmt, err :=tx.Prepare("UPDATE orders SET " +
		"order_document=$1, " +
		"order_movement=$2, " +
		"money_movement=$3, " +
		"company_id=$4, " +
		"billing_no=$5, " +
		"user_id=$6, " +
		"customer_id=$7, " +
		"supplier_id=$8, " +
		"order_date=$9, " +
		"payment_id=$10, " +
		"error_msg=$11, " +
		"uuid=$12, " +
		"comment=$13, " +
		"is_deleted=$14, " +
		"is_money_for_debt=$15, " +
		"is_editted=$16, " +
		"updated_at=$17 WHERE order_id=$18")

	if err != nil {
		return ErrorFunc(err)
	}

	res, err := stmt.Exec(
		order.OrderDocument,
		order.OrderMovement,
		order.MoneyMovementType,
		companyId,
		order.BillingNo,
		order.UserId,
		order.CustomerId,
		order.SupplierId,
		order.OrderDate,
		order.PaymentId,
		order.ErrorMsg,
		order.OrderUUID,
		order.Comment,
		order.IsDeleted,
		order.IsMoneyForDebt,
		order.IsEdited,
		updatedAt(),
		order.OrderId)

	if err != nil {
		return ErrorFunc(err)
	}

	affect, err := res.RowsAffected()
	if err != nil {
		return ErrorFunc(err)
	}

	return uint64(affect), nil
}

var selectOrderRow string =
				"order_id, " +
				"order_document, " +
				"order_movement, " +
				"money_movement, " +
				"billing_no, " +
				"user_id, " +
				"customer_id, " +
				"supplier_id, " +
				"order_date, " +
				"payment_id, " +
				"error_msg, " +
				"uuid, " +
				"comment, " +
				"is_deleted, " +
				"is_money_for_debt, " +
				"is_editted "

func scanOrderRows(rows *sqlx.Rows) ([]*pb.OrderRequest, error) {
	orders := make([]*pb.OrderRequest, 0)
	for rows.Next() {
		order := new(pb.OrderRequest)
		err := rows.Scan(
			&order.OrderId,
			&order.OrderDocument,
			&order.OrderMovement,
			&order.MoneyMovementType,
			&order.BillingNo,
			&order.UserId,
			&order.CustomerId,
			&order.SupplierId,
			&order.OrderDate,
			&order.PaymentId,
			&order.ErrorMsg,
			&order.OrderUUID,
			&order.Comment,
			&order.IsDeleted,
			&order.IsMoneyForDebt,
			&order.IsEdited)

		if err != nil {
			log.WithFields(log.Fields{"scanOrderRows":err,}).Warn("ERROR")
			return nil, err
		}

		orders = append(orders, order)
	}
	return orders, nil
}

func AllOrdersForFilter(db *sqlx.DB, orderFilter *pb.OrderFilter, companyId uint64) ([]*pb.OrderRequest, error) {

	rows, err := db.Queryx("SELECT " +
		selectOrderRow +
		"FROM orders WHERE order_date<=$1 AND company_id=$2 ORDER BY order_date DESC LIMIT $3", orderFilter.OrderDate, companyId, orderFilter.Limit)

	if err != nil {
		log.WithFields(log.Fields{"error":err,}).Warn("ERROR")
	}

	orders, err := scanOrderRows(rows)

	if err = rows.Err(); err != nil {
		log.WithFields(log.Fields{"error":err,}).Warn("ERROR")
		return nil, err
	}

	return orders, nil
}

func AllUpdatedOrders(db *sqlx.DB, orderFilter *pb.OrderFilter, companyId uint64) ([]*pb.OrderRequest, error) {

	rows, err := db.Queryx("SELECT " +
		selectOrderRow +
		"FROM orders WHERE updated_at >= $1 AND company_id=$2 ORDER BY order_date DESC LIMIT $3", orderFilter.OrderDate, companyId, orderFilter.Limit)

	if err != nil {
		log.WithFields(log.Fields{"error":err,}).Warn("ERROR")
	}

	orders, err := scanOrderRows(rows)

	if err = rows.Err(); err != nil {
		return nil, err
	}

	return orders, nil
}

func DeleteOrders(tx *sqlx.Tx, companyId uint64) (uint64, error) {

	stmt, err := tx.Prepare("DELETE FROM orders WHERE company_id = $1")
	if err != nil {
		return ErrorFunc(err)
	}

	res, err := stmt.Exec(companyId)
	if err != nil {
		return ErrorFunc(err)
	}

	affect, err := res.RowsAffected()
	if err != nil {
		return ErrorFunc(err)
	}

	return uint64(affect), nil
}