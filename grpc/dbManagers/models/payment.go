package models

import (
	"github.com/jmoiron/sqlx"
	pb "bitbucket.org/kmomuke/rentau/grpc/proto"
	"errors"
)

//type PaymentRequest struct {
//	PaymentId              uint64  `protobuf:"varint,1,opt,name=paymentId" json:"paymentId,omitempty"`
//	TotalOrderPrice        float64 `protobuf:"fixed64,2,opt,name=totalOrderPrice" json:"totalOrderPrice,omitempty"`
//	Discount               float64 `protobuf:"fixed64,3,opt,name=discount" json:"discount,omitempty"`
//	TotalPriceWithDiscount float64 `protobuf:"fixed64,4,opt,name=totalPriceWithDiscount" json:"totalPriceWithDiscount,omitempty"`
//	MinusPrice             float64 `protobuf:"fixed64,5,opt,name=minusPrice" json:"minusPrice,omitempty"`
//	PlusPrice              float64 `protobuf:"fixed64,6,opt,name=plusPrice" json:"plusPrice,omitempty"`
//	Comment                string  `protobuf:"bytes,7,opt,name=comment" json:"comment,omitempty"`
//}

type Payment struct {
	paymentId uint64 `db:"payment_id"`
	totalOrderPrice float32 `db:"total_order_price"`
	discount float32 `db:"discount"`
	totalPriceWithDiscount float32 `db:"total_price_with_discount"`
}

//CREATE TABLE public.payments (
//	payment_id BIGINT PRIMARY KEY NOT NULL DEFAULT nextval('payments_payment_id_seq'::regclass),
//	total_order_price REAL,
//	discount REAL,
//	total_price_with_discount REAL,
//	plus_price REAL DEFAULT 0,
//	minus_price REAL DEFAULT 0,
//	comment CHARACTER VARYING(400) DEFAULT ''
//);

func StorePayment(tx *sqlx.Tx, payment *pb.PaymentRequest) (uint64, error)  {

	var lastInsertId uint64
	err := tx.QueryRow("INSERT INTO payments " +
		"(total_order_price, " +
		"discount, " +
		"total_price_with_discount, " +
		"plus_price, " +
		"minus_price, " +
		"comment) " +
		"VALUES($1, $2, $3, $4, $5, $6) returning payment_id;",
		payment.TotalOrderPrice,
		payment.Discount,
		payment.TotalPriceWithDiscount,
		payment.PlusPrice,
		payment.MinusPrice,
		payment.Comment).Scan(&lastInsertId)

	if err != nil {
		return ErrorFunc(err)
	}

	return lastInsertId, nil
}

func PaymentForOrder(db *sqlx.DB, paymentId uint64) (*pb.PaymentRequest, error) {

	rows, err := db.Queryx("SELECT " +
		"payment_id, " +
		"total_order_price, " +
		"discount, " +
		"total_price_with_discount, " +
		"plus_price, " +
		"minus_price, " +
		"comment " +
		"FROM payments WHERE payment_id=$1 LIMIT $2", paymentId, 1)

	if err != nil {
		print("error")
	}

	payments := make([]*pb.PaymentRequest, 0)
	for rows.Next() {
		payment := new(pb.PaymentRequest)
		err := rows.Scan(
			&payment.PaymentId,
			&payment.TotalOrderPrice,
			&payment.Discount,
			&payment.TotalPriceWithDiscount,
			&payment.PlusPrice,
			&payment.MinusPrice,
			&payment.Comment)

		if err != nil {
			return nil, err
		}
		payments = append(payments, payment)
	}

	if err = rows.Err(); err != nil {
		return nil, err
	}

	if len(payments) > 0 {
		return payments[0], nil
	}

	return nil, errors.New("Not found PaymentForOrder")
}
