package models

import (
	pb "bitbucket.org/kmomuke/rentau/grpc/proto"
	"github.com/jmoiron/sqlx"
	log "github.com/Sirupsen/logrus"
)

//type CategoryRequest struct {
//	CategoryId   uint64
//	CategoryName string
//}

type Category struct {
	categoryId uint64 `db:"category_id"`
	companyId uint64 `db:"company_id"`
	categoryName string `db:"category_name"`
}

//CREATE TABLE IF NOT EXISTS categories (
//	category_id BIGSERIAL PRIMARY KEY NOT NULL,
//	company_id BIGINT,
//	category_name varchar (400),
//	updated_at BIGINT
//);

func StoreCategory(tx *sqlx.Tx, catRequest *pb.CategoryRequest, companyId uint64) (uint64, error)  {

	var lastInsertId uint64

	err := tx.QueryRow("INSERT INTO categories " +
		"(category_name, " +
		"company_id, " +
		"updated_at) " +
		"VALUES($1, $2, $3) returning category_id;",
		catRequest.CategoryName,
		companyId,
		updatedAt()).Scan(&lastInsertId)

	if err != nil {
		return ErrorFunc(err)
	}

	return lastInsertId, nil
}

func UpdateCategory(tx *sqlx.Tx, catRequest *pb.CategoryRequest, companyId uint64) (uint64, error)  {

	stmt, err := tx.Prepare("UPDATE categories SET " +
		"category_name=$1, " +
		"company_id=$2, " +
		"updated_at=$3 " +
		"WHERE category_id=$4")

	if err != nil {
		return ErrorFunc(err)
	}

	res, err := stmt.Exec(
		catRequest.CategoryName,
		companyId,
		updatedAt(),
		catRequest.CategoryId)

	if err != nil {
		return ErrorFunc(err)
	}

	affect, err := res.RowsAffected()
	if err != nil {
		return ErrorFunc(err)
	}

	return uint64(affect), nil
}

func AllCategoriesForCompany(db *sqlx.DB, companyId uint64) ([]*pb.CategoryRequest, error) {

	rows, err := db.Queryx("SELECT " +
		selectCategoryRow +
		"FROM categories WHERE company_id=$1 ORDER BY category_name ASC", companyId)

	if err != nil {
		log.WithFields(log.Fields{"Queryx":err,}).Warn("ERROR")
		return nil, err
	}

	categories, err := scanCategoryRow(rows)
	if err != nil {
		log.WithFields(log.Fields{"scanCategoryRow":err,}).Warn("ERROR")
		return nil, err
	}

	return categories, nil
}

var selectCategoryRow string =
		"category_id, " +
		"category_name "

func scanCategoryRow(rows *sqlx.Rows) ([]*pb.CategoryRequest, error) {
	categories := make([]*pb.CategoryRequest, 0)

	for rows.Next() {
		category := new(pb.CategoryRequest)
		err := rows.Scan(
			&category.CategoryId,
			&category.CategoryName)
		if err != nil {
			break
			return nil, err
		}
		categories = append(categories, category)
	}

	if err := rows.Err(); err != nil {
		return nil, err
	}

	return categories, nil
}

func AllUpdatedCategories(db *sqlx.DB, categoryFilter *pb.CategoryFilter, companyId uint64) ([]*pb.CategoryRequest, error) {

	rows, err := db.Queryx("SELECT " +
		selectCategoryRow +
		"FROM categories WHERE updated_at >= $1 AND company_id=$2 LIMIT $3", categoryFilter.CategoryUpdatedAt, companyId, 1000)

	if err != nil {
		print("error")
	}

	categories, err := scanCategoryRow(rows)
	if err != nil {
		log.WithFields(log.Fields{"scanCategoryRow":err,}).Warn("ERROR")
		return nil, err
	}

	return categories, nil
}

func DeleteCategories(tx *sqlx.Tx, companyId uint64) (uint64, error) {

	stmt, err := tx.Prepare("DELETE FROM categories WHERE company_id = $1")
	if err != nil {
		return ErrorFunc(err)
	}

	res, err := stmt.Exec(companyId)
	if err != nil {
		return ErrorFunc(err)
	}

	affect, err := res.RowsAffected()
	if err != nil {
		return ErrorFunc(err)
	}

	return uint64(affect), nil
}
