package models

import (
	"github.com/jmoiron/sqlx"
	pb "bitbucket.org/kmomuke/rentau/grpc/proto"
	log "github.com/Sirupsen/logrus"
)

//type CustomerRequest struct {
//	CustomerId        uint64
//	CustomerImagePath string
//	FirstName         string
//	SecondName        string
//	PhoneNumber       string
//	Address           string
//	UserId            uint64
//}

type Customer struct {
	customerId uint64 `db:"customer_id"`
	user_id uint64 `db:"user_id"`
	company_id uint64 `db:"company_id"`
	customerImagePath string `db:"customer_image_path"`
	firstName string `db:"first_name"`
	secondName string `db:"second_name"`
	phoneNumber string `db:"phone_number"`
	address float32 `db:"address"`
}

//CREATE TABLE IF NOT EXISTS customers (
//	customer_id BIGSERIAL PRIMARY KEY NOT NULL,
//	user_id BIGINT,
//	company_id BIGINT,
//	customer_image_path varchar (400),
//	first_name varchar (400),
//	second_name varchar (400),
//	phone_number varchar (400),
//	address varchar (400),
//	updated_at BIGINT
//);

func StoreCustomer(tx *sqlx.Tx, customerRequest *pb.CustomerRequest, companyId uint64) (uint64, error)  {

	var lastInsertId uint64
	err := tx.QueryRow("INSERT INTO customers(" +
		"user_id, " +
		"company_id, " +
		"customer_image_path, " +
		"first_name, " +
		"second_name, " +
		"phone_number, " +
		"address, " +
		"updated_at) VALUES($1, $2, $3, $4, $5, $6, $7, $8) returning customer_id;",
		customerRequest.UserId,
		companyId,
		customerRequest.CustomerImagePath,
		customerRequest.FirstName,
		customerRequest.SecondName,
		customerRequest.PhoneNumber,
		customerRequest.Address,
		updatedAt()).Scan(&lastInsertId)

	if err != nil {
		return ErrorFunc(err)
	}

	return lastInsertId, nil
}

func UpdateCustomer(tx *sqlx.Tx, customerReq *pb.CustomerRequest, companyId uint64) (uint64, error)  {

	stmt, err :=tx.Prepare("UPDATE customers SET " +
		"customer_image_path=$1, " +
		"first_name=$2, " +
		"second_name=$3, " +
		"phone_number=$4, " +
		"address=$5, " +
		"user_id=$6, " +
		"company_id=$7, " +
		"updated_at=$8 " +
		"WHERE customer_id=$9")

	if err != nil {
		return ErrorFunc(err)
	}

	res, err := stmt.Exec(
		customerReq.CustomerImagePath,
		customerReq.FirstName,
		customerReq.SecondName,
		customerReq.PhoneNumber,
		customerReq.Address,
		customerReq.UserId,
		companyId,
		updatedAt(),
		customerReq.CustomerId)

	if err != nil {
		return ErrorFunc(err)
	}

	affect, err := res.RowsAffected()
	if err != nil {
		return ErrorFunc(err)
	}

	return uint64(affect), nil
}

func AllCustomersForCompany(db *sqlx.DB, companyId uint64) ([]*pb.CustomerRequest, error) {

	rows, err := db.Queryx("SELECT " +
		selectCustomerRow +
		"FROM customers WHERE company_id=$1 ORDER BY first_name ASC", companyId)

	if err != nil {
		log.WithFields(log.Fields{"error": err, }).Warn("")
		return nil, err
	}

	customers, err := scanCustomerRow(rows)
	if err != nil {
		return nil, err
	}

	return customers, nil
}

var selectCustomerRow string =
		"customer_id, " +
		"customer_image_path, " +
		"first_name, " +
		"second_name, " +
		"phone_number, " +
		"address, " +
		"user_id "

func scanCustomerRow(rows *sqlx.Rows) ([]*pb.CustomerRequest, error) {
	customers := make([]*pb.CustomerRequest, 0)
	for rows.Next() {
		customer := new(pb.CustomerRequest)
		err := rows.Scan(
			&customer.CustomerId,
			&customer.CustomerImagePath,
			&customer.FirstName,
			&customer.SecondName,
			&customer.PhoneNumber,
			&customer.Address,
			&customer.UserId)

		if err != nil {
			log.WithFields(log.Fields{"error": err, }).Warn("")
			return nil, err
		}
		customers = append(customers, customer)
	}
	return customers, nil
}

func AllUpdatedCustomers(db *sqlx.DB, custFilter *pb.CustomerFilter, companyId uint64) ([]*pb.CustomerRequest, error)  {

	rows, err := db.Queryx("SELECT " +
		selectCustomerRow +
		"FROM customers WHERE updated_at >= $1 AND company_id = $2 LIMIT $3", custFilter.CustomerUpdatedAt, companyId, 1000)

	if err != nil {
		log.WithFields(log.Fields{"error": err, }).Warn("")
		return nil, err
	}

	customers, err := scanCustomerRow(rows)
	if err != nil {
		return nil, err
	}

	return customers, nil
}

func DeleteCustomers(tx *sqlx.Tx, companyId uint64) (uint64, error) {

	stmt, err := tx.Prepare("DELETE FROM customers WHERE company_id = $1")
	if err != nil {
		return ErrorFunc(err)
	}

	res, err := stmt.Exec(companyId)
	if err != nil {
		return ErrorFunc(err)
	}

	affect, err := res.RowsAffected()
	if err != nil {
		return ErrorFunc(err)
	}

	return uint64(affect), nil
}