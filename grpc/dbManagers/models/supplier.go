package models

import (
	"github.com/jmoiron/sqlx"
	log "github.com/Sirupsen/logrus"
	pb "bitbucket.org/kmomuke/rentau/grpc/proto"
)

//type SupplierRequest struct {
//	SupplierId        uint64
//	SupplierImagePath string
//	CompanyName       string
//	ContactFname      string
//	PhoneNumber       string
//	Address           string
//}

type Supplier struct {
	supplierId uint64 `db:"supplier_id"`
	companyId uint64 `db:"company_id"`
	supplierImagePath string `db:"supplier_image_path"`
	companyName string `db:"company_name"`
	contactFname string `db:"contact_fname"`
	phoneNumber string `db:"phone_number"`
	address float32 `db:"address"`
}

//CREATE TABLE IF NOT EXISTS suppliers (
//	supplier_id BIGSERIAL PRIMARY KEY NOT NULL,
//	company_id BIGINT,
//	supplier_image_path varchar (400),
//	company_name varchar (400),
//	contact_fname varchar (400),
//	phone_number varchar (400),
//	address varchar (400),
//	updated_at BIGINT
//);

func StoreSupplier(tx *sqlx.Tx, supplierRequest *pb.SupplierRequest, companyId uint64) (uint64, error)  {

	var lastInsertId uint64
	err := tx.QueryRow("INSERT INTO suppliers(" +
		"company_id, " +
		"supplier_image_path, " +
		"company_name, " +
		"contact_fname, " +
		"phone_number, " +
		"address, " +
		"updated_at) VALUES($1, $2, $3, $4, $5, $6, $7) returning supplier_id;",
		companyId,
		supplierRequest.SupplierImagePath,
		supplierRequest.CompanyName,
		supplierRequest.ContactFname,
		supplierRequest.PhoneNumber,
		supplierRequest.Address,
		updatedAt()).Scan(&lastInsertId)

	if err != nil {
		return ErrorFunc(err)
	}

	return lastInsertId, nil
}

func UpdateSupplier(tx *sqlx.Tx, supplierRequest *pb.SupplierRequest, companyId uint64) (uint64, error)  {

	stmt, err := tx.Prepare("UPDATE suppliers SET " +
		"company_id=$1, " +
		"supplier_image_path=$2, " +
		"company_name=$3, " +
		"contact_fname=$4, " +
		"phone_number=$5, " +
		"address=$6, " +
		"updated_at=$7 WHERE supplier_id=$8")

	if err != nil {
		return ErrorFunc(err)
	}

	res, err := stmt.Exec(
		companyId,
		supplierRequest.SupplierImagePath,
		supplierRequest.CompanyName,
		supplierRequest.ContactFname,
		supplierRequest.PhoneNumber,
		supplierRequest.Address,
		updatedAt(),
		supplierRequest.SupplierId)

	if err != nil {
		return ErrorFunc(err)
	}

	affect, err := res.RowsAffected()
	if err != nil {
		return ErrorFunc(err)
	}

	return uint64(affect), nil
}

var selectSupplierRow string =
		"supplier_id, " +
		"supplier_image_path, " +
		"company_name, " +
		"contact_fname, " +
		"phone_number, " +
		"address "

func scanSupplierRows(rows *sqlx.Rows) ([]*pb.SupplierRequest, error) {
	suppliers := make([]*pb.SupplierRequest, 0)
	for rows.Next() {
		supplier := new(pb.SupplierRequest)
		err := rows.Scan(
			&supplier.SupplierId,
			&supplier.SupplierImagePath,
			&supplier.CompanyName,
			&supplier.ContactFname,
			&supplier.PhoneNumber,
			&supplier.Address)
		if err != nil {
			return nil, err
		}
		suppliers = append(suppliers, supplier)
	}

	if err := rows.Err(); err != nil {
		return nil, err
	}

	return  suppliers, nil
}

func AllSuppliersForCompany(db *sqlx.DB, companyId uint64) ([]*pb.SupplierRequest, error) {

	rows, err := db.Queryx("SELECT " +
		selectSupplierRow +
		"FROM suppliers WHERE company_id=$1 ORDER BY supplier_id ASC", companyId)

	if err != nil {
		log.WithFields(log.Fields{"err": err}).Warn("")
		return nil, err
	}

	suppliers, err:= scanSupplierRows(rows)
	if err != nil {
		log.WithFields(log.Fields{"err": err}).Warn("")
		return nil, err
	}

	return suppliers, nil
}

func AllSuppliersForUpdate(db *sqlx.DB, filter *pb.SupplierFilter, companyId uint64) ([]*pb.SupplierRequest, error) {

	rows, err := db.Queryx("SELECT " +
		selectSupplierRow +
		"FROM suppliers WHERE updated_at >= $1 AND company_id=$2 LIMIT $3", filter.SupplierUpdatedAt, companyId, 1000)

	if err != nil {
		log.WithFields(log.Fields{"err": err}).Warn("")
		print("error")
	}

	suppliers, err:= scanSupplierRows(rows)
	if err != nil {
		log.WithFields(log.Fields{"err": err}).Warn("")
		return nil, err
	}

	return suppliers, nil
}

func DeleteSuppliers(tx *sqlx.Tx, companyId uint64) (uint64, error) {

	stmt, err := tx.Prepare("DELETE FROM suppliers WHERE company_id = $1")
	if err != nil {
		return ErrorFunc(err)
	}

	res, err := stmt.Exec(companyId)
	if err != nil {
		return ErrorFunc(err)
	}

	affect, err := res.RowsAffected()
	if err != nil {
		return ErrorFunc(err)
	}

	return uint64(affect), nil
}