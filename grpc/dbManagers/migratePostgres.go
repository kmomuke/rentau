package dbManagers
import (
	"database/sql"
	_ "github.com/lib/pq"
	"github.com/mattes/migrate"
	"github.com/mattes/migrate/database/postgres"
	_ "github.com/mattes/migrate/source/file"
	"fmt"
)

// Dev
// Production
// Test

const (
	//DevelopmentPath = "user=kanybek dbname=multi_stock password=nazgulum host=localhost port=5432 sslmode=disable"
	DevelopmentPath = "user=kanybek dbname=rentau password=nazgulum host=localhost port=5432 sslmode=disable"
	TestPath = "dbname=template1 host=localhost sslmode=disable"
)

func MigrateDatabaseUp(path string, migrationsPath string) {
	db, err := sql.Open("postgres", path)
	defer db.Close()

	if err != nil {
		fmt.Println(err)
	}

	driver, err := postgres.WithInstance(db, &postgres.Config{})
	m, err := migrate.NewWithDatabaseInstance(
		migrationsPath,
		"postgres", driver)

	if err != nil {
		fmt.Println(err)
	}

	m.Up()

	defer m.Close()
}

//migrate create -ext sql -dir migrations create_user