package models

import (
	"testing"

	pb "bitbucket.org/kmomuke/rentau/grpc/proto"
	"github.com/stretchr/testify/assert"
)

func TestCreateCompany(t *testing.T) {

	company := new(pb.CompanyRequest)
	company.CompanyName = "CompanyName"
	company.Email = "EmailEmail"
	company.Address = "Address"

	companyId, err := dbMng.CreateCompany(company)

	assert.Equal(t, companyId, uint64(1), "they should be equal")
	assert.Nil(t, err, "error should be nil")
	assert.NotEqual(t, 0, companyId, "userId  should not be equal to zero")
	if assert.NotNil(t, company) {
	}
}

func TestUpdateCompany(t *testing.T) {

	company := new(pb.CompanyRequest)
	company.CompanyId = 1
	company.CompanyName = "CompanyNameCompanyNameCompanyName"
	company.Email = "EmailEmailEmailEmail"
	company.Address = "AddressAddressAddressAddress"

	rowsAffected, err := dbMng.UpdateCompany(company)
	assert.NotEqual(t, 0, rowsAffected, "userId  should not be equal to zero")
	assert.Nil(t, err, "error should be nil")

	company2, err := dbMng.CompanyFor(1)

	assert.Equal(t, company2.CompanyId, uint64(1), "they should be equal")
	assert.Equal(t, company2.CompanyName, "CompanyNameCompanyNameCompanyName", "CompanyNameCompanyNameCompanyName")
	assert.Equal(t, company2.Email, "EmailEmailEmailEmail", "EmailEmailEmailEmail")
	assert.Equal(t, company2.Address, "AddressAddressAddressAddress", "AddressAddressAddressAddress")

	assert.Nil(t, err, "error should be nil")
	assert.NotEqual(t, 0, company2.CompanyId, "AccountId  should not be equal to zero")
	if assert.NotNil(t, company2) {

	}
}
