package models

import (
	"testing"

	pb "bitbucket.org/kmomuke/rentau/grpc/proto"
	"github.com/stretchr/testify/assert"
)

//type SupplierRequest struct {
//	SupplierId        uint64
//	SupplierImagePath string
//	CompanyName       string
//	ContactFname      string
//	PhoneNumber       string
//	Address           string
//}

func TestCreateSupplier(t *testing.T) {

	supplier := new(pb.SupplierRequest)
	//supplier.SupplierId
	supplier.SupplierImagePath = "SupplierImagePath"
	supplier.CompanyName = "CompanyName"
	supplier.ContactFname = "ContactFname"
	supplier.PhoneNumber = "PhoneNumber"
	supplier.Address = "Address"

	supplierId, err := dbMng.CreateSupplier(supplier, 3000)

	assert.Equal(t, supplierId, uint64(1), "they should be equal")
	assert.Nil(t, err, "error should be nil")
	assert.NotEqual(t, 0, supplierId, "userId  should not be equal to zero")
	if assert.NotNil(t, supplier) {
	}

	suppliers, err := dbMng.AllSuppliersForInitial(3000)
	for _, supplier := range suppliers {
		assert.Equal(t, supplier.SupplierId, uint64(1), "they should be equal")
		assert.Equal(t, supplier.SupplierImagePath, "SupplierImagePath", "CategoryName")
		assert.Equal(t, supplier.CompanyName, "CompanyName", "CategoryName")
		assert.Equal(t, supplier.ContactFname, "ContactFname", "CategoryName")
		assert.Equal(t, supplier.PhoneNumber, "PhoneNumber", "CategoryName")
		assert.Equal(t, supplier.Address, "Address", "CategoryName")
	}

	assert.Nil(t, err, "error should be nil")
	if assert.NotNil(t, suppliers) {
	}
}

func TestUpdateSupplier(t *testing.T) {

	supplier := new(pb.SupplierRequest)
	supplier.SupplierId = uint64(1)
	supplier.SupplierImagePath = "SupplierImagePathSupplierImagePath"
	supplier.CompanyName = "CompanyNameCompanyName"
	supplier.ContactFname = "ContactFnameContactFname"
	supplier.PhoneNumber = "PhoneNumberPhoneNumber"
	supplier.Address = "AddressAddress"

	rowsAffected, err := dbMng.UpdateSupplier(supplier, 3000)

	assert.Equal(t, rowsAffected, uint64(1), "they should be equal")
	assert.Nil(t, err, "error should be nil")
	assert.NotEqual(t, 0, rowsAffected, "userId  should not be equal to zero")
	if assert.NotNil(t, supplier) {
	}

	suppliers, err := dbMng.AllSuppliersForInitial(3000)
	for _, supplier := range suppliers {
		assert.Equal(t, supplier.SupplierId, uint64(1), "they should be equal")
		assert.Equal(t, supplier.SupplierImagePath, "SupplierImagePathSupplierImagePath", "CategoryName")
		assert.Equal(t, supplier.CompanyName, "CompanyNameCompanyName", "CategoryName")
		assert.Equal(t, supplier.ContactFname, "ContactFnameContactFname", "CategoryName")
		assert.Equal(t, supplier.PhoneNumber, "PhoneNumberPhoneNumber", "CategoryName")
		assert.Equal(t, supplier.Address, "AddressAddress", "CategoryName")
	}

	assert.Nil(t, err, "error should be nil")
	if assert.NotNil(t, suppliers) {
	}
}
