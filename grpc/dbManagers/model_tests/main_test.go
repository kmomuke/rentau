package models

import (
	"testing"
	"os"
	"log"
	"bitbucket.org/kmomuke/rentau/grpc/dbManagers"
)

var dbMng *dbManagers.DbManager

func init() {
	//dbManagers.MigrateDatabaseDown(dbManagers.TestPath, "file://../migrations/")
	dbManagers.MigrateDatabaseUp(dbManagers.TestPath, "file://../migrations/")
	dbMng = dbManagers.NewDbManager(dbManagers.TestPath)
}

func TestMain(m *testing.M) {
	log.Println("This gets run BEFORE any tests get run!")
	exitVal := m.Run()
	log.Println("This gets run AFTER any tests get run!")
	os.Exit(exitVal)
}

func TestOne(t *testing.T) {
	log.Println("TestOne running")
}

func TestTwo(t *testing.T) {
	log.Println("TestTwo running")
}

//go test -v
