package models

import (
	"testing"

	pb "bitbucket.org/kmomuke/rentau/grpc/proto"
	uuid "github.com/satori/go.uuid"
	"github.com/stretchr/testify/assert"
)

//type ProductRequest struct {
//	ProductId        uint64
//	ProductImagePath string
//	ProductName      string
//	SupplierId       uint64
//	CategoryId       uint64
//	Barcode          string
//	QuantityPerUnit  string
//	SaleUnitPrice    float64
//	IncomeUnitPrice  float64
//	UnitsInStock     float64
//}

func TestCreateProduct(t *testing.T) {

	product := new(pb.ProductRequest)
	//product.ProductId
	product.ProductImagePath = "ProductImagePath"
	product.ProductName = "ProductName"
	product.SupplierId = uint64(1212)
	product.CategoryId = uint64(3131)
	product.Barcode = "Barcode"
	product.QuantityPerUnit = "QuantityPerUnit"
	product.SaleUnitPrice = float64(20000)
	product.IncomeUnitPrice = float64(10000)
	product.UnitsInStock = float64(1200)

	productId, err := dbMng.CreateProduct(product, 3000)

	assert.Equal(t, productId, uint64(1), "they should be equal")
	assert.Nil(t, err, "error should be nil")
	assert.NotEqual(t, 0, productId, "userId  should not be equal to zero")
	if assert.NotNil(t, product) {
	}

	products, err := dbMng.AllProductsForInitial(3000)
	for _, product := range products {
		assert.Equal(t, product.ProductId, uint64(1), "they should be equal")
		assert.Equal(t, product.ProductImagePath, "ProductImagePath", "CategoryName")
		assert.Equal(t, product.ProductName, "ProductName", "CategoryName")
		assert.Equal(t, product.SupplierId, uint64(1212), "CategoryName")
		assert.Equal(t, product.CategoryId, uint64(3131), "CategoryName")
		assert.Equal(t, product.Barcode, "Barcode", "CategoryName")
		assert.Equal(t, product.QuantityPerUnit, "QuantityPerUnit", "CategoryName")
		assert.Equal(t, product.SaleUnitPrice, float64(20000), "CategoryName")
		assert.Equal(t, product.IncomeUnitPrice, float64(10000), "CategoryName")
		assert.Equal(t, product.UnitsInStock, float64(1200), "CategoryName")
	}

	assert.Nil(t, err, "error should be nil")
	if assert.NotNil(t, products) {
	}
}

func TestUpdateProduct(t *testing.T) {

	product := new(pb.ProductRequest)
	product.ProductId = uint64(1)
	product.ProductImagePath = "ProductImagePathProductImagePath"
	product.ProductName = "ProductNameProductName"
	product.SupplierId = uint64(12120)
	product.CategoryId = uint64(31310)
	product.Barcode = "BarcodeBarcodeBarcode"
	product.QuantityPerUnit = "QuantityPerUnitQuantityPerUnit"
	product.SaleUnitPrice = float64(120000)
	product.IncomeUnitPrice = float64(110000)
	product.UnitsInStock = float64(11200)

	productId, err := dbMng.UpdateProduct(product, 3000)

	assert.Equal(t, productId, uint64(1), "they should be equal")
	assert.Nil(t, err, "error should be nil")
	assert.NotEqual(t, 0, productId, "userId  should not be equal to zero")
	if assert.NotNil(t, product) {
	}

	products, err := dbMng.AllProductsForInitial(3000)
	for _, product := range products {
		assert.Equal(t, product.ProductId, uint64(1), "they should be equal")
		assert.Equal(t, product.ProductImagePath, "ProductImagePathProductImagePath", "CategoryName")
		assert.Equal(t, product.ProductName, "ProductNameProductName", "CategoryName")
		assert.Equal(t, product.SupplierId, uint64(12120), "CategoryName")
		assert.Equal(t, product.CategoryId, uint64(31310), "CategoryName")
		assert.Equal(t, product.Barcode, "BarcodeBarcodeBarcode", "CategoryName")
		assert.Equal(t, product.QuantityPerUnit, "QuantityPerUnitQuantityPerUnit", "CategoryName")
		assert.Equal(t, product.SaleUnitPrice, float64(120000), "CategoryName")
		assert.Equal(t, product.IncomeUnitPrice, float64(110000), "CategoryName")
		assert.Equal(t, product.UnitsInStock, float64(11200), "CategoryName")
	}

	assert.Nil(t, err, "error should be nil")
	if assert.NotNil(t, products) {
	}
}

func TestProductIncrease(t *testing.T) {

	var date = updatedAt()
	uuidLocal, _ := uuid.NewV4()
	var uudidd = uuidLocal.String()

	orderDetail := new(pb.OrderDetailRequest)
	orderDetail.OrderDetailId = uint64(1)
	orderDetail.OrderId = uint64(1200)
	orderDetail.OrderDetailDate = date
	orderDetail.IsLast = uint32(1)
	orderDetail.ProductId = uint64(1)
	orderDetail.BillingNo = "BillingNo"
	orderDetail.OrderDetailComment = "OrderDetailComment"
	orderDetail.Price = float64(43242)
	orderDetail.OrderQuantity = float64(11200)
	orderDetail.OrderDetailUUID = uudidd
	orderDetail.ProductQuantity = float64(12)

	orderDetails := make([]*pb.OrderDetailRequest, 0)
	orderDetails = append(orderDetails, orderDetail)

	rowsAffected, err := dbMng.IncreaseProductsInStock(orderDetails)

	assert.Equal(t, rowsAffected, uint64(1), "they should be equal")
	assert.Nil(t, err, "error should be nil")
	assert.NotEqual(t, 0, rowsAffected, "userId  should not be equal to zero")
	if assert.NotNil(t, orderDetail) {
	}

	products, err := dbMng.AllProductsForInitial(3000)
	for _, product := range products {
		assert.Equal(t, product.ProductId, uint64(1), "they should be equal")
		assert.Equal(t, product.ProductImagePath, "ProductImagePathProductImagePath", "CategoryName")
		assert.Equal(t, product.ProductName, "ProductNameProductName", "CategoryName")
		assert.Equal(t, product.SupplierId, uint64(12120), "CategoryName")
		assert.Equal(t, product.CategoryId, uint64(31310), "CategoryName")
		assert.Equal(t, product.Barcode, "BarcodeBarcodeBarcode", "CategoryName")
		assert.Equal(t, product.QuantityPerUnit, "QuantityPerUnitQuantityPerUnit", "CategoryName")
		assert.Equal(t, product.SaleUnitPrice, float64(120000), "CategoryName")
		assert.Equal(t, product.IncomeUnitPrice, float64(110000), "CategoryName")
		assert.Equal(t, product.UnitsInStock, float64(22400), "CategoryName")
	}

	assert.Nil(t, err, "error should be nil")
	if assert.NotNil(t, products) {
	}
}

func TestProductDecrease(t *testing.T) {

	var date = updatedAt()
	uuidLocal, _ := uuid.NewV4()
	var uudidd = uuidLocal.String()

	orderDetail := new(pb.OrderDetailRequest)
	orderDetail.OrderDetailId = uint64(1)
	orderDetail.OrderId = uint64(1200)
	orderDetail.OrderDetailDate = date
	orderDetail.IsLast = uint32(1)
	orderDetail.ProductId = uint64(1)
	orderDetail.BillingNo = "BillingNo"
	orderDetail.OrderDetailComment = "OrderDetailComment"
	orderDetail.Price = float64(43242)
	orderDetail.OrderQuantity = float64(11200)
	orderDetail.OrderDetailUUID = uudidd
	orderDetail.ProductQuantity = float64(12)

	orderDetails := make([]*pb.OrderDetailRequest, 0)
	orderDetails = append(orderDetails, orderDetail)

	rowsAffected, err := dbMng.DecreaseProductsInStock(orderDetails)

	assert.Equal(t, rowsAffected, uint64(1), "they should be equal")
	assert.Nil(t, err, "error should be nil")
	assert.NotEqual(t, 0, rowsAffected, "userId  should not be equal to zero")
	if assert.NotNil(t, orderDetail) {
	}

	products, err := dbMng.AllProductsForInitial(3000)
	for _, product := range products {
		assert.Equal(t, product.ProductId, uint64(1), "they should be equal")
		assert.Equal(t, product.ProductImagePath, "ProductImagePathProductImagePath", "CategoryName")
		assert.Equal(t, product.ProductName, "ProductNameProductName", "CategoryName")
		assert.Equal(t, product.SupplierId, uint64(12120), "CategoryName")
		assert.Equal(t, product.CategoryId, uint64(31310), "CategoryName")
		assert.Equal(t, product.Barcode, "BarcodeBarcodeBarcode", "CategoryName")
		assert.Equal(t, product.QuantityPerUnit, "QuantityPerUnitQuantityPerUnit", "CategoryName")
		assert.Equal(t, product.SaleUnitPrice, float64(120000), "CategoryName")
		assert.Equal(t, product.IncomeUnitPrice, float64(110000), "CategoryName")
		assert.Equal(t, product.UnitsInStock, float64(11200), "CategoryName")
	}

	assert.Nil(t, err, "error should be nil")
	if assert.NotNil(t, products) {
	}
}
