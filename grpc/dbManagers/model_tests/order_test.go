package models

import (
	"fmt"
	"testing"

	pb "bitbucket.org/kmomuke/rentau/grpc/proto"
	uuid "github.com/satori/go.uuid"
	"github.com/stretchr/testify/assert"
)

//type OrderRequest struct {
//	OrderId           uint64
//	OrderDocument     uint32
//	MoneyMovementType uint32
//	BillingNo         string
//	UserId            uint64
//	CustomerId        uint64
//	SupplierId        uint64
//	OrderDate         uint64
//	PaymentId         uint64
//	ErrorMsg          string
//	Comment           string
//      OrderUUID         string
//	IsDeleted         uint32
//	IsMoneyForDebt    uint32
//	IsEdited          uint32
//}

func TestCreateOrder(t *testing.T) {

	var date = updatedAt()

	uuidLocal, _ := uuid.NewV4()
	var uudidd = uuidLocal.String()

	order := new(pb.OrderRequest)
	//order.OrderId
	order.OrderDocument = uint32(1200)
	order.OrderMovement = uint32(22200)
	order.MoneyMovementType = uint32(1200)
	order.BillingNo = "BillingNo"
	order.UserId = uint64(400)

	order.CustomerId = uint64(4000)
	order.SupplierId = uint64(2000)
	order.OrderDate = date
	order.PaymentId = uint64(2200)

	order.ErrorMsg = "ErrorMsg"
	order.Comment = "Comment"
	order.OrderUUID = uudidd
	order.IsDeleted = uint32(1)
	order.IsMoneyForDebt = uint32(1)
	order.IsEdited = uint32(1)

	orderId, err := dbMng.CreateOrder(order, 3000)

	fmt.Printf("orderId ==> %d\n", orderId)

	assert.Equal(t, orderId, uint64(4), "they should be equal")
	assert.Nil(t, err, "error should be nil")
	assert.NotEqual(t, 0, orderId, "userId  should not be equal to zero")
	if assert.NotNil(t, order) {
	}

	orderFilter := new(pb.OrderFilter)
	orderFilter.OrderDate = uint64(date + uint64(10000))

	orders, err := dbMng.AllOrdersForInitial(orderFilter, 3000)

	for _, order := range orders {
		assert.Equal(t, order.OrderId, uint64(1), "they should be equal")
		assert.Equal(t, order.OrderDocument, uint32(1200), "they should be equal")
		assert.Equal(t, order.OrderMovement, uint32(22200), "they should be equal")
		assert.Equal(t, order.MoneyMovementType, uint32(1200), "they should be equal")
		assert.Equal(t, order.BillingNo, "BillingNo", "they should be equal")
		assert.Equal(t, order.UserId, uint64(400), "they should be equal")

		assert.Equal(t, order.CustomerId, uint64(4000), "CategoryName")
		assert.Equal(t, order.SupplierId, uint64(2000), "CategoryName")
		assert.Equal(t, order.OrderDate, date, "CategoryName")
		assert.Equal(t, order.PaymentId, uint64(2200), "CategoryName")

		assert.Equal(t, order.ErrorMsg, "ErrorMsg", "CategoryName")
		assert.Equal(t, order.Comment, "Comment", "CategoryName")
		assert.Equal(t, order.OrderUUID, uudidd, "CategoryName")
		assert.Equal(t, order.IsDeleted, uint32(1), "CategoryName")
		assert.Equal(t, order.IsMoneyForDebt, uint32(1), "CategoryName")
		assert.Equal(t, order.IsEdited, uint32(1), "CategoryName")
	}

	assert.Nil(t, err, "error should be nil")
	if assert.NotNil(t, order) {
	}
}

func TestUpdateOrder(t *testing.T) {

	var date = updatedAt()
	uuidLocal, _ := uuid.NewV4()
	var uudidd = uuidLocal.String()

	order := new(pb.OrderRequest)
	order.OrderId = uint64(4)
	order.OrderDocument = uint32(3200)
	order.OrderMovement = uint32(22200)
	order.MoneyMovementType = uint32(3200)
	order.BillingNo = "BillingNoBillingNo"
	order.UserId = uint64(400400)

	order.CustomerId = uint64(5000)
	order.SupplierId = uint64(5000)
	order.OrderDate = date
	order.PaymentId = uint64(600)

	order.ErrorMsg = "ErrorMsgErrorMsg"
	order.Comment = "CommentComment"
	order.OrderUUID = uudidd
	order.IsDeleted = uint32(0)
	order.IsMoneyForDebt = uint32(0)
	order.IsEdited = uint32(0)

	orderId, err := dbMng.UpdateOrder(order, 3000)

	assert.Equal(t, orderId, uint64(1), "they should be equal")
	assert.Nil(t, err, "error should be nil")
	assert.NotEqual(t, 0, orderId, "userId  should not be equal to zero")
	if assert.NotNil(t, order) {
	}

	orderFilter := new(pb.OrderFilter)
	orderFilter.OrderDate = uint64(date + uint64(10000))

	orders, err := dbMng.AllOrdersForInitial(orderFilter, 3000)

	for _, order := range orders {
		assert.Equal(t, order.OrderId, uint64(1), "they should be equal")
		assert.Equal(t, order.OrderDocument, uint32(3200), "they should be equal")
		assert.Equal(t, order.OrderMovement, uint32(22200), "they should be equal")
		assert.Equal(t, order.MoneyMovementType, uint32(3200), "they should be equal")
		assert.Equal(t, order.BillingNo, "BillingNoBillingNo", "they should be equal")
		assert.Equal(t, order.UserId, uint64(400400), "they should be equal")

		assert.Equal(t, order.CustomerId, uint64(5000), "CategoryName")
		assert.Equal(t, order.SupplierId, uint64(5000), "CategoryName")
		assert.Equal(t, order.OrderDate, date, "CategoryName")
		assert.Equal(t, order.PaymentId, uint64(600), "CategoryName")

		assert.Equal(t, order.ErrorMsg, "ErrorMsgErrorMsg", "CategoryName")
		assert.Equal(t, order.Comment, "CommentComment", "CategoryName")
		assert.Equal(t, order.OrderUUID, uudidd, "CategoryName")
		assert.Equal(t, order.IsDeleted, uint32(0), "CategoryName")
		assert.Equal(t, order.IsMoneyForDebt, uint32(0), "CategoryName")
		assert.Equal(t, order.IsEdited, uint32(0), "CategoryName")
	}

	assert.Nil(t, err, "error should be nil")
	if assert.NotNil(t, order) {
	}
}
