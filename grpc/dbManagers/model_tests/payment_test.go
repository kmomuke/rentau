package models

import (
	"testing"
	"github.com/stretchr/testify/assert"
	pb "bitbucket.org/kmomuke/rentau/grpc/proto"
)

func TestCreatePayment(t *testing.T) {

	payment := new(pb.PaymentRequest)

	payment.TotalOrderPrice = float64(123)
	payment.Discount = float64(223)
	payment.TotalPriceWithDiscount = float64(323)
	payment.MinusPrice = float64(54321)
	payment.PlusPrice = float64(4321)
	payment.Comment = "KOKO_JAMBO"

	paymentId, err := dbMng.CreatePayment(payment)

	assert.Equal(t, paymentId, uint64(4), "they should be equal")
	assert.Nil(t, err, "error should be nil")
	assert.NotEqual(t, 0, paymentId, "userId  should not be equal to zero")
	if assert.NotNil(t, payment) {
	}

	payment_, err := dbMng.PaymentFor(paymentId)

	assert.Equal(t, payment_.PaymentId, uint64(4), "they should be equal")
	assert.Equal(t, payment_.TotalOrderPrice, float64(123), "CompanyNameCompanyNameCompanyName")
	assert.Equal(t, payment_.Discount, float64(223), "EmailEmailEmailEmail")
	assert.Equal(t, payment_.TotalPriceWithDiscount, float64(323), "AddressAddressAddressAddress")
	assert.Equal(t, payment_.PlusPrice, float64(4321), "AddressAddressAddressAddress")
	assert.Equal(t, payment_.MinusPrice, float64(54321), "AddressAddressAddressAddress")
	assert.Equal(t, payment_.Comment, "KOKO_JAMBO", "AddressAddressAddressAddress")


	assert.Nil(t, err, "error should be nil")
	assert.NotEqual(t, 0, payment_.PaymentId, "AccountId  should not be equal to zero")
	if assert.NotNil(t, payment_) {
	}
}