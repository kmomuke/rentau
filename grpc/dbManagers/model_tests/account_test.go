package models

import (
	"testing"
	"github.com/stretchr/testify/assert"
	pb "bitbucket.org/kmomuke/rentau/grpc/proto"
)

func TestCreateAccount(t *testing.T) {

	account := new(pb.AccountRequest)
	account.CustomerId = 2400
	account.SupplierId = 2411
	account.Balance = float64(240032)
	accountId, err := dbMng.CreateAccount(account)

	assert.Equal(t, accountId, uint64(1), "they should be equal")
	assert.Nil(t, err, "error should be nil")
	assert.NotEqual(t, 0, accountId, "userId  should not be equal to zero")
	if assert.NotNil(t, account) {
	}
}

func TestUpdateAccountForCustomer(t *testing.T)  {

	account, err := dbMng.AccountForCustomer(2400)
	assert.Equal(t, account.AccountId, uint64(1), "they should be equal")
	assert.Equal(t, account.CustomerId, uint64(2400), "they should be equal")
	assert.Equal(t, account.SupplierId, uint64(2411), "they should be equal")
	assert.Equal(t, account.Balance, float64(240032), "they should be equal")

	assert.Nil(t, err, "error should be nil")
	assert.NotEqual(t, 0, account.AccountId, "AccountId  should not be equal to zero")
	if assert.NotNil(t, account) {
	}

	rowsAffected, err := dbMng.UpdateCustomerBallance(2400, 100000)
	assert.Nil(t, err, "error should be nil")
	assert.NotEqual(t, uint64(0), rowsAffected, "AccountId  should not be equal to zero")

	account_, err := dbMng.AccountForCustomer(2400)
	assert.Equal(t, account_.Balance, float64(100000), "they should be equal")


	rowsAffected2, err := dbMng.UpdateSupplierBallance(2411, 200000)
	assert.Nil(t, err, "error should be nil")
	assert.NotEqual(t, uint64(0), rowsAffected2, "AccountId  should not be equal to zero")


	account2, err := dbMng.AccountForSupplier(2411)
	assert.Equal(t, account2.Balance, float64(200000), "they should be equal")

}
