package models

import (
	"testing"
	"github.com/stretchr/testify/assert"
	pb "bitbucket.org/kmomuke/rentau/grpc/proto"
)

func TestCreateCustomer(t *testing.T) {

	customer := new(pb.CustomerRequest)
	//customer.CustomerId
	customer.CustomerImagePath = "CustomerImagePath"
	customer.FirstName = "FirstName"
	customer.SecondName = "SecondName"
	customer.PhoneNumber = "PhoneNumber"
	customer.Address = "Address"
	customer.UserId = uint64(1200)

	customerId, err := dbMng.CreateCustomer(customer, 3000)

	assert.Equal(t, customerId, uint64(1), "they should be equal")
	assert.Nil(t, err, "error should be nil")
	assert.NotEqual(t, 0, customerId, "userId  should not be equal to zero")
	if assert.NotNil(t, customer) {
	}


	customers, err := dbMng.AllCustomersForInitial(3000)
	for _, customer := range customers {
		assert.Equal(t, customer.CustomerId, uint64(1), "they should be equal")
		assert.Equal(t, customer.CustomerImagePath, "CustomerImagePath", "CategoryName")
		assert.Equal(t, customer.FirstName, "FirstName", "CategoryName")
		assert.Equal(t, customer.SecondName, "SecondName", "CategoryName")
		assert.Equal(t, customer.PhoneNumber, "PhoneNumber", "CategoryName")
		assert.Equal(t, customer.Address, "Address", "CategoryName")
		assert.Equal(t, customer.UserId, uint64(1200), "CategoryName")
	}

	assert.Nil(t, err, "error should be nil")
	if assert.NotNil(t, customers) {
	}
}

func TestUpdateCustomer(t *testing.T)  {

	customer := new(pb.CustomerRequest)
	customer.CustomerId = uint64(1)
	customer.CustomerImagePath = "CustomerImagePathCustomerImagePath"
	customer.FirstName = "FirstNameFirstName"
	customer.SecondName = "SecondNameSecondName"
	customer.PhoneNumber = "PhoneNumberPhoneNumber"
	customer.Address = "AddressAddress"
	customer.UserId = uint64(120012)

	rowsAffected, err := dbMng.UpdateCustomer(customer, 3000)
	assert.NotEqual(t, 0, rowsAffected, "userId  should not be equal to zero")
	assert.Nil(t, err, "error should be nil")

	customers, err := dbMng.AllCustomersForInitial(3000)
	for _, customer := range customers {
		assert.Equal(t, customer.CustomerId, uint64(1), "they should be equal")
		assert.Equal(t, customer.CustomerImagePath, "CustomerImagePathCustomerImagePath", "CategoryName")
		assert.Equal(t, customer.FirstName, "FirstNameFirstName", "CategoryName")
		assert.Equal(t, customer.SecondName, "SecondNameSecondName", "CategoryName")
		assert.Equal(t, customer.PhoneNumber, "PhoneNumberPhoneNumber", "CategoryName")
		assert.Equal(t, customer.Address, "AddressAddress", "CategoryName")
		assert.Equal(t, customer.UserId, uint64(120012), "CategoryName")
	}

	assert.Nil(t, err, "error should be nil")
	if assert.NotNil(t, customers) {
	}
}