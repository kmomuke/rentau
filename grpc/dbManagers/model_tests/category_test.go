package models

import (
	"testing"
	"github.com/stretchr/testify/assert"
	pb "bitbucket.org/kmomuke/rentau/grpc/proto"
)

func TestCreateCategory(t *testing.T) {

	category := new(pb.CategoryRequest)
	category.CategoryName = "CategoryName"

	categoryId, err := dbMng.CreateCategory(category, 3000)

	assert.Equal(t, categoryId, uint64(1), "they should be equal")
	assert.Nil(t, err, "error should be nil")
	assert.NotEqual(t, 0, categoryId, "userId  should not be equal to zero")
	if assert.NotNil(t, category) {
	}
}

func TestUpdateCategory(t *testing.T)  {

	category := new(pb.CategoryRequest)
	category.CategoryId = 1
	category.CategoryName = "CategoryName"

	rowsAffected, err := dbMng.UpdateCategory(category, 3000)
	assert.NotEqual(t, 0, rowsAffected, "userId  should not be equal to zero")
	assert.Nil(t, err, "error should be nil")

	categories, err := dbMng.AllCategoriesForInitial(3000)
	for _, category := range categories {
		assert.Equal(t, category.CategoryId, uint64(1), "they should be equal")
		assert.Equal(t, category.CategoryName, "CategoryName", "CategoryName")
	}

	assert.Nil(t, err, "error should be nil")
	if assert.NotNil(t, categories) {
	}
}