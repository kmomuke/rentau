package models

import (
	"fmt"
	"testing"
	"time"

	"bitbucket.org/kmomuke/rentau/grpc/dbManagers"
	pb "bitbucket.org/kmomuke/rentau/grpc/proto"
	uuid "github.com/satori/go.uuid"
	"github.com/stretchr/testify/assert"
)

func TestCreateUser(t *testing.T) {

	user := new(pb.UserRequest)

	uuidLocal, _ := uuid.NewV4()
	var uuid string = uuidLocal.String()
	print(uuid)

	user.UserUUID = uuid
	user.RoleId = 1000
	user.UserImagePath = "UserImagePath"
	user.FirstName = "FirstName"
	user.SecondName = "SecondName"
	user.Email = "Email"
	user.Password = "Password"
	user.PhoneNumber = "PhoneNumber"
	user.Address = "Address"

	var companyId uint64
	companyId = 20000

	userId, err := dbMng.CreateUser(user, companyId)

	assert.Equal(t, userId, uint64(1), "they should be equal")
	assert.Nil(t, err, "error should be nil")
	assert.NotEqual(t, 0, userId, "userId  should not be equal to zero")
	if assert.NotNil(t, user) {
	}
}

func TestUpdateUser(t *testing.T) {
	user := new(pb.UserRequest)

	user.UserId = 1
	user.RoleId = 2000
	user.UserImagePath = "UserImagePathUserImagePath"
	user.FirstName = "FirstNameFirstName"
	user.SecondName = "SecondNameSecondName"
	user.Email = "EmailEmail"
	user.Password = "PasswordPassword"
	user.PhoneNumber = "PhoneNumberPhoneNumber"
	user.Address = "AddressAddressAddress"

	var companyId uint64
	companyId = 40000

	rowsAffected, err := dbMng.UpdateUser(user, companyId)
	assert.Nil(t, err, "error should be nil")
	assert.NotEqual(t, 0, rowsAffected, "rowsAffected should not be equal to zero")

	users, err := dbMng.AllUsers(companyId)
	assert.Nil(t, err, "error should be nil")
	if assert.NotNil(t, users) {
	}
	assert.NotEqual(t, 0, len(users), "users.count should not be equal to zero")

	for _, user := range users {
		assert.Equal(t, user.UserId, uint64(1), "they should be equal")
		assert.Equal(t, user.RoleId, uint64(2000), "they should be equal")
		assert.Equal(t, user.CompanyId, uint64(40000), "they should be equal")
		//assert.Equal(t, user.UserUUID, "23086476-adad-4a6b-9e87-75f5bc752474", "they should be equal")
		assert.Equal(t, user.UserImagePath, "UserImagePathUserImagePath", "they should be equal")
		assert.Equal(t, user.FirstName, "FirstNameFirstName", "they should be equal")
		assert.Equal(t, user.SecondName, "SecondNameSecondName", "they should be equal")
		assert.Equal(t, user.Email, "EmailEmail", "they should be equal")
		assert.Equal(t, user.Password, "PasswordPassword", "they should be equal")
		assert.Equal(t, user.PhoneNumber, "PhoneNumberPhoneNumber", "they should be equal")
		assert.Equal(t, user.Address, "AddressAddressAddress", "they should be equal")
	}
}

func TestAllUsers(t *testing.T) {

	var companyId uint64
	companyId = 20000

	users, err := dbMng.AllUsers(companyId)
	assert.Nil(t, err, "error should be nil")

	for _, user := range users {
		assert.Equal(t, user.UserId, uint64(1), "they should be equal")
		assert.Equal(t, user.RoleId, uint64(1000), "they should be equal")
		assert.Equal(t, user.UserImagePath, "UserImagePath", "they should be equal")
		assert.Equal(t, user.FirstName, "FirstName", "they should be equal")
		assert.Equal(t, user.SecondName, "SecondName", "they should be equal")
		assert.Equal(t, user.Email, "Email", "they should be equal")
		assert.Equal(t, user.Password, "Password", "they should be equal")
		assert.Equal(t, user.PhoneNumber, "PhoneNumber", "they should be equal")
		assert.Equal(t, user.Address, "Address", "they should be equal")
	}
}

func BenchmarkHello(b *testing.B) {
	for i := 0; i < 1000; i++ {
		go func(k int, dBMan *dbManagers.DbManager) {

			user := new(pb.UserRequest)

			user.RoleId = 1000
			user.UserImagePath = "UserImagePath"
			user.FirstName = fmt.Sprintf("FirstName is %d", k)
			user.SecondName = fmt.Sprintf("SecondName is %d", k)
			user.Email = fmt.Sprintf("Email is %d", k)
			user.Password = fmt.Sprintf("Password is %d", k)
			user.PhoneNumber = "PhoneNumber"
			user.Address = "Address"

			var companyId uint64
			companyId = 20000 + uint64(k)

			userId, err := dbMng.CreateUser(user, companyId)

			if err != nil {
				fmt.Println(err)
			} else {
				fmt.Println(userId)
			}
		}(i, dbMng)
	}
	time.Sleep(2 * 1e9)
}
