package models

import (
	"testing"

	pb "bitbucket.org/kmomuke/rentau/grpc/proto"
	uuid "github.com/satori/go.uuid"
	"github.com/stretchr/testify/assert"
)

//type TransactionRequest struct {
//	TransactionId     uint64
//	TransactionDate   uint64
//	IsLastTransaction uint32
//	TransactionType   uint32
//	MoneyAmount       float64
//	OrderId           uint64
//	CustomerId        uint64
//	SupplierId        uint64
//	UserId            uint64
//	Comment           string
//	TransactionUUID   string
//	BallanceAmount    float64
//}

func TestCreateTransaction(t *testing.T) {

	var date = updatedAt()
	uuidLocal, _ := uuid.NewV4()
	var uudidd = uuidLocal.String()

	transaction := new(pb.TransactionRequest)
	//transaction.TransactionId
	transaction.TransactionDate = date
	transaction.IsLastTransaction = uint32(1)
	transaction.TransactionType = uint32(1000)
	transaction.MoneyAmount = float64(23000)
	transaction.OrderId = uint64(1000)
	transaction.CustomerId = uint64(4000)
	transaction.SupplierId = uint64(6000)
	transaction.UserId = uint64(5000)
	transaction.Comment = "Comment"
	transaction.TransactionUUID = uudidd
	transaction.BallanceAmount = float64(123000)

	transactionId, err := dbMng.StoreTransaction(transaction, 3000)

	assert.Equal(t, transactionId, uint64(1), "they should be equal")
	assert.Nil(t, err, "error should be nil")
	assert.NotEqual(t, 0, transactionId, "userId  should not be equal to zero")
	if assert.NotNil(t, transaction) {
	}

	//type TransactionFilter struct {
	//	TransactionKeyword string
	//	TransactionDate    uint64
	//	Limit              uint32
	//	CustomerId         uint64
	//	SupplierId         uint64
	//}

	filter := new(pb.TransactionFilter)
	filter.CustomerId = uint64(4000)
	filter.TransactionDate = date + uint64(4000)
	filter.Limit = uint32(1000)

	transactions, err := dbMng.AllTransactionsForFilter(filter, 3000)
	for _, transaction := range transactions {
		assert.Equal(t, transaction.TransactionId, uint64(1), "they should be equal")
		assert.Equal(t, transaction.TransactionDate, date, "CategoryName")
		assert.Equal(t, transaction.IsLastTransaction, uint32(1), "CategoryName")
		assert.Equal(t, transaction.TransactionType, uint32(1000), "CategoryName")
		assert.Equal(t, transaction.MoneyAmount, float64(23000), "CategoryName")
		assert.Equal(t, transaction.OrderId, uint64(1000), "CategoryName")
		assert.Equal(t, transaction.CustomerId, uint64(4000), "CategoryName")
		assert.Equal(t, transaction.SupplierId, uint64(6000), "CategoryName")
		assert.Equal(t, transaction.UserId, uint64(5000), "CategoryName")
		assert.Equal(t, transaction.Comment, "Comment", "CategoryName")
		assert.Equal(t, transaction.TransactionUUID, uudidd, "CategoryName")
		assert.Equal(t, transaction.BallanceAmount, float64(123000), "CategoryName")
	}

	assert.Nil(t, err, "error should be nil")
	if assert.NotNil(t, transactions) {
	}

	transactionSupp, err := dbMng.RecentTransactionForSupplier(6000)
	assert.Nil(t, err, "error should be nil")
	if assert.NotNil(t, transactionSupp) {
		assert.Equal(t, transactionSupp.TransactionId, uint64(1), "they should be equal")
		assert.Equal(t, transactionSupp.TransactionDate, date, "CategoryName")
		assert.Equal(t, transactionSupp.IsLastTransaction, uint32(1), "CategoryName")
		assert.Equal(t, transactionSupp.TransactionType, uint32(1000), "CategoryName")
		assert.Equal(t, transactionSupp.MoneyAmount, float64(23000), "CategoryName")
		assert.Equal(t, transactionSupp.OrderId, uint64(1000), "CategoryName")
		assert.Equal(t, transactionSupp.CustomerId, uint64(4000), "CategoryName")
		assert.Equal(t, transactionSupp.SupplierId, uint64(6000), "CategoryName")
		assert.Equal(t, transactionSupp.UserId, uint64(5000), "CategoryName")
		assert.Equal(t, transactionSupp.Comment, "Comment", "CategoryName")
		assert.Equal(t, transactionSupp.TransactionUUID, uudidd, "CategoryName")
		assert.Equal(t, transactionSupp.BallanceAmount, float64(123000), "CategoryName")
	}

	transactionCust, err := dbMng.RecentTransactionForCustomer(4000)
	assert.Nil(t, err, "error should be nil")
	if assert.NotNil(t, transactionCust) {
		assert.Equal(t, transactionCust.TransactionId, uint64(1), "they should be equal")
		assert.Equal(t, transactionCust.TransactionDate, date, "CategoryName")
		assert.Equal(t, transactionCust.IsLastTransaction, uint32(1), "CategoryName")
		assert.Equal(t, transactionCust.TransactionType, uint32(1000), "CategoryName")
		assert.Equal(t, transactionCust.MoneyAmount, float64(23000), "CategoryName")
		assert.Equal(t, transactionCust.OrderId, uint64(1000), "CategoryName")
		assert.Equal(t, transactionCust.CustomerId, uint64(4000), "CategoryName")
		assert.Equal(t, transactionCust.SupplierId, uint64(6000), "CategoryName")
		assert.Equal(t, transactionCust.UserId, uint64(5000), "CategoryName")
		assert.Equal(t, transactionCust.Comment, "Comment", "CategoryName")
		assert.Equal(t, transactionCust.TransactionUUID, uudidd, "CategoryName")
		assert.Equal(t, transactionCust.BallanceAmount, float64(123000), "CategoryName")
	}
}

func TestUpdateTransaction(t *testing.T) {

	var date = updatedAt()
	uuidLocal, _ := uuid.NewV4()
	var uudidd = uuidLocal.String()

	transaction := new(pb.TransactionRequest)
	transaction.TransactionId = uint64(1)
	transaction.TransactionDate = date
	transaction.IsLastTransaction = uint32(1)
	transaction.TransactionType = uint32(1000)
	transaction.MoneyAmount = float64(23000)
	transaction.OrderId = uint64(1000)
	transaction.CustomerId = uint64(4000)
	transaction.SupplierId = uint64(6000)
	transaction.UserId = uint64(55000)
	transaction.Comment = "CommentCommentComment"
	transaction.TransactionUUID = uudidd
	transaction.BallanceAmount = float64(1213000)

	transactionId, err := dbMng.UpdateTransaction(transaction, 3000)

	assert.Equal(t, transactionId, uint64(1), "they should be equal")
	assert.Nil(t, err, "error should be nil")
	assert.NotEqual(t, 0, transactionId, "userId  should not be equal to zero")
	if assert.NotNil(t, transaction) {
	}

	filter := new(pb.TransactionFilter)
	filter.CustomerId = uint64(4000)
	filter.TransactionDate = date + uint64(4000)
	filter.Limit = uint32(1000)

	transactions, err := dbMng.AllTransactionsForFilter(filter, 3000)
	for _, transaction := range transactions {
		assert.Equal(t, transaction.TransactionId, uint64(1), "they should be equal")
		assert.Equal(t, transaction.TransactionDate, date, "CategoryName")
		assert.Equal(t, transaction.IsLastTransaction, uint32(1), "CategoryName")
		assert.Equal(t, transaction.TransactionType, uint32(1000), "CategoryName")
		assert.Equal(t, transaction.MoneyAmount, float64(23000), "CategoryName")
		assert.Equal(t, transaction.OrderId, uint64(1000), "CategoryName")
		assert.Equal(t, transaction.CustomerId, uint64(4000), "CategoryName")
		assert.Equal(t, transaction.SupplierId, uint64(6000), "CategoryName")
		assert.Equal(t, transaction.UserId, uint64(55000), "CategoryName")
		assert.Equal(t, transaction.Comment, "CommentCommentComment", "CategoryName")
		assert.Equal(t, transaction.TransactionUUID, uudidd, "CategoryName")
		assert.Equal(t, transaction.BallanceAmount, float64(1213000), "CategoryName")
	}

	assert.Nil(t, err, "error should be nil")
	if assert.NotNil(t, transactions) {
	}
}
