package models

import (
	"testing"
	"time"

	pb "bitbucket.org/kmomuke/rentau/grpc/proto"
	uuid "github.com/satori/go.uuid"
	"github.com/stretchr/testify/assert"
)

func updatedAt() uint64 {
	updatedAt := (time.Now().UnixNano() / 1000000)
	return uint64(updatedAt)
}

func TestCreateOrderDetail(t *testing.T) {

	var date = updatedAt()

	uuidLocal, _ := uuid.NewV4()
	var uudidd = uuidLocal.String()

	orderDetail := new(pb.OrderDetailRequest)
	//orderDetail.OrderDetailId
	orderDetail.OrderId = uint64(1200)
	orderDetail.OrderDetailDate = date
	orderDetail.IsLast = uint32(1)
	orderDetail.ProductId = uint64(400)
	orderDetail.BillingNo = "BillingNo"
	orderDetail.OrderDetailComment = "OrderDetailComment"
	orderDetail.Price = float64(43242)
	orderDetail.OrderQuantity = float64(11)
	orderDetail.OrderDetailUUID = uudidd
	orderDetail.ProductQuantity = float64(12)

	orderDetailId, err := dbMng.CreateOrderDetail(orderDetail, 3000)

	assert.Equal(t, orderDetailId, uint64(1), "they should be equal")
	assert.Nil(t, err, "error should be nil")
	assert.NotEqual(t, 0, orderDetailId, "userId  should not be equal to zero")
	if assert.NotNil(t, orderDetail) {
	}

	filter := new(pb.OrderDetailFilter)
	filter.OrderDetailDate = updatedAt() + uint64(100000)
	orderDetails, err := dbMng.AllOrderDetails(filter, 3000)
	for _, orderDetail := range orderDetails {
		assert.Equal(t, orderDetail.OrderDetailId, uint64(1), "they should be equal")
		assert.Equal(t, orderDetail.OrderId, uint64(1200), "they should be equal")
		assert.Equal(t, orderDetail.OrderDetailDate, date, "they should be equal")
		assert.Equal(t, orderDetail.IsLast, uint32(1), "they should be equal")
		assert.Equal(t, orderDetail.ProductId, uint64(400), "they should be equal")
		assert.Equal(t, orderDetail.BillingNo, "BillingNo", "CategoryName")
		assert.Equal(t, orderDetail.OrderDetailComment, "OrderDetailComment", "CategoryName")
		assert.Equal(t, orderDetail.Price, float64(43242), "CategoryName")
		assert.Equal(t, orderDetail.OrderQuantity, float64(11), "CategoryName")
		assert.Equal(t, orderDetail.OrderDetailUUID, uudidd, "CategoryName")
		assert.Equal(t, orderDetail.ProductQuantity, float64(12), "CategoryName")
	}

	assert.Nil(t, err, "error should be nil")
	if assert.NotNil(t, orderDetails) {
	}
}
