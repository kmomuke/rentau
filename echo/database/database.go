package models

import (
	_ "github.com/lib/pq"
	"github.com/jmoiron/sqlx"
	log "github.com/Sirupsen/logrus"
	"errors"

)

type UserRequest struct {
	UserId        uint64
	UserUUID      string
	CompanyId     uint64
	RoleId        uint64
	UserImagePath string
	FirstName     string
	SecondName    string
	Email         string
	Password      string
	PhoneNumber   string
	Address       string
}

type OrderFilter struct {
	OrderKeyword string
	OrderDate    uint64
	Limit        uint32
}

type OrderRequest struct {
	OrderId           uint64
	OrderDocument     uint32 `json:"-"`
	MoneyMovementType uint32 `json:"-"`
	BillingNo         string `json:"Customer"`
	UserId            uint64 `json:"-"`
	CustomerId        uint64 `json:"-"`
	SupplierId        uint64 `json:"-"`
	OrderDate         uint64
	PaymentId         uint64 `json:"-"`
	ErrorMsg          string `json:"-"`
	Comment           string `json:"-"`
	OrderUUID         string `json:"-"`
	IsDeleted         uint32 `json:"-"`
	IsMoneyForDebt    uint32 `json:"-"`
	IsEdited          uint32 `json:"-"`
}

type PaymentRequest struct {
	PaymentId              uint64 `json:"-"`
	TotalOrderPrice        float64
	Discount               float64
	TotalPriceWithDiscount float64
}

type OrderDetailRequest struct {
	OrderDetailId   uint64 `json:"-"`
	OrderId         uint64 `json:"-"`
	OrderDetailDate uint64 `json:"-"`
	IsLast          uint32 `json:"-"`
	ProductId       uint64 `json:"-"`
	BillingNo       string `json:"Product"`
	Price           float64
	OrderQuantity   float64 `json:"Quantity"`
	Discount        int32 `json:"-"`
	QuantityPerUnit string `json:"PerUnit"`
}

type CreateOrderRequest struct {
	Order        *OrderRequest
	Payment      *PaymentRequest
	OrderDetails []*OrderDetailRequest
}

type CustomerRequest struct {
	CustomerId        uint64
	CustomerImagePath string
	FirstName         string
	SecondName        string
	PhoneNumber       string
	Address           string
	UserId           uint64
	CustomerUpdatedAt uint64
}

type ProductRequest struct {
	ProductId        uint64
	ProductImagePath string
	ProductName      string
	SupplierId       uint64
	CategoryId       uint64
	Barcode          string
	QuantityPerUnit  string
	SaleUnitPrice    float64
	IncomeUnitPrice  float64
	UnitsInStock     float64
	ProductUpdatedAt uint64
}

func NewDBToConnect(dataSourceName string) (*sqlx.DB, error) {

	db, err := sqlx.Connect("postgres", dataSourceName)
	if err != nil {
		log.WithFields(log.Fields{"error": err,}).Fatal("Can not connected to database")
		return nil, err
	}

	pingError := db.Ping()
	if pingError != nil {
		log.WithFields(log.Fields{"ping": pingError,}).Fatal("Can not connected Ping to database")
		return nil, pingError
	}

	return db, nil
}

var selectUserRow string = "user_id, " +
		"company_id, " +
		"user_uuid, " +
		"role_id, " +
		"user_image_path, " +
		"first_name, " +
		"second_name, " +
		"email, " +
		"password, " +
		"phone_number, " +
		"address "

func scanUserRow(rows *sqlx.Rows) ([]*UserRequest, error) {
	users := make([]*UserRequest, 0)
	for rows.Next() {
		user := new(UserRequest)
		err := rows.Scan(
			&user.UserId,
			&user.CompanyId,
			&user.UserUUID,
			&user.RoleId,
			&user.UserImagePath,
			&user.FirstName,
			&user.SecondName,
			&user.Email,
			&user.Password,
			&user.PhoneNumber,
			&user.Address)

		if err != nil {
			log.WithFields(log.Fields{"scanUserRow":err,}).Warn("ERROR")
			break
		}

		users = append(users, user)
	}

	return users, nil
}

func UserForEmail(db *sqlx.DB, email string) (*UserRequest, error)  {

	rows, err := db.Queryx("SELECT " +
		selectUserRow +
		"FROM users WHERE email=$1", email)

	if err != nil {
		print("error")
	}

	users, err := scanUserRow(rows)

	if len(users) > 0 {
		return users[0], nil
	}

	return nil, nil
}

func UserForUUID(db *sqlx.DB, uuid string) (*UserRequest, error)  {

	rows, err := db.Queryx("SELECT " +
		selectUserRow +
		"FROM users WHERE user_uuid = $1", uuid)

	if err != nil {
		print("error")
	}

	users, err := scanUserRow(rows)

	if len(users) > 0 {
		return users[0], nil
	}

	return nil, errors.New("No such user")
}

var selectOrderRow string =
	"order_id, " +
		"order_document, " +
		"money_movement, " +
		"billing_no, " +
		"user_id, " +
		"customer_id, " +
		"supplier_id, " +
		"order_date, " +
		"payment_id, " +
		"error_msg, " +
		"uuid, " +
		"comment, " +
		"is_deleted, " +
		"is_money_for_debt, " +
		"is_editted "

func scanOrderRows(rows *sqlx.Rows) ([]*OrderRequest, error) {
	orders := make([]*OrderRequest, 0)
	for rows.Next() {
		order := new(OrderRequest)
		err := rows.Scan(
			&order.OrderId,
			&order.OrderDocument,
			&order.MoneyMovementType,
			&order.BillingNo,
			&order.UserId,
			&order.CustomerId,
			&order.SupplierId,
			&order.OrderDate,
			&order.PaymentId,
			&order.ErrorMsg,
			&order.OrderUUID,
			&order.Comment,
			&order.IsDeleted,
			&order.IsMoneyForDebt,
			&order.IsEdited)

		if err != nil {
			log.WithFields(log.Fields{"scanOrderRows":err,}).Warn("ERROR")
			return nil, err
		}

		orders = append(orders, order)
	}
	return orders, nil
}

func AllOrdersForFilter(db *sqlx.DB, orderFilter *OrderFilter, companyId uint64) ([]*OrderRequest, error) {

	rows, err := db.Queryx("SELECT " +
		selectOrderRow +
		"FROM orders WHERE order_date<=$1 AND company_id=$2 AND order_document=1000 " +
		"ORDER BY order_date DESC LIMIT $3", orderFilter.OrderDate, companyId, orderFilter.Limit)

	if err != nil {
		log.WithFields(log.Fields{"error":err,}).Warn("ERROR")
		return nil, err
	}

	orders, err := scanOrderRows(rows)

	if err = rows.Err(); err != nil {
		log.WithFields(log.Fields{"error":err,}).Warn("ERROR")
		return nil, err
	}

	return orders, nil
}

func AllOrderDetailsForOrder(db *sqlx.DB, order *OrderRequest) ([]*OrderDetailRequest, error) {

	rows, err := db.Queryx("SELECT " +
		"order_detail_id, " +
		"order_id, " +
		"order_detail_date, " +
		"is_last, " +
		"billing_no, " +
		"product_id, " +
		"price, " +
		"order_quantity " +
		"FROM orderdetails WHERE order_id=$1 " +
		"ORDER BY order_detail_date DESC", order.OrderId)

	if err != nil {
		print(" AllOrderDetailsForOrder_error ")
	}

	orderDetails := make([]*OrderDetailRequest, 0)
	for rows.Next() {
		orderDetail := new(OrderDetailRequest)
		err := rows.Scan(
			&orderDetail.OrderDetailId,
			&orderDetail.OrderId,
			&orderDetail.OrderDetailDate,
			&orderDetail.IsLast,
			&orderDetail.BillingNo,
			&orderDetail.ProductId,
			&orderDetail.Price,
			&orderDetail.OrderQuantity)

		if err != nil {
			return nil, err
		}
		orderDetails = append(orderDetails, orderDetail)
	}

	if err = rows.Err(); err != nil {
		return nil, err
	}

	for _, orderDetail := range orderDetails {
		product, err := ProductFor(db, orderDetail.ProductId)
		if err != nil {
			print(" ProductFor_error_not_found ")
			orderDetail.BillingNo = "NOT FOUND"
		}
		if product != nil {
			orderDetail.BillingNo = product.ProductName
			orderDetail.QuantityPerUnit = product.QuantityPerUnit
		}
	}

	return orderDetails, nil
}

func PaymentForOrder(db *sqlx.DB, order *OrderRequest) (*PaymentRequest, error) {

	pingError := db.Ping()

	if pingError != nil {
		log.Fatalln(pingError)
		return nil, pingError
	}

	rows, err := db.Queryx("SELECT " +
		"payment_id, " +
		"total_order_price, " +
		"discount, " +
		"total_price_with_discount " +
		"FROM payments WHERE payment_id=$1 LIMIT $2", order.PaymentId, 1)

	if err != nil {
		print(" PaymentForOrder_error ")
	}

	payments := make([]*PaymentRequest, 0)
	for rows.Next() {
		payment := new(PaymentRequest)
		err := rows.Scan(
			&payment.PaymentId,
			&payment.TotalOrderPrice,
			&payment.Discount,
			&payment.TotalPriceWithDiscount)

		if err != nil {
			return nil, err
		}
		payments = append(payments, payment)
	}

	if err = rows.Err(); err != nil {
		return nil, err
	}

	if len(payments) > 0 {
		return payments[0], nil
	}

	log.WithFields(log.Fields{"order.OrderId": order.OrderId}).Warn("")
	return nil, errors.New("Not found PaymentForOrder")
}

func ProductFor(db *sqlx.DB, productId uint64) (*ProductRequest, error) {

	rows, err := db.Queryx("SELECT " +
		"product_id, " +
		"product_image_path, " +
		"product_name, " +
		"supplier_id, " +
		"category_id, " +
		"barcode, " +
		"quantity_per_unit, " +
		"sale_unit_price, " +
		"income_unit_price, " +
		"units_in_stock " +
		"FROM products WHERE product_id=$1", productId)

	if err != nil {
		print(" ProductFor_error_Queryx ")
	}

	products := make([]*ProductRequest, 0)
	for rows.Next() {
		product := new(ProductRequest)
		err := rows.Scan(
			&product.ProductId,
			&product.ProductImagePath,
			&product.ProductName,
			&product.SupplierId,
			&product.CategoryId,
			&product.Barcode,
			&product.QuantityPerUnit,
			&product.SaleUnitPrice,
			&product.IncomeUnitPrice,
			&product.UnitsInStock)

		if err != nil {
			return nil, err
		}

		products = append(products, product)
	}

	if err = rows.Err(); err != nil {
		return nil, err
	}

	if len(products) > 0 {
		return products[0], nil
	}

	return nil, errors.New("Not found Product")
}

func CustomerFor(db *sqlx.DB, customerId uint64) (*CustomerRequest, error) {

	pingError := db.Ping()

	if pingError != nil {
		log.Fatalln(pingError)
		return nil, pingError
	}

	rows, err := db.Queryx("SELECT " +
		"customer_id, " +
		"customer_image_path, " +
		"first_name, " +
		"second_name, " +
		"phone_number, " +
		"address, " +
		"user_id, " +
		"updated_at " +
		"FROM customers WHERE customer_id=$1", customerId)

	if err != nil {
		log.WithFields(log.Fields{"error": err, }).Warn("")
		return nil, err
	}

	realCustomers := make([]*CustomerRequest, 0)
	for rows.Next() {
		customer := new(CustomerRequest)
		err := rows.Scan(
			&customer.CustomerId,
			&customer.CustomerImagePath,
			&customer.FirstName,
			&customer.SecondName,
			&customer.PhoneNumber,
			&customer.Address,
			&customer.UserId,
			&customer.CustomerUpdatedAt)

		if err != nil {
			log.WithFields(log.Fields{"error": err, }).Warn("")
			return nil, err
		}
		realCustomers = append(realCustomers, customer)
	}

	if err = rows.Err(); err != nil {
		log.WithFields(log.Fields{"error": err, }).Warn("")
		return nil, err
	}

	if len(realCustomers) > 0 {
		return realCustomers[0], nil
	}

	return nil, errors.New("Not found Customer")
}