package main

import (
	"net/http"
	"time"

	jwt "github.com/dgrijalva/jwt-go"
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	"golang.org/x/crypto/bcrypt"

	"fmt"
	"os"

	model "bitbucket.org/kmomuke/rentau/echo/database"
	log "github.com/Sirupsen/logrus"
	"github.com/jmoiron/sqlx"
	"github.com/pkg/errors"
)

// jwtCustomClaims are custom claims extending default ones.
type jwtCustomClaims struct {
	UserUUID string `json:"name"`
	Admin    bool   `json:"admin"`
	jwt.StandardClaims
}

var db *sqlx.DB

func login(c echo.Context) error {

	username := c.FormValue("username")
	password := c.FormValue("password")

	userExsist, err := model.UserForEmail(db, username)

	if err != nil {
		return err
	}

	if userExsist == nil {
		return errors.New("User with email does not exsists")
	}

	if bcrypt.CompareHashAndPassword([]byte(userExsist.Password), []byte(password)) != nil {
		return errors.New("Password invalid")
	}

	if true {
		// Set custom claims
		claims := &jwtCustomClaims{
			userExsist.UserUUID,
			true,
			jwt.StandardClaims{
				ExpiresAt: time.Now().Add(time.Hour * 72).Unix(),
			},
		}

		// Create token with claims
		token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)

		// Generate encoded token and send it as response.
		t, err := token.SignedString([]byte("secret"))
		if err != nil {
			return err
		}
		return c.JSON(http.StatusOK, echo.Map{
			"token": t,
		})
	}

	return echo.ErrUnauthorized
}

func accessible(c echo.Context) error {
	return c.String(http.StatusOK, "Accessible")
}

func restricted(c echo.Context) error {
	user := c.Get("user").(*jwt.Token)
	claims := user.Claims.(*jwtCustomClaims)
	userUUID := claims.UserUUID

	log.WithFields(log.Fields{"userUUID:": userUUID}).Info("")

	userReq, err := model.UserForUUID(db, userUUID)
	if err != nil {
		return err
	}

	orderFilter := new(model.OrderFilter)
	orderFilter.OrderDate = uint64(time.Now().UnixNano() / 1000000)
	orderFilter.Limit = 100

	createOrderRequests := make([]*model.CreateOrderRequest, 0)

	orders, err := model.AllOrdersForFilter(db, orderFilter, userReq.CompanyId)
	for _, order := range orders {
		createOrderRequest := new(model.CreateOrderRequest)

		customer, error := model.CustomerFor(db, order.CustomerId)
		if error != nil {
			order.BillingNo = "NOT FOUND"
		}
		if customer != nil {
			order.BillingNo = customer.FirstName + " " + customer.SecondName + " " + customer.PhoneNumber + " " + customer.Address
		}

		createOrderRequest.Order = order

		payment, error := model.PaymentForOrder(db, order)
		if error != nil {
			break
		}
		createOrderRequest.Payment = payment
		orderDetails, error := model.AllOrderDetailsForOrder(db, order)
		if error != nil {
			break
		}
		createOrderRequest.OrderDetails = orderDetails
		createOrderRequests = append(createOrderRequests, createOrderRequest)
	}

	log.WithFields(log.Fields{"initial len(orders):": len(orders)}).Info("")
	if err != nil {
		log.WithFields(log.Fields{"error": err}).Warn("ERROR")
	}

	return c.JSON(http.StatusOK, createOrderRequests)
}

func main() {

	connInfo := fmt.Sprintf(
		"user=%s dbname=%s password=%s host=%s port=%s sslmode=disable",
		os.Getenv("DB_ENV_POSTGRES_USER"),
		os.Getenv("DB_ENV_POSTGRES_DATABASENAME"),
		os.Getenv("DB_ENV_POSTGRES_PASSWORD"),
		os.Getenv("RENTAU_POSTGRES_1_PORT_5432_TCP_ADDR"),
		os.Getenv("RENTAU_POSTGRES_1_PORT_5432_TCP_PORT"),
	)

	log.WithFields(log.Fields{"-----------------------------": ""}).Info("")
	log.WithFields(log.Fields{"connInfo": connInfo}).Info("")
	log.WithFields(log.Fields{"-----------------------------": ""}).Info("")

	var databaseError error
	//db, databaseError = model.NewDBToConnect("user=kanybek dbname=rentau password=nazgulum host=localhost port=5432 sslmode=disable")
	db, databaseError = model.NewDBToConnect(connInfo)
	if databaseError != nil {
		log.WithFields(log.Fields{"omg": databaseError}).Fatal("failed to listen:")
	}

	e := echo.New()

	// Middleware
	e.Use(middleware.Logger())
	e.Use(middleware.Recover())

	// CORS default
	// Allows requests from any origin wth GET, HEAD, PUT, POST or DELETE method.
	e.Use(middleware.CORS())

	// CORS restricted
	// Allows requests from any `https://labstack.com` or `https://labstack.net` origin
	// wth GET, PUT, POST or DELETE method.
	//e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
	//	AllowOrigins: []string{"https://labstack.com", "https://labstack.net"},
	//	AllowMethods: []string{echo.GET, echo.PUT, echo.POST, echo.DELETE},
	//}))

	// Login route
	e.POST("/login", login)

	// Unauthenticated route
	e.GET("/", accessible)

	// Restricted group
	r := e.Group("/restricted")

	// Configure middleware with the custom claims type
	config := middleware.JWTConfig{
		Claims:     &jwtCustomClaims{},
		SigningKey: []byte("secret"),
	}
	r.Use(middleware.JWTWithConfig(config))
	r.GET("", restricted)

	e.Logger.Fatal(e.Start(":1323"))
}
